-- This is the top-module of the whole formalization where we
-- instantiate the scheduler parametric non-interference theorems with
-- round-robin (the scheduler used by GHC) and conclude that MAC is
-- secure form that. For simplicity, in this module we use the
-- two-point lattice, i.e., 𝓛 = {Low, High}, where High ⋤ Low is the only
-- disallowed flow. The attacker's level is assumed to be Low.

module MAC  where

open import Lattice.TwoPoint
open import Data.Product
open import Types 𝓛⁽ᴸᴴ⁾
open import Sequential 𝓛⁽ᴸᴴ⁾
open import Sequential.Security 𝓛⁽ᴸᴴ⁾ Low

-- MAC satisfies progress-insensitive noninterference (PINI).  If two
-- low-equivalent programs terminates then they terminate in
-- low-equivalent configurations.
mac-is-pini : ∀ {l τ} {p₁ p₁' p₂ p₂' : Program (Mac l τ)} -> p₁ ≅ᴾ p₂ -> p₁ ⇓ p₁' -> p₂ ⇓ p₂' -> p₁' ≅ᴾ p₂'
mac-is-pini p₁≈p₂ step = pini p₁≈p₂ step

open import Scheduler.RoundRobin 𝓛⁽ᴸᴴ⁾
open import Scheduler.RoundRobin.Security {𝓛⁽ᴸᴴ⁾} Low
open import Concurrent 𝓛⁽ᴸᴴ⁾ RR
open import Concurrent.Valid 𝓛⁽ᴸᴴ⁾ RR
open import Concurrent.Security Low RR-is-NI valid-RR

-- MAC satisfies progress-sensitive noninterference (PSNI).
mac-is-psni : ∀ {l n} {c₁ c₁' c₂ : Global} {{v₁ : validᴳ c₁}} {{v₂ : validᴳ c₂}}
                      -> c₁ ≅ᴳ c₂ -> ( l , n )  ⊢ c₁ ↪ c₁' -> ∃ (λ c₂' → c₂ ↪⋆ c₂' × c₁' ≅ᴳ c₂')
mac-is-psni c₁≈c₂ s = psni c₁≈c₂ s
