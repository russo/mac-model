-- This module defines the operational semantics of the concurrent
-- calculus.

import Lattice as L
import Scheduler as S

module Concurrent.Semantics (𝓛 : L.Lattice) (𝓢 : S.Scheduler 𝓛) where

open import Relation.Binary.PropositionalEquality
open import Data.Nat
open import Types 𝓛
open S 𝓛 renaming (Event to Eventˢ)
open S.Scheduler 𝓛 𝓢

open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛
open import Concurrent.Calculus 𝓛 𝓢

open import Relation.Nullary
open import Data.Product renaming (_,_ to ⟪_,_⟫)

-- Sequential events are used to detect spawning of new threads.
data Event (l : Label) : Set where
  ∅ : Event l
  fork : ∀ {h} -> (l⊑h : l ⊑ h) (t : Thread h) -> Event l
  fork∙ : ∀ {h} -> (l⊑h : l ⊑ h) (t : Thread h) -> Event l

-- Instead of lifting the whole semantics to carry a sequential event,
-- function even compute the sequential even directly from the
-- sequential step.
event  : ∀ {l τ} {p₁ p₂ : Program (Mac l τ)} -> p₁ ⟼ p₂ -> Event l
event (Bind₁ step) = event step
event (Catch₁ step) = event step
event (Fork {t = t} l⊑h) = fork l⊑h t
event (Fork∙ {t = t} l⊑h) = fork∙ l⊑h t
event _ = ∅

-- A transition ⟪ l , n ⟫ ⊢ c ↪ c' denotes a step in the concurrent
-- semantics, that brings the global configuration c to c' by running
-- thread n at security level l.  (For simplicity we define directly
-- the annotated version of the concurrent semantics)
data _⊢_↪_ : Label × ℕ -> Global -> Global -> Set where

  -- A single sequential step
  Step : ∀ {l n ω Σ Φ ω' Σ' } ->
           let P = Φ l    in
           {t t' : Thread l} {P' : Pool l}
           (sch : ω ⟶ ω' ↑ ⟪ l , n , Step ⟫ )
           (r : n ↦ t ∈ᴾ P)
           (step : ⟨ Σ , t ⟩ ⟼ ⟨ Σ' , t' ⟩)
           (s≡∅ : event step ≡ ∅)
           (u : P' ≔ P [ n ↦ t' ]ᴾ)
           -> ⟪ l , n ⟫ ⊢ ⟨ ω , Σ , Φ ⟩ ↪ ⟨ ω' , Σ' , Φ [ l ↦ P' ]ᴹ ⟩

  -- A new thread is forked
  CFork : ∀ {L nᴸ H ω ω' Σ Φ} {tᴴ : Thread H} {t t' : Thread L} {P' : Pool L} ->
           let P = Φ L
               Φ' = Φ [ L ↦ P' ]ᴹ
               Pᴴ = Φ' H
               nᴴ = lengthᴾ Pᴴ in
           (L⊑H : L ⊑ H)
           (sch : ω ⟶ ω' ↑ ⟪ L , nᴸ , Fork H nᴴ L⊑H ⟫ )
           (r : nᴸ ↦ t ∈ᴾ P)
           (step : ⟨ Σ , t ⟩ ⟼ ⟨ Σ , t' ⟩)
           (s≡f : event step ≡ fork L⊑H tᴴ)
           (u : P' ≔ P [ nᴸ ↦ t' ]ᴾ)
           -> ⟪ L , nᴸ ⟫ ⊢ ⟨ ω , Σ , Φ ⟩ ↪ ⟨ ω' , Σ , Φ' [ H ↦ Pᴴ ▻ tᴴ ]ᴹ  ⟩

  -- This rule triggers when fork∙ is executed (a high-thread gets forked).
  -- The thread is not actually added to thread pool because this operation
  -- is not visible to the attacker
  CFork∙ : ∀ {L nᴸ H ω ω' Σ Φ} {tᴴ : Thread H} ->
           let P = Φ L in
           {t t' : Thread L} {P' : Pool L}
           (L⊑H : L ⊑ H)
           (sch : ω ⟶ ω' ↑ ⟪ L , nᴸ , Step ⟫ )
           (r : nᴸ ↦ t ∈ᴾ P)
           (step : ⟨ Σ , t ⟩ ⟼ ⟨ Σ , t' ⟩)
           (s≡f : event step ≡ fork∙ L⊑H tᴴ)
           (u : P' ≔ P [ nᴸ ↦ t' ]ᴾ)
           -> ⟪ L , nᴸ ⟫ ⊢ ⟨ ω , Σ , Φ ⟩ ↪ ⟨ ω' , Σ , Φ [ L ↦ P' ]ᴹ ⟩

  -- The selected thread has terminated: inform the scheduler.
  Done : ∀ {l n ω ω' Σ Φ t} ->
         let P = Φ l in
         (sch : ω ⟶ ω' ↑ ⟪ l , n , Done ⟫ )
         (r : n ↦ t ∈ᴾ P)
         (done : Doneᴾ ⟨ Σ , t ⟩)
         -> ⟪ l , n ⟫ ⊢ ⟨ ω , Σ , Φ ⟩ ↪ ⟨ ω' , Σ , Φ ⟩

  -- The selected thread is stuck: inform the scheduler.
  Stuck : ∀ {l n ω ω' Σ Φ t} ->
          let P = Φ l in
         (sch : ω ⟶ ω' ↑ ⟪ l , n , Stuck ⟫ )
         (r : n ↦ t ∈ᴾ P)
         (stuck : Stuckᴾ ⟨ Σ , t ⟩)
         -> ⟪ l , n ⟫ ⊢ ⟨ ω , Σ , Φ ⟩ ↪ ⟨ ω' , Σ , Φ ⟩

-- Return the thread that runs in the concurrent step
getThread : ∀ {x} {c₁ c₂ : Global} → x ⊢ c₁ ↪ c₂ → Thread (proj₁ x)
getThread (Step {t = t} sch r step s≡∅ u) = t
getThread (CFork {t = t} L⊑H sch r step s≡f u) = t
getThread (CFork∙ {t = t} L⊑H sch r step s≡f u) = t
getThread (Done {t = t}  sch r done) = t
getThread (Stuck {t = t} sch r stuck) = t

-- Return the proof that the next scheduled thread is in the right
-- thread pool at the right position.
get-∈ᴾ  : ∀ {x : Label × ℕ} {c₁ c₂ : Global} → (step : x ⊢ c₁ ↪ c₂) →
           let ⟪ l , n ⟫ = x
               Pˡ = mapᴾ c₁ l
               t = getThread step in n ↦ t ∈ᴾ Pˡ
get-∈ᴾ (Step sch r step s≡∅ u) = r
get-∈ᴾ (CFork L⊑H sch r step s≡f u) = r
get-∈ᴾ (CFork∙ L⊑H sch r step s≡f u) = r
get-∈ᴾ (Done sch r done) = r
get-∈ᴾ (Stuck sch r stuck) = r

-- Return the scheduler event triggered by the concurrent semantic step.
getEvent : ∀ {x} {c₁ c₂ : Global} -> x ⊢ c₁ ↪ c₂ -> Eventˢ (proj₁ x)
getEvent (Step sch r step s≡∅ u) = Step
getEvent (CFork {L} {H = H} {Φ = Φ} {P' = P'} L⊑H sch r step s≡f u) = Fork H (lengthᴾ ((Φ [ L ↦ P' ]ᴹ) H)) L⊑H
getEvent (CFork∙ L⊑H sch r step s≡f u) = Step
getEvent (Done sch r done) = Done
getEvent (Stuck sch r stuck) = Stuck

-- Return the scheduler step performed by the concurrent semantics.
getSchStep : ∀ {x} {c₁ c₂ : Global} -> (s : x ⊢ c₁ ↪ c₂) -> state c₁ ⟶ state c₂ ↑ ⟪ proj₁ x , proj₂ x , getEvent s ⟫
getSchStep (Step sch r step s≡∅ u) = sch
getSchStep (CFork L⊑H sch r step s≡f u) = sch
getSchStep (CFork∙ L⊑H sch r step s≡f u) = sch
getSchStep (Done sch r done) = sch
getSchStep (Stuck sch r stuck) = sch

--------------------------------------------------------------------------------

-- Transitive reflexive closure of the concurrent small step relation.
data _↪⋆_ : Global -> Global -> Set where
  [] : ∀ {c} -> c ↪⋆ c
  _∷_ : ∀ {x c₁ c₂ c₃} -> x ⊢ c₁ ↪ c₂ -> c₂ ↪⋆ c₃ -> c₁ ↪⋆ c₃

-- Redexᴳ (l , n) c is the proof that the global configuration c steps
-- and runs thread (l , n).
data Redexᴳ (x : Label × ℕ) (c : Global) : Set where
  Step : ∀ {c'} -> x ⊢ c ↪ c' -> Redexᴳ x c
