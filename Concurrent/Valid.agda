-- This module lifts the valid judgment defined in Sequential.Valid to
-- the concurrent configuration.

import Lattice as L
import Scheduler as S

module Concurrent.Valid (𝓛 : L.Lattice) (𝓢 : S.Scheduler 𝓛) where

open import Types 𝓛
open S.Scheduler 𝓛 𝓢
open import Scheduler.Base 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛
open import Sequential.Valid 𝓛 hiding (validᴹ ; validᴹ-⊆ ) renaming (validᴾ to validᴾ' ; validˢ to validˢ')
open import  Concurrent.Calculus 𝓛 𝓢
open import Concurrent.Semantics 𝓛 𝓢

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality
open import Data.Nat hiding (_≟_)
open import Data.Product as P renaming (_,_ to ⟪_,_⟫)
open import Data.Empty

-- A valid pool contains only valid threads and is not ∙.
validᴾ : ∀ {l} -> Store -> Pool l -> Set
validᴾ Σ [] = ⊤
validᴾ Σ (t ◅ P) = valid Σ t × validᴾ Σ P
validᴾ Σ ∙ = ⊥

-- A valid pool map contains only valid pools
validᴹ : Store -> Mapᴾ -> Set
validᴹ Σ Φ = (l : Label) → validᴾ Σ (Φ l)

-- The scheduler state is valid with respect to the thread pool map if
-- any alive thread according to the scheduler is present in the pool map.
validˢ : State → Mapᴾ → Set
validˢ ω Φ = (∀ x →
                let ⟪ l , n ⟫ = x in
                   ⟪ l , n ⟫ ∈ˢ ω → ∃ (λ t → n ↦ t ∈ᴾ (Φ l)))

mutual
  -- A valid scheduler preserves the valid scheduler property
  record Validˢ : Set where
    field validˢ↪ : ∀ {x c₁ c₂} → x ⊢ c₁ ↪ c₂ → validᴳ c₁ → validˢ (state c₂) (mapᴾ c₂)

  -- A valid configurations contains a valid store and a valid pool map.
  validᴳ : ∀ Global -> Set
  validᴳ ⟨ ω , Σ , Φ ⟩  = validˢ ω Φ × validˢ' Σ × validᴹ Σ Φ

--------------------------------------------------------------------------------
-- Helper lemmas: manipulating valid objects preserves the valid property.

-- Reading from a valid thread pool gives a valid thread.
readᴾ-valid : ∀ {Σ l n t} {P : Pool l} -> n ↦ t ∈ᴾ P -> validᴾ Σ P  -> valid Σ t
readᴾ-valid here (v P., _) = v
readᴾ-valid (there r) (_ P., v) = readᴾ-valid r v

-- Writing a valid thread to a valid pool gives a valid thread pool
writeᴾ-valid : ∀ {Σ l n t} {P₁ P₂ : Pool l} -> P₂ ≔ P₁ [ n ↦ t ]ᴾ -> validᴾ Σ P₁ -> valid Σ t -> validᴾ Σ P₂
writeᴾ-valid here (_ Σ., vᴾ) v = v Σ., vᴾ
writeᴾ-valid (there w) (v' Σ., vᴾ) v = v' Σ., writeᴾ-valid w vᴾ v

-- Appending a new thread in a valid thread pool gives a valid pool
valid-▻ : ∀ {Σ l t} {P : Pool l} -> validᴾ Σ P -> valid Σ t -> validᴾ Σ (P ▻ t)
valid-▻ {P = []} tt v = v Σ., tt
valid-▻ {P = t ◅ P} (v' Σ., vᴾ) v = v' Σ., valid-▻ vᴾ v
valid-▻ {P = ∙} () v

-- Updating the pool map with a valid pool gives a valid pool map
updateᴹ-valid : ∀ {l Σ Φ} {P : Pool l} -> validᴹ Σ Φ -> validᴾ Σ P -> validᴹ Σ (Φ [ l ↦ P ]ᴹ)
updateᴹ-valid {l} vᴹ vᴾ l' with l ≟ l'
... | yes refl = vᴾ
... | no _ = vᴹ l'

-- If a pool is valid for some store Σ₁, then it is valid for any other
-- store Σ₂ that extends it, i.e., Σ₁ ⊆ Σ₂
validᴾ-⊆ : ∀ {l Σ₁ Σ₂} {P : Pool l} -> validᴾ Σ₁ P -> Σ₁ ⊆ˢ Σ₂ -> validᴾ Σ₂ P
validᴾ-⊆ {P = []} tt Σ₁⊆Σ₂ = tt
validᴾ-⊆ {P = t ◅ P} (v Σ., vᴾ) Σ₁⊆Σ₂ = (valid-⊆ t v Σ₁⊆Σ₂) Σ., validᴾ-⊆ vᴾ Σ₁⊆Σ₂
validᴾ-⊆ {P = ∙} () Σ₁⊆Σ₂

-- If a pool map is valid for some store Σ₁, then it is valid for any other
-- store Σ₂ that extends it, i.e., Σ₁ ⊆ Σ₂
validᴹ-⊆ : ∀ {Σ₁ Σ₂ Φ} -> validᴹ Σ₁ Φ -> Σ₁ ⊆ˢ Σ₂ -> validᴹ Σ₂ Φ
validᴹ-⊆ v Σ⊆Σ' = λ l → validᴾ-⊆ (v l) Σ⊆Σ'

-- A valid program can only fork valid threads.
fork-valid : ∀ {L H τ} {L⊑H : L ⊑ H} {p₁ p₂ : Program (Mac L τ)} {tᴴ : Thread H}
                  (step : p₁ ⟼ p₂) -> validᴾ' p₁  -> event step ≡ fork L⊑H tᴴ -> valid (store p₁) tᴴ
fork-valid (Lift x) v ()
fork-valid (Label' l⊑h) v ()
fork-valid (Unlabel₁ l⊑h step) v ()
fork-valid (Unlabel₂ l⊑h) v ()
fork-valid (Unlabelχ l⊑h) v ()
fork-valid (Bind₁ step) (vˢ Σ., v Σ., _) eq = fork-valid step (vˢ Σ., v) eq
fork-valid Bind₂ v ()
fork-valid Bind₃ v ()
fork-valid (Join l⊑h x) v ()
fork-valid (Joinχ l⊑h x) v ()
fork-valid (Join∙ l⊑h) v ()
fork-valid (Catch₁ step) (vˢ Σ., v Σ., _) eq = fork-valid step (vˢ Σ., v) eq
fork-valid Catch₂ v ()
fork-valid Catch₃ v ()
fork-valid (New l⊑h) v ()
fork-valid (Write₁ l⊑h step) v ()
fork-valid (Write₂ l⊑h w) v ()
fork-valid (Read₁ l⊑h step) v ()
fork-valid (Read₂ l⊑h r) v ()
fork-valid (New∙ l⊑h) v ()
fork-valid (Write∙₁ l⊑h step) v ()
fork-valid (Write∙₂ l⊑h) v ()
fork-valid (Fork l⊑h) v refl = proj₂ v
fork-valid (Fork∙ l⊑h) v ()
fork-valid (NewMVar l⊑h) v ()
fork-valid (Take₁ step) v ()
fork-valid (Take₂ r w) v ()
fork-valid (Put₁ step) v ()
fork-valid (Put₂ r w) v ()
fork-valid (NewMVar∙ l⊑h) v ()


-- The proof that thesemantics preserve validity of the global configuration
-- is parametrized by the corresponding proof for the scheduler used
module Valid↪ (𝓥 : Validˢ) where

  open Validˢ 𝓥 public

  -- The concurrent semantics preserves the valid property.
  valid↪ : ∀ {l n} {c₁ c₂ : Global} -> (l P., n) ⊢ c₁ ↪ c₂ -> validᴳ c₁ -> validᴳ c₂
  valid↪ {l} s@(Step sch r step s≡∅ u) (ωⱽ P., vˢ P., vᴹ) with readᴾ-valid r (vᴹ l)
  ... | vᴾ with valid⟼ (vˢ P., vᴾ) step | step-⊆ˢ (vˢ Σ., vᴾ) step
  ... | vˢ' P., vᵗ' | Σ₁⊆Σ₂ with validᴹ-⊆ vᴹ Σ₁⊆Σ₂
  ... | vᴹ' =  validˢ↪ s (ωⱽ P., vˢ P., vᴹ)  P., vˢ' P., updateᴹ-valid vᴹ' (writeᴾ-valid u (vᴹ' l) vᵗ')
  valid↪ {L} s@(CFork {H = H} L⊑H sch r step s≡f u) (ωⱽ P., vˢ P., vᴹ) with readᴾ-valid r (vᴹ L)
  ... | vᴾ with valid⟼ (vˢ P., vᴾ) step | step-⊆ˢ (vˢ Σ., vᴾ) step
  ... | vˢ' P., vᵗ' | Σ₁⊆Σ₂ with validᴹ-⊆ vᴹ Σ₁⊆Σ₂
  ... | vᴹ' with updateᴹ-valid vᴹ' (writeᴾ-valid u (vᴹ L) vᵗ')
  ... | vᴹ'' with valid-▻ (vᴹ'' H) (fork-valid step (vˢ' Σ., vᴾ) s≡f)
  ... | valid-forked = validˢ↪ s (ωⱽ P., vˢ' P., vᴹ') P., vˢ' Σ., updateᴹ-valid vᴹ'' valid-forked
  valid↪ {L} s@(CFork∙ L⊑H sch r step s≡f u) (ωⱽ P., vˢ P., vᴹ) with readᴾ-valid r (vᴹ L)
  ... | vᴾ with valid⟼ (vˢ P., vᴾ) step | step-⊆ˢ (vˢ Σ., vᴾ) step
  ... | _ P., vᵗ' | Σ₁⊆Σ₂ with validᴹ-⊆ vᴹ Σ₁⊆Σ₂
  ... | vᴹ' = validˢ↪ s (ωⱽ P., vˢ P., vᴹ') P., vˢ Σ., (updateᴹ-valid vᴹ' (writeᴾ-valid u (vᴹ' L) vᵗ'))
  valid↪ s@(Done sch r done) (ωⱽ P., vˢ P., vᴹ) = validˢ↪ s (ωⱽ P., vˢ P., vᴹ) P., vˢ Σ., vᴹ
  valid↪ s@(Stuck sch r stuck) (ωⱽ P., vˢ P., vᴹ) = validˢ↪ s (ωⱽ P., vˢ P., vᴹ) P., vˢ Σ., vᴹ

--------------------------------------------------------------------------------
