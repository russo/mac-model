import Lattice as L
import Scheduler as S
open import Scheduler.Security
import Concurrent.Valid as V

module Concurrent.Security {𝓛 : L.Lattice} {𝓢 : S.Scheduler 𝓛} (A : L.Label 𝓛) (𝓝 : NIˢ 𝓛 A 𝓢) (𝓥 : V.Validˢ 𝓛 𝓢) where

open import Concurrent.Security.LowEq A 𝓝 public
open import Concurrent.Security.PSNI A 𝓝 𝓥 public
