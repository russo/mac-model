-- This module proves that the concurrent calculus is deterministic.

import Lattice as L
import Scheduler as S

module Concurrent.Determinism (𝓛 : L.Lattice) (𝓢 : S.Scheduler 𝓛) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛
open import Sequential.Determinism 𝓛
open import Concurrent.Calculus 𝓛 𝓢
open import Concurrent.Semantics 𝓛 𝓢 public
open S.Scheduler 𝓛 𝓢

open import Data.Empty
open import Data.Product
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

--------------------------------------------------------------------------------
-- Helper lemmas

-- Reading from pools is deterministic. (We have to explicitly prove
-- this because we model the operation using an indexed data-type and
-- the relation may not be functional)
readᴾ-≡ : ∀ {n l} {P : Pool l} {t₁ t₂ : Thread l} -> n ↦ t₁ ∈ᴾ P -> n ↦ t₂ ∈ᴾ P -> t₁ ≡ t₂
readᴾ-≡ here here = refl
readᴾ-≡ (there x) (there y) = readᴾ-≡ x y

-- Updating pools is deterministic. (We have to explicitly prove this
-- because we model the operation using an indexed data-type and the
-- relation may not be functional)
updateᴾ-≡ : ∀ {n l} {P₁ P₂ P₃ : Pool l} {t : Thread l} -> P₂ ≔ P₁ [ n ↦ t ]ᴾ -> P₃ ≔ P₁ [ n ↦ t ]ᴾ -> P₂ ≡ P₃
updateᴾ-≡ here here = refl
updateᴾ-≡ (there x) (there y) rewrite updateᴾ-≡ x y = refl

-- A bunch of helper lemmas for step-≡

new-∅ : ∀ {Σ τ l h p'} {l⊑h : l ⊑ h} {t : CTerm τ} -> (step : ⟨ Σ , new l⊑h t ⟩ ⟼ p' ) -> event step ≡ ∅
new-∅ (Lift ())
new-∅ (New l⊑h) = refl

write-∅ : ∀ {Σ τ l h p' t'} {l⊑h : l ⊑ h} {t : CTerm τ} -> (step : ⟨ Σ , write l⊑h t t' ⟩ ⟼ p' ) -> event step ≡ ∅
write-∅ (Lift ())
write-∅ (Write₁ l⊑h step₂) = refl
write-∅ (Write₂ l⊑h w) = refl

newMVar-∅ : ∀ {Σ τ l h p'} {l⊑h : l ⊑ h} -> (step : ⟨ Σ , newMVar {τ = τ} l⊑h ⟩ ⟼ p' ) -> event step ≡ ∅
newMVar-∅ (Lift ())
newMVar-∅ (NewMVar l⊑h) = refl

take-∅ : ∀ {Σ l τ  t p'} (step : ⟨ Σ , take {l = l} {τ = τ} t ⟩ ⟼ p') -> event step ≡ ∅
take-∅ (Lift ())
take-∅ (Take₁ step) = refl
take-∅ (Take₂ r w) = refl

put-∅ : ∀ {Σ l τ t' t p'} (step : ⟨ Σ , put {l = l} {τ = τ} t t' ⟩ ⟼ p') -> event step ≡ ∅
put-∅ (Lift ())
put-∅ (Put₁ step) = refl
put-∅ (Put₂ r w) = refl

--------------------------------------------------------------------------------

-- Any two steps between the same terms yield the same sequential
-- event.
step-≡ : ∀ {l τ} {p₁ p₂ : Program (Mac l τ)} -> (step₁ step₂ : p₁ ⟼ p₂) -> event step₁ ≡ event step₂
step-≡ (Lift (App x)) (Lift x₁) = refl
step-≡ (Lift Beta) (Lift _) = refl
step-≡ (Lift (If₁ x)) (Lift _) = refl
step-≡ (Lift If₂) (Lift _) = refl
step-≡ (Lift If₃) (Lift _) = refl
step-≡ (Lift Hole) (Lift _) = refl
step-≡ (Label' l⊑h) (Lift x) = refl
step-≡ (Label' l⊑h) (Label' .l⊑h) = refl
step-≡ (Unlabel₁ l⊑h step₁) (Lift x) = refl
step-≡ (Unlabel₁ l⊑h step₁) (Unlabel₁ .l⊑h step₂) = refl
step-≡ (Unlabel₂ l⊑h) (Lift x) = refl
step-≡ (Unlabel₂ l⊑h) (Unlabel₂ .l⊑h) = refl
step-≡ (Unlabelχ l⊑h) (Lift x) = refl
step-≡ (Unlabelχ l⊑h) (Unlabelχ .l⊑h) = refl
step-≡ (Bind₁ step₁) (Lift ())
step-≡ (Bind₁ step₁) (Bind₁ step₂) = step-≡ step₁ step₂
step-≡ Bind₂ (Lift x) = refl
step-≡ Bind₂ Bind₂ = refl
step-≡ Bind₃ (Lift x) = refl
step-≡ Bind₃ Bind₃ = refl
step-≡ (Join l⊑h x) (Lift x₁) = refl
step-≡ (Join l⊑h x) (Join .l⊑h x₁) = refl
step-≡ (Joinχ l⊑h x) (Lift x₁) = refl
step-≡ (Joinχ l⊑h x) (Joinχ .l⊑h x₁) = refl
step-≡ (Join∙ l⊑h) (Lift x) = refl
step-≡ (Join∙ l⊑h) (Join∙ .l⊑h) = refl
step-≡ (Catch₁ step₁) (Lift ())
step-≡ (Catch₁ step₁) (Catch₁ step₂) = step-≡ step₁ step₂
step-≡ Catch₂ (Lift x) = refl
step-≡ Catch₂ Catch₂ = refl
step-≡ Catch₃ (Lift x) = refl
step-≡ Catch₃ Catch₃ = refl
step-≡ (New l⊑h) step₂ rewrite new-∅ step₂ = refl
step-≡ (Write₁ l⊑h step₁) (Lift x) = refl
step-≡ (Write₁ l⊑h step₁) (Write₁ .l⊑h step₂) = refl
step-≡ (Write₂ l⊑h w) step₂ rewrite write-∅ step₂ = refl
step-≡ (Read₁ l⊑h step₁) (Lift x) = refl
step-≡ (Read₁ l⊑h step₁) (Read₁ .l⊑h step₂) = refl
step-≡ (Read₂ l⊑h r) (Lift x) = refl
step-≡ (Read₂ l⊑h r) (Read₂ .l⊑h r₁) = refl
step-≡ (New∙ l⊑h) (Lift x) = refl
step-≡ (New∙ l⊑h) (New∙ .l⊑h) = refl
step-≡ (Write∙₁ l⊑h step₁) (Lift x) = refl
step-≡ (Write∙₁ l⊑h step₁) (Write∙₁ .l⊑h step₂) = refl
step-≡ (Write∙₂ l⊑h) (Lift x) = refl
step-≡ (Write∙₂ l⊑h) (Write∙₂ .l⊑h) = refl
step-≡ (Fork l⊑h) (Lift ())
step-≡ (Fork l⊑h) (Fork .l⊑h) = refl
step-≡ (Fork∙ l⊑h) (Lift ())
step-≡ (Fork∙ l⊑h) (Fork∙ .l⊑h) = refl
step-≡ (NewMVar l⊑h) step₂ rewrite newMVar-∅ step₂ = refl
step-≡ (Take₁ step₁) (Lift x) = refl
step-≡ (Take₁ step₁) (Take₁ step₂) = refl
step-≡ (Take₂ r w) step₂ rewrite take-∅ step₂ = refl
step-≡ (Put₁ step₁) (Lift x) = refl
step-≡ (Put₁ step₁) (Put₁ step₂) = refl
step-≡ (Put₂ r w) step₂ rewrite put-∅ step₂ = refl
step-≡ (NewMVar∙ l⊑h) (Lift x) = refl
step-≡ (NewMVar∙ l⊑h) (NewMVar∙ .l⊑h) = refl

-- Helper lemma
trans-≢ : ∀ {A : Set} {a b c : A} -> a ≡ b -> b ≡ c -> a ≢ c -> ⊥
trans-≢ eq₁ eq₂ ¬p = ¬p (trans eq₁ eq₂)

-- Determinisim of the concurrent semantics.
determinismᴳ : ∀ {l n} {c₁ c₂ c₃ : Global} -> (l , n) ⊢ c₁ ↪ c₂ -> (l , n) ⊢ c₁ ↪ c₃ -> c₂ ≡ c₃
determinismᴳ (Step sch r step s≡∅ u) (Step sch₁ r₁ step₁ s≡∅₁ u₁)
  rewrite determinismˢ sch sch₁ | readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite updateᴾ-≡ u u₁ = refl
determinismᴳ (Step sch r step s≡∅ u) (CFork L⊑H sch₁ r₁ step₁ s≡f u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite sym (step-≡ step step₁) = ⊥-elim (trans-≢ (sym s≡∅) s≡f (λ ()))
determinismᴳ (Step sch r step s≡∅ u) (CFork∙ L⊑H sch₁ r₁ step₁ s≡f u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite sym (step-≡ step step₁) = ⊥-elim (trans-≢ (sym s≡∅) s≡f (λ ()))
determinismᴳ (Step sch r step s≡∅ u) (Done sch₁ r₁ done)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (¬doneSteps done (Step step))
determinismᴳ (Step sch r step s≡∅ u) (Stuck sch₁ r₁ stuck)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₂ stuck (Step step))
determinismᴳ (CFork L⊑H sch r step s≡f u) (Step sch₁ r₁ step₁ s≡∅ u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite sym (step-≡ step step₁) = ⊥-elim (trans-≢ (sym s≡∅) s≡f (λ ()))
determinismᴳ (CFork L⊑H sch r step s≡f u) (CFork L⊑H₁ sch₁ r₁ step₁ s≡f₁ u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl with trans (sym s≡f) (trans (step-≡ step step₁) s≡f₁)
... | refl rewrite updateᴾ-≡ u u₁ | determinismˢ sch sch₁ = refl
determinismᴳ (CFork L⊑H sch r step s≡f u) (CFork∙ L⊑H₁ sch₁ r₁ step₁ s≡f₁ u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite sym (step-≡ step step₁) = ⊥-elim (trans-≢ (sym s≡f₁) s≡f (λ ()))
determinismᴳ (CFork L⊑H sch r step s≡f u) (Done sch₁ r₁ done)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (¬doneSteps done (Step step))
determinismᴳ (CFork L⊑H sch r step s≡f u) (Stuck sch₁ r₁ stuck)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₂ stuck (Step step))
determinismᴳ (CFork∙ L⊑H sch r step s≡f u) (Step sch₁ r₁ step₁ s≡∅ u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite sym (step-≡ step step₁) = ⊥-elim (trans-≢ (sym s≡∅) s≡f (λ ()))
determinismᴳ (CFork∙ L⊑H sch r step s≡f u) (CFork L⊑H₁ sch₁ r₁ step₁ s≡f₁ u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite sym (step-≡ step step₁) = ⊥-elim (trans-≢ (sym s≡f₁) s≡f (λ ()))
determinismᴳ (CFork∙ L⊑H sch r step s≡f u) (CFork∙ L⊑H₁ sch₁ r₁ step₁ s≡f₁ u₁)
  rewrite readᴾ-≡ r r₁ with determinismᶜ step step₁
... | refl rewrite determinismˢ sch sch₁ | updateᴾ-≡ u u₁ = refl
determinismᴳ (CFork∙ L⊑H sch r step s≡f u) (Done sch₁ r₁ done)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (¬doneSteps done (Step step))
determinismᴳ (CFork∙ L⊑H sch r step s≡f u) (Stuck sch₁ r₁ stuck)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₂ stuck (Step step))
determinismᴳ (Done sch r done) (Step sch₁ r₁ step s≡∅ u)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (¬doneSteps done (Step step))
determinismᴳ (Done sch r done) (CFork L⊑H sch₁ r₁ step s≡f u)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (¬doneSteps done (Step step))
determinismᴳ (Done sch r done) (CFork∙ L⊑H sch₁ r₁ step s≡f u)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (¬doneSteps done (Step step))
determinismᴳ (Done sch r done) (Done sch₁ r₁ done₁)
  rewrite readᴾ-≡ r r₁ | determinismˢ sch sch₁ = refl
determinismᴳ (Done sch r done) (Stuck sch₁ r₁ stuck)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₁ stuck done)
determinismᴳ (Stuck sch r stuck) (Step sch₁ r₁ step s≡∅ u)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₂ stuck (Step step))
determinismᴳ (Stuck sch r stuck) (CFork L⊑H sch₁ r₁ step s≡f u)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₂ stuck (Step step))
determinismᴳ (Stuck sch r stuck) (CFork∙ L⊑H sch₁ r₁ step s≡f u)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₂ stuck (Step step))
determinismᴳ (Stuck sch r stuck) (Done sch₁ r₁ done)
  rewrite readᴾ-≡ r r₁ = ⊥-elim (proj₁ stuck done)
determinismᴳ (Stuck sch r stuck) (Stuck sch₁ r₁ stuck₁)
  rewrite readᴾ-≡ r r₁ | determinismˢ sch sch₁ = refl
