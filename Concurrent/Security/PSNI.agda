-- In this module we prove scheduler parametric progress-sensitve
-- noninterference.

import Lattice as L
import Scheduler as S
open import Scheduler.Security
import Concurrent.Valid as V

module Concurrent.Security.PSNI {𝓛 : L.Lattice} {𝓢 : S.Scheduler 𝓛} (A : L.Label 𝓛) (𝓝 : NIˢ 𝓛 A 𝓢) (𝓥 : V.Validˢ 𝓛 𝓢) where

open import Data.Product as P
open import Data.Nat
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

open import Types 𝓛
open import Concurrent.Calculus 𝓛 𝓢
open import Concurrent.Security.Simulation A 𝓝
open import Concurrent.Security.Progress A 𝓝
open import Concurrent.Determinism 𝓛 𝓢
module N = Scheduler.Security.NIˢ 𝓝
open import Scheduler.Base 𝓛

import Concurrent.Security.LowEq
module L₁ = Concurrent.Security.LowEq A 𝓝
open L₁

import Sequential.Security.LowEq
module L₂ =  Sequential.Security.LowEq 𝓛 A
open L₂

open import Concurrent.Valid 𝓛 𝓢
open V.Valid↪ 𝓛 𝓢 𝓥

-- Low-equivalence preservation (low steps).  If two low-equivalent
-- configuration step with the same low-thread, then the resulting
-- configurations are low-equivalent.
squareᴸ : ∀ {l n} {c₁ c₁' c₂ c₂' : Global} {{v₁ : validᴳ c₁}} {{v₂ : validᴳ c₂}} ->
            (l ⊑ A) -> c₁ ≈ᴳ c₂ -> ( l , n ) ⊢ c₁ ↪ c₁' -> ( l , n ) ⊢ c₂ ↪ c₂' -> c₁' ≈ᴳ c₂'
squareᴸ l⊑A c₁≈c₂ step₁ step₂ = ⌜ aux ⌞ c₁≈c₂ ⌟ᴳ (εᴳ-simᴸ l⊑A step₁) (εᴳ-simᴸ l⊑A step₂) ⌝ᴳ
  where aux : ∀ {n l} {c₁ c₁' c₂ c₂' : Global} -> c₁ ≡ c₂ -> (l , n) ⊢ c₁ ↪ c₁' -> (l , n) ⊢ c₂ ↪ c₂' -> c₁' ≡ c₂'
        aux refl x y = determinismᴳ x y

-- Low-equivalence preservation (high step).
-- If c₂ is 1 + j step behind c₁, then a high thread is scheduled
-- first in (H, m) ⊢ c₂ ↪ c₂' By using the scheduler requirements
-- (no-starvation and no-observable effect), we prove annotated
-- low-equivalence preservation, i.e., c₁ ≈ c₂' and c₂' is j step
-- behind c₁.
triangleᴴ : ∀ {L H i j n m} {c₁ c₁' c₂ c₂' : Global} {{v₁ : validᴳ c₁}} {{v₂ : validᴳ c₂}} ->
            L ⊑ A -> c₁ ≈ᴳ⟨ i , suc j ⟩ c₂ -> ( L , n ) ⊢ c₁ ↪ c₁' -> ( H , m ) ⊢ c₂ ↪ c₂' -> c₁ ≈ᴳ⟨ i , j ⟩ c₂'
triangleᴴ L⊑A L₁.⟨ ω₁≈ω₂ , Σ₁≈Σ₂ , Φ₁≈Φ₂ ⟩ step₁ step₂ with N.triangleˢ L⊑A ω₁≈ω₂ (getSchStep step₁) (getSchStep step₂)
... | H⋤A , ω₁≈ω₂' with ⌜ εᴳ-simᴴ H⋤A step₂ ⌝ᴳ
... | L₁.⟨ ω₂≈ω₂' , Σ₂≈Σ₂' , Φ₂≈Φ₂' ⟩
  = L₁.⟨ ω₁≈ω₂' , L₂.trans-≈ˢ Σ₁≈Σ₂ Σ₂≈Σ₂' , trans-≈ᴹ Φ₁≈Φ₂ Φ₂≈Φ₂' ⟩

-- If configuration c₁ runs with a low thread ((l , n) ⊢ c₁ ↪ c₁') and
-- c₂ is low-equivalent and n₂ steps behind (i.e., c₁ ≈ᴳ⟨ n₁ , n₂ ⟩
-- c₂), then in at most n₂ steps c₂ catches up (c₂ ↪⋆ c₂') to a
-- low-equivalent configuration c₁' ≈ c₂'.
εᴳ-simᴸ⋆ : ∀ {L n n₁} {c₁ c₁' c₂ : Global} {{v₁ : validᴳ c₁}} {{v₂ : validᴳ c₂}} (n₂ : ℕ) ->
             L ⊑ A -> (L , n)  ⊢ c₁ ↪ c₁' -> c₁ ≈ᴳ⟨ n₁ , n₂ ⟩ c₂ -> ∃ (λ c₂' → c₁' ≈ᴳ c₂' × c₂ ↪⋆ c₂')
εᴳ-simᴸ⋆ zero L⊑A step c₁≈c₂ with progressᴸ L⊑A c₁≈c₂ step
... | Step step' = _ , (squareᴸ L⊑A (forgetᴳ c₁≈c₂) step step') , (step' ∷ [])
εᴳ-simᴸ⋆ {{v₂ = v₂}} (suc n₂) L⊑A step c₁≈c₂  with progressᴴ L⊑A c₁≈c₂ step
... | (H , m) , (Step step') with triangleᴴ L⊑A c₁≈c₂ step step'
... | c₁≈c₂' with εᴳ-simᴸ⋆ {{v₂ = valid↪ step' v₂}} n₂ L⊑A step c₁≈c₂'
... |  c₂'' , c₁≈c₂'' , ss = c₂'' , c₁≈c₂'' , step' ∷ ss

-- The actual proof depends on whether we are matching a low or a high step.
-- This helper lemma choose the correct one.
εᴳ-sim⋆ : ∀ {l n} {c₁ c₁' c₂ : Global} {{v₁ : validᴳ c₁}} {{v₂ : validᴳ c₂}}
            -> Dec (l ⊑ A) -> ( l , n ) ⊢ c₁ ↪ c₁' -> c₁ ≈ᴳ c₂ -> ∃ (λ c₂' → c₁' ≈ᴳ c₂' × c₂ ↪⋆ c₂')
εᴳ-sim⋆ (yes L⊑A) step x = εᴳ-simᴸ⋆ _ L⊑A step (alignᴳ x)
εᴳ-sim⋆ {c₁ = c₁} {c₁' = c₁'} {c₂ = c₂} (no H⋤A) stepᴴ c₁≈c₂
  = c₂ , (trans-≈ᴳ (sym-≈ᴳ (⌜ εᴳ-simᴴ H⋤A stepᴴ ⌝ᴳ)) c₁≈c₂) , []

-- Progress-sensitive noninterference (PSNI)
psni : ∀ {l n} {c₁ c₁' c₂ : Global} {{v₁ : validᴳ c₁}} {{v₂ : validᴳ c₂}}
         -> c₁ ≅ᴳ c₂ -> (l , n)  ⊢ c₁ ↪ c₁' -> ∃ (λ c₂' → c₂ ↪⋆ c₂' × c₁' ≅ᴳ c₂')
psni {l} eq s with εᴳ-sim⋆ (l ⊑? A) s ⌜ eq ⌝ᴳ
psni eq s | c₂' , c₁'≈ᴳc₂' ,  c₂↪⋆c₂' = c₂' , c₂↪⋆c₂' , ⌞ c₁'≈ᴳc₂' ⌟ᴳ

--------------------------------------------------------------------------------
