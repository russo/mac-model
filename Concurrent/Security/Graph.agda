-- This module defines the graph of the erasure function for the
-- concurrent part of the calculus. The module follows closely the
-- same structure of Sequential.Security.Graph, where we first define
-- the accessability predicate and show that it corresponds exactly to
-- the erasure function proving a double implication (lift and unlift)

import Lattice as L
import Scheduler as S
open import Scheduler.Security hiding (εᴹ)

module Concurrent.Security.Graph {𝓛 : L.Lattice} {𝓢 : S.Scheduler 𝓛} (A : L.Label 𝓛) (𝓝 : NIˢ 𝓛 A 𝓢) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Security.Graph 𝓛 A hiding (Eraseᴾ ; lift-εᴾ ; unlift-εᴾ ; Eraseᴹ ; lift-εᴹ ; unlift-εᴹ)
open import Concurrent.Calculus 𝓛 𝓢
open import Concurrent.Security.Erasure A 𝓝

open import Data.Empty
open import Data.Product
open import Data.Sum
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

data Eraseᴾ {l} : Pool l -> Pool l -> Set where
  [] : Eraseᴾ [] []
  _◅_ : ∀ {t₁ t₂ P₁ P₂} -> Erase t₁ t₂ -> Eraseᴾ P₁ P₂ -> Eraseᴾ (t₁ ◅ P₁) (t₂ ◅ P₂)
  ∙ : Eraseᴾ ∙ ∙

lift-εᴾ : ∀ {l} (P : Pool l) -> Eraseᴾ P (εᴾ P)
lift-εᴾ [] = []
lift-εᴾ (t ◅ P) = (lift-ε t) ◅ (lift-εᴾ P)
lift-εᴾ ∙ = ∙


unlift-εᴾ : ∀ {l} {P P' : Pool l} -> Eraseᴾ P P' -> P' ≡ εᴾ P
unlift-εᴾ [] = refl
unlift-εᴾ (e₁ ◅ e₂) with unlift-ε e₁ | unlift-εᴾ e₂
... | eq₁ | eq₂ rewrite eq₁ | eq₂ = refl
unlift-εᴾ ∙ = refl

--------------------------------------------------------------------------------

Eraseᴹ : Mapᴾ -> Mapᴾ -> Set
Eraseᴹ Φ Φ' = (l : Label) -> (l ⊑ A × Eraseᴾ (Φ l) (Φ' l)) ⊎ ( l ⋤ A × Φ' l ≡ ∙)

lift-εᴹ : (Φ : Mapᴾ) -> Eraseᴹ Φ (εᴹ Φ)
lift-εᴹ Φ l with l ⊑? A
... | yes l⊑A = inj₁ (l⊑A , lift-εᴾ (Φ l))
... | no l⋤A = inj₂ (l⋤A , refl)

unlift-εᴹ : {Φ Φ' : Mapᴾ} -> Eraseᴹ Φ Φ' -> εᴹ Φ ≡ Φ'
unlift-εᴹ {Φ} {Φ'} e = fun-ext proof
  where proof : (l : Label) -> εᴹ Φ l ≡ Φ' l
        proof l with e l | l ⊑? A
        proof l | inj₁ (l⊑A , e) | yes _ rewrite unlift-εᴾ e = refl
        proof l | inj₁ (l⊑A , e) | no l⋤A = ⊥-elim (l⋤A l⊑A)
        proof l | inj₂ (l⋤A , eq) | yes l⊑A = ⊥-elim (l⋤A l⊑A)
        proof l | inj₂ (l⋤A , eq) | no _ rewrite eq = refl

--------------------------------------------------------------------------------
