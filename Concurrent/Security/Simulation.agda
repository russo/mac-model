import Lattice as L
import Scheduler as S
import Scheduler.Security as Sch hiding (εᴹ) renaming (εᴱ to εᴱ')

module Concurrent.Security.Simulation {𝓛 : L.Lattice} {𝓢 : S.Scheduler 𝓛} (A : L.Label 𝓛) (𝓝 : Sch.NIˢ 𝓛 A 𝓢) where

open import Data.Product as P
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

open import Types 𝓛
open import Sequential.Security.Erasure 𝓛 A as E hiding (εᴹ ; εᴾ)
open import Sequential.Security 𝓛 A renaming (⌞_⌟ˢ to store-⌞_⌟)
open import Sequential.Security.Simulation 𝓛 A
open import Sequential.Security.Progress 𝓛 A

open import Concurrent.Calculus 𝓛 𝓢 as C
open import Concurrent.Semantics 𝓛 𝓢
open import Concurrent.Security.Erasure  A 𝓝
open import Concurrent.Security.LowEq A 𝓝
open import Concurrent.Valid 𝓛 𝓢 as V

open import Scheduler.Base 𝓛
open Sch 𝓛 A
open Sch.NIˢ 𝓝

open import Data.Nat hiding (_≟_)
open import Data.Empty


--------------------------------------------------------------------------------
-- Restricted Simulation.
-- 1-step simulation for low concurrent steps
εᴳ-simᴸ : ∀ {L n} {c c' : Global} {{v : validᴳ c}} -> L ⊑ A -> ( L , n ) ⊢ c ↪ c' -> ( L , n ) ⊢ (εᴳ c) ↪ (εᴳ c')

εᴳ-simᴸ {c = C.⟨ ω , Σ , Φ ⟩} L⊑A (Step {l = L} {P' = P'} sch r step s≡∅ u)
  rewrite εᴹ-updateᴸ-≡ Φ P' L⊑A with εᴾ-read Φ L⊑A r | εᴾ-write Φ L⊑A u | εᴹ-εᴾ-≡ Φ L⊑A
... | r' | u' | eq rewrite eq = Step (εˢ-simᴸ L⊑A sch) r' (εᴾ-sim (yes L⊑A) step) (ε-event L⊑A step s≡∅) u'

-- Fork
εᴳ-simᴸ {c = C.⟨ ω , Σ , Φ ⟩} L⊑A (CFork {H = H} {tᴴ = tᴴ} {P' = P'} L⊑H sch r step s≡f u)
  with H ⊑? A

-- H ⊑ A
εᴳ-simᴸ {L} {c = C.⟨ ω , Σ , Φ ⟩} L⊑A (CFork {nᴸ = nᴸ} {H = H} {ω' = ω'} {tᴴ = tᴴ} {P' = P'} L⊑H sch r step s≡f u)
  | yes H⊑A rewrite εᴹ-updateᴸ-≡ (Φ [ L ↦ P' ]ᴹ) ((Φ [ L ↦ P' ]ᴹ) H ▻ tᴴ) H⊑A
                   | εᴹ-updateᴸ-≡ Φ P' L⊑A | εᴾ-▻ (Φ [ L ↦ P' ]ᴹ) H⊑A tᴴ
  with εᴾ-read Φ L⊑A r | εᴾ-write Φ L⊑A u  | εᴹ-εᴾ-≡ Φ L⊑A
... | r' | u' | eq₁ rewrite eq₁ with ε-event L⊑A step s≡f | εᴱ-forkᴸ-≡ L⊑H H⊑A tᴴ
... | s≡f' | eq₂ rewrite eq₂
  with εˢ-simᴸ L⊑A sch | εᴱ-Forkᴸ-≡ L⊑H H⊑A (lengthᴾ ((Φ [ L ↦ P' ]ᴹ) H)) | εᴾ-lengthᴾ-≡ (Φ [ L ↦ P' ]ᴹ) H⊑A
... | sch' | eq₃ | eq₄ rewrite eq₃ | sym eq₄ | εᴹ-read-updateᴸ-≡ Φ P' L⊑A H⊑A
  = CFork L⊑H sch' r' (εᴾ-sim (yes L⊑A) step) s≡f' u'

-- H ⋤ A
εᴳ-simᴸ {L} {c = C.⟨ ω , Σ , Φ ⟩} L⊑A (CFork {H = H} {tᴴ = tᴴ} {P' = P'} L⊑H sch r step s≡f u) | no H⋤A
  rewrite εᴹ-updateᴴ-≡ (Φ [ L ↦ P' ]ᴹ) (((Φ [ L ↦ P' ]ᴹ) H) ▻ tᴴ) H⋤A | εᴹ-updateᴸ-≡ Φ P' L⊑A
    with εᴾ-read Φ L⊑A r | εᴾ-write Φ L⊑A u  | εᴹ-εᴾ-≡ Φ L⊑A
... | r' | u' | eq₁ rewrite eq₁ with εˢ-simᴸ L⊑A sch | εᴱ-Forkᴴ-≡ L⊑H H⋤A (lengthᴾ ((Φ [ L ↦ P' ]ᴹ) H))
... | sch' | eq₂ rewrite eq₂ with ε-event L⊑A step s≡f | εᴱ-forkᴴ-≡ L⊑H H⋤A tᴴ
... | e≡s | eq₃ rewrite eq₃ =  CFork∙ L⊑H sch' r' (εᴾ-sim (yes L⊑A) step) e≡s u'

εᴳ-simᴸ L⊑A (CFork∙ {Φ = Φ} {P' = P'} L⊑H sch r step s≡f u)
  rewrite εᴹ-updateᴸ-≡ Φ P' L⊑A with εᴾ-read Φ L⊑A r | εᴾ-write Φ L⊑A u | εᴹ-εᴾ-≡ Φ L⊑A
... | r' | u' | eq rewrite eq = CFork∙ L⊑H (εˢ-simᴸ L⊑A sch) r' (εᴾ-sim (yes L⊑A) step) (ε-event L⊑A step s≡f) u'
εᴳ-simᴸ L⊑A (Done {Φ = Φ} sch r done)
  with εᴾ-read Φ L⊑A r | εᴹ-εᴾ-≡ Φ L⊑A
... | r' | eq rewrite eq = Done (εˢ-simᴸ L⊑A sch) r' (val-ε done)
εᴳ-simᴸ {L} {{_ , vˢ , vᴹ}} L⊑A (Stuck {Σ = Σ} {Φ = Φ} {t = t} sch r stuck) with εᴾ-read Φ L⊑A r | εᴹ-εᴾ-≡ Φ L⊑A
... | r' | eq rewrite eq = Stuck (εˢ-simᴸ L⊑A sch) r' (stuck-ε {{ vˢ , readᴾ-valid r (vᴹ L) }} L⊑A stuck)

-- No Observable Effect.
-- When the global configuration runs a high thread, the new
-- configuration is low-equivalent to the intial configuration, i.e.,
-- the high step does not leak.
εᴳ-simᴴ : ∀ {H n} {c₁ c₂ : Global} -> H ⋤ A -> ( H , n ) ⊢ c₁ ↪ c₂ -> c₁ ≅ᴳ c₂
εᴳ-simᴴ H⋤A (Step {Φ = Φ} {P' = P'} sch r step s≡∅ u)
 rewrite ⌞ εˢ-simᴴ H⋤A sch ⌟ˢ | stepᴴ-≡ H⋤A step | εᴹ-updateᴴ-≡ Φ P' H⋤A = refl
εᴳ-simᴴ {L} L⋤A (CFork {H = H} {Φ = Φ} {tᴴ = tᴴ} {P' = P'} L⊑H sch r step s≡f u)
 rewrite ⌞ εˢ-simᴴ L⋤A sch ⌟ˢ | stepᴴ-≡ L⋤A step
         | εᴹ-updateᴴ-≡ (Φ [ L ↦ P' ]ᴹ) ((Φ [ L ↦ P' ]ᴹ) H ▻ tᴴ) (trans-⋤ L⊑H L⋤A) | εᴹ-updateᴴ-≡ Φ P' L⋤A = refl
εᴳ-simᴴ H⋤A (CFork∙ {Φ = Φ} {P' = P'} L⊑H sch r step s≡f u) rewrite ⌞ εˢ-simᴴ H⋤A sch ⌟ˢ |  εᴹ-updateᴴ-≡ Φ P' H⋤A = refl
εᴳ-simᴴ H⋤A (Done sch r done) rewrite ⌞ εˢ-simᴴ H⋤A sch ⌟ˢ = refl
εᴳ-simᴴ H⋤A (Stuck sch r stuck) rewrite ⌞ εˢ-simᴴ H⋤A sch ⌟ˢ = refl

--------------------------------------------------------------------------------
