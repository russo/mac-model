-- This module defines the erasure function for the concurrent
-- calculus and proves a few basic lemmas about it.  The module
-- parameter A denotes the security level of the attacker and 𝓝
-- denotes a non-interfering scheduler that provides a scheduler state
-- specific erasure function.

import Lattice as L
import Scheduler as S
open import Scheduler.Security hiding (εᴹ ; εᴱ)

module Concurrent.Security.Erasure {𝓛 : L.Lattice} {𝓢 : S.Scheduler 𝓛} (A : L.Label 𝓛) (𝓝 : NIˢ 𝓛 A 𝓢) where

open import Relation.Binary.PropositionalEquality
open import Data.Empty
open import Data.Nat hiding (_≟_)
open import Relation.Nullary

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Security.Erasure 𝓛 A as Seq hiding (εᴾ ; εˢ ; εᴹ)
open import Concurrent.Calculus 𝓛 𝓢
open import Concurrent.Semantics 𝓛 𝓢
open Scheduler.Security.NIˢ 𝓝

--------------------------------------------------------------------------------

-- Pools are erased homomorphically.
εᴾ : ∀ {l} -> Pool l -> Pool l
εᴾ  [] = []
εᴾ (t ◅ P) = ε t ◅ εᴾ P
εᴾ ∙ = ∙

-- Sensitive pools are collapsed to ∙, otherwise they are erased homomorphically.
εᴹ : Mapᴾ -> Mapᴾ
εᴹ Φ l with l ⊑? A
... | yes l⊑A = εᴾ (Φ l)
... | no l⋤A = ∙

-- The erasure for global configuration erases each component.
εᴳ : Global -> Global
εᴳ ⟨ ω , Σ , Φ ⟩ = ⟨ εˢ ω , Seq.εˢ Σ , εᴹ Φ ⟩

-- Erasure of sequential events.
εᴱ : ∀ {l} -> Event l -> Event l
εᴱ ∅ = ∅
εᴱ (fork {h = H} l⊑h t) with H ⊑? A
... | yes H⊑A = fork l⊑h (ε t)
... | no H⋤A = fork∙ l⊑h (ε t)
εᴱ (fork∙ l⊑h t) = fork∙ l⊑h (ε t)

--------------------------------------------------------------------------------
-- Helper Lemmas

-- Read and erase is the same as erase first and read then for low thread pools.
εᴹ-εᴾ-≡ : {L : Label} (Φ : Mapᴾ) -> L ⊑ A ->
          let P = Φ L in εᴾ P ≡ εᴹ Φ L
εᴹ-εᴾ-≡ {L} Φ L⊑A with L ⊑? A
... | yes _ = refl
... | no l⋤A = ⊥-elim (l⋤A L⊑A)

-- If we read a thread from a low thread pool, then we read the erased
-- thread from the erased thread pool.
εᴾ-read : ∀ {L n} {t : Thread L} -> (Φ : Mapᴾ) -> L ⊑ A ->
           let P = Φ L in
              n ↦ t ∈ᴾ P -> n ↦ (ε t) ∈ᴾ (εᴾ P)
εᴾ-read {L} {n} {t} Φ L⊑A r = proof r
  where proof : ∀ {n} {P : Pool L} -> n ↦ t ∈ᴾ P -> n ↦ (ε t) ∈ᴾ (εᴾ P)
        proof here = here
        proof (there r) = there (proof r)

--  Lifts an update to a low thread pool to work with an erased thread
--  and erased thread pools.
εᴾ-write : ∀ {L n} {P₂ : Pool L} {t : Thread L} -> (Φ : Mapᴾ) -> L ⊑ A ->
           let P₁ = Φ L in
             P₂ ≔ P₁ [ n ↦ t ]ᴾ -> (εᴾ P₂) ≔ (εᴾ P₁) [ n ↦ ε t ]ᴾ
εᴾ-write {L} {P₂ = P₂} {t} Φ L⊑A = proof
  where proof : ∀ {n} {P₁ P₂ : Pool L} -> P₂ ≔ P₁ [ n ↦ t ]ᴾ -> (εᴾ P₂) ≔ (εᴾ P₁) [ n ↦ ε t ]ᴾ
        proof here = here
        proof (there w) = there (proof w)

-- Erasure is homomorphic over the extension of low thread pools.
εᴾ-▻ : ∀ {H} -> (Φ : Mapᴾ) (H⊑A : H ⊑ A) (tᴴ : Thread H) ->
       let P = Φ H in
        εᴾ (Φ H ▻ tᴴ) ≡  (εᴹ Φ) H ▻ (ε tᴴ)
εᴾ-▻ {H} Φ H⊑A tᴴ with H ⊑? A
εᴾ-▻ {H} Φ H⊑A tᴴ | no ¬p = ⊥-elim (¬p H⊑A)
εᴾ-▻ {H} Φ H⊑A tᴴ | yes p = proof (Φ H)
  where proof : ∀ (P : Pool H) -> εᴾ (P ▻ tᴴ) ≡ εᴾ P ▻ ε tᴴ
        proof [] = refl
        proof (t ◅ P) rewrite proof P = refl
        proof ∙ = refl

-- Erasure is homomorphic with respect to the combination of update and read of low memories.
εᴹ-read-updateᴸ-≡ : ∀ {L H} (Φ : Mapᴾ) (P' : Pool L) → L ⊑ A → H ⊑ A →  (εᴹ (Φ [ L ↦ P' ]ᴹ )) H ≡ ((εᴹ Φ) [ L ↦ εᴾ P' ]ᴹ) H
εᴹ-read-updateᴸ-≡ {L} Φ P' L⊑A H⊑A with L ⊑? A
εᴹ-read-updateᴸ-≡ {L} {H} Φ P' L⊑A H⊑A | yes p with H ⊑? A
εᴹ-read-updateᴸ-≡ {L} {H} Φ P' L⊑A H⊑A | yes p | yes p₁ with L ≟ H
εᴹ-read-updateᴸ-≡ {L} {H} Φ P' L⊑A H⊑A | yes p | yes p₁ | yes refl = refl
εᴹ-read-updateᴸ-≡ {L} {H} Φ P' L⊑A H⊑A | yes p | yes p₁ | no ¬p with H ⊑? A
εᴹ-read-updateᴸ-≡ {L} Φ P' L⊑A H⊑A | yes p | yes p₁ | no ¬p | yes p₂ = refl
εᴹ-read-updateᴸ-≡ {L} Φ P' L⊑A H⊑A | yes p | yes p₁ | no ¬p | no ¬p₁ = ⊥-elim (¬p₁ p₁)
εᴹ-read-updateᴸ-≡ {L} Φ P' L⊑A H⊑A | yes p | no ¬p = ⊥-elim (¬p H⊑A)
εᴹ-read-updateᴸ-≡ {L} Φ P' L⊑A H⊑A | no ¬p = ⊥-elim (¬p L⊑A)


-- The length of a low thread pool is not affected by the erasure
-- function.
εᴾ-lengthᴾ-≡ : ∀ {H} -> (Φ : Mapᴾ) (H⊑A : H ⊑ A) ->
             let P = Φ H in
               lengthᴾ (εᴹ Φ H) ≡ lengthᴾ P
εᴾ-lengthᴾ-≡ {H} Φ H⊑A with H ⊑? A
εᴾ-lengthᴾ-≡ {H} Φ H⊑A | no ¬p = ⊥-elim (¬p H⊑A)
εᴾ-lengthᴾ-≡ {H} Φ H⊑A | yes p = proof (Φ H)
  where proof : ∀ (P : Pool H) -> lengthᴾ (εᴾ P) ≡ lengthᴾ P
        proof [] = refl
        proof (t ◅ P) rewrite proof P = refl
        proof ∙ = refl

-- The erasure function is homomorphic over thread pool maps that are
-- updated in the low pools.
εᴹ-updateᴸ-≡ : ∀ {L} (Φ : Mapᴾ) (P' : Pool L) (L⊑A : L ⊑ A) ->
              let P = Φ L in
              εᴹ (Φ [ L ↦ P' ]ᴹ) ≡ ((εᴹ Φ) [ L ↦ εᴾ P' ]ᴹ)
εᴹ-updateᴸ-≡ {L} Φ P' L⊑A = fun-ext proof
  where proof : (l : Label) -> εᴹ (Φ [ L ↦ P' ]ᴹ) l ≡ (εᴹ Φ [ L ↦ εᴾ P' ]ᴹ) l
        proof l with l ⊑? A
        proof l | yes l⊑A with L ≟ l
        proof l | yes l⊑A | yes refl = refl
        proof l | yes l⊑A | no ¬p with L ≟ l
        proof l | yes l⊑A | no ¬p | yes refl = ⊥-elim (¬p refl)
        proof l | yes l⊑A | no ¬p | no ¬p₁ with l ⊑? A
        proof l | yes l⊑A | no ¬p | no ¬p₁ | yes p = refl
        proof l | yes l⊑A | no ¬p | no ¬p₁ | no ¬p₂ = ⊥-elim (¬p₂ l⊑A)
        proof l | no l⋤A with L ≟ l
        proof l | no l⋤A | yes refl = ⊥-elim (l⋤A L⊑A)
        proof l | no l⋤A | no ¬p with l ⊑? A
        proof l | no l⋤A | no ¬p | yes p = ⊥-elim (l⋤A p)
        proof l | no l⋤A | no ¬p | no ¬p₁ = refl

-- Updating high thread pools does not change the pool map.
εᴹ-updateᴴ-≡ : ∀ {H} (Φ : Mapᴾ) (P' : Pool H) (H⋤A : H ⋤ A) ->
              let P = Φ H in
              εᴹ (Φ [ H ↦ P' ]ᴹ) ≡ (εᴹ Φ)
εᴹ-updateᴴ-≡ {H} Φ P' H⋤A = fun-ext proof
  where proof : ∀ (l : Label) → εᴹ (Φ [ H ↦ P' ]ᴹ) l ≡ εᴹ Φ l
        proof l with l ⊑? A
        proof l | yes H⊑A with H ≟ l
        proof l | yes H⊑A | yes refl = ⊥-elim (H⋤A H⊑A)
        proof l | yes H⊑A | no ¬p = refl
        proof l | no ¬p = refl

--------------------------------------------------------------------------------

-- Low fork events, i.e., fork(t) are erased homomorphically, i.e., fork(ε(t)).
εᴱ-forkᴸ-≡ : ∀ {L H} -> (L⊑H : L ⊑ H) (H⊑A : H ⊑ A) (t : Thread H) -> εᴱ (fork L⊑H t) ≡ fork L⊑H (ε t)
εᴱ-forkᴸ-≡ {H = H} L⊑H H⊑A t with H ⊑? A
... | yes _ = refl
... | no H⋤A = ⊥-elim (H⋤A H⊑A)

-- High fork (sequential) events are erased to fork∙ (sequential) events.
εᴱ-forkᴴ-≡ : ∀ {L H} -> (L⊑H : L ⊑ H) (H⋤A : H ⋤ A) (t : Thread H) -> εᴱ (fork L⊑H t) ≡ fork∙ L⊑H (ε t)
εᴱ-forkᴴ-≡ {H = H} L⊑H H⋤A t with H ⊑? A
... | yes H⊑A =  ⊥-elim (H⋤A H⊑A)
... | no _ = refl

open import Sequential.Semantics 𝓛
open import Sequential.Security.Simulation 𝓛 A
open import Sequential.Security.Graph 𝓛 A

-- If a low sequential step triggers a sequential event, the erased
-- step triggers the corresponding erased event.
ε-event : ∀ {L τ e} {p p' : Program (Mac L τ)} (L⊑A : L ⊑ A) ->
           (step : p ⟼ p') ->  event step ≡ e -> event (εᴾ-sim (yes L⊑A) step) ≡ (εᴱ e)
ε-event {L} {τ} {e} {p} {p'} L⊑A step s≡∅ = aux step s≡∅ (lift-εᴾ (yes L⊑A) p) (lift-εᴾ (yes L⊑A) p') (εᴾ-sim (yes L⊑A) step)
  where aux : ∀ {e τ} {p p' pᵉ pᵉ' : Program (Mac L τ)}
                        (step : p ⟼ p') -> event step ≡ e -> Eraseᴾ (yes L⊑A) p pᵉ -> Eraseᴾ (yes L⊑A) p' pᵉ' ->
                        (step' : pᵉ ⟼ pᵉ') -> event step' ≡ (εᴱ e)
        -- Another function that would be better discharged with proof automation
        aux (Lift (App x)) refl (lowᴾ _ eˢ (e ∘ e₁)) (lowᴾ _ eˢ₁ (e₂ ∘ e₃)) (Lift x₁) = refl
        aux (Lift Beta) refl (lowᴾ _ eˢ (e ∘ e₂)) (lowᴾ _ eˢ₁ e₁) (Lift x) = refl
        aux (Lift (If₁ x)) refl (lowᴾ _ eˢ (if e then e₁ else e₂)) (lowᴾ _ eˢ₁ (if e₃ then e₄ else e₅)) (Lift x₁) = refl
        aux (Lift If₂) refl (lowᴾ _ eˢ (if e then e₂ else e₃)) (lowᴾ _ eˢ₁ e₁) (Lift x) = refl
        aux (Lift If₃) refl (lowᴾ _ eˢ (if e then e₂ else e₃)) (lowᴾ _ eˢ₁ e₁) (Lift x) = refl
        aux (Lift Hole) refl (lowᴾ _ eˢ ∙) (lowᴾ _ eˢ₁ e₁) (Lift x) = refl
        aux (Label' l⊑h) refl (lowᴾ _ eˢ (label h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Label' l⊑h) refl (lowᴾ _ eˢ (label h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Label' .l⊑h) = refl
        aux (Label' l⊑h) refl (lowᴾ _ eˢ (label' h⋤A)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Label' l⊑h) refl (lowᴾ _ eˢ (label' h⋤A)) (lowᴾ _ eˢ₁ e₁) (Label' .l⊑h) = refl
        aux (Unlabel₁ l⊑h step) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Unlabel₁ l⊑h step) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabel₁ .l⊑h step') = refl
        aux (Unlabel₁ l⊑h step) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabel₂ .l⊑h) = refl
        aux (Unlabel₁ l⊑h step) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabelχ .l⊑h) = refl
        aux (Unlabel₂ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Unlabel₂ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabel₁ .l⊑h step') = refl
        aux (Unlabel₂ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabel₂ .l⊑h) = refl
        aux (Unlabel₂ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabelχ .l⊑h) = refl
        aux (Unlabelχ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Lift x) = refl
        aux (Unlabelχ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabel₁ .l⊑h step') = refl
        aux (Unlabelχ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabel₂ .l⊑h) = refl
        aux (Unlabelχ l⊑h) refl (lowᴾ _ eˢ (unlabel .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Unlabelχ .l⊑h) = refl
        aux (Bind₁ step) refl (lowᴾ _ eˢ (e >>= e₂)) (lowᴾ _ eˢ₁ (e₁ >>= e₃)) (Lift ())
        aux (Bind₁ step) refl (lowᴾ _ eˢ (e >>= e₂)) (lowᴾ _ eˢ₁ (e₁ >>= e₃)) (Bind₁ step')
          = aux step refl (lowᴾ L⊑A eˢ e) (lowᴾ L⊑A eˢ₁ e₁) step'
        aux Bind₂ refl (lowᴾ _ eˢ (e >>= e₁)) (lowᴾ _ eˢ₁ (e₂ ∘ e₃)) (Lift x) = refl
        aux Bind₂ refl (lowᴾ _ eˢ (e >>= e₁)) (lowᴾ _ eˢ₁ (e₂ ∘ e₃)) Bind₂ = refl
        aux Bind₃ refl (lowᴾ _ eˢ (e >>= e₁)) (lowᴾ _ eˢ₁ (throw e₂)) (Lift x) = refl
        aux Bind₃ refl (lowᴾ _ eˢ (e >>= e₁)) (lowᴾ _ eˢ₁ (throw e₂)) Bind₃ = refl
        aux (Join l⊑h x) refl (lowᴾ _ eˢ (join .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Lift x₁) = refl
        aux (Join l⊑h x) refl (lowᴾ _ eˢ (join .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Join .l⊑h x₁) = refl
        aux (Join l⊑h x) refl (lowᴾ _ eˢ (join .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Joinχ .l⊑h x₁) = refl
        aux (Join l⊑h x) refl (lowᴾ _ eˢ (join' .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ e₁) (Lift x₁) = refl
        aux (Join l⊑h x) refl (lowᴾ _ eˢ (join' .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ e₁) (Join∙ .l⊑h) = refl
        aux (Joinχ l⊑h x) refl (lowᴾ _ eˢ (join .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Lift x₁) = refl
        aux (Joinχ l⊑h x) refl (lowᴾ _ eˢ (join .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Join .l⊑h x₁) = refl
        aux (Joinχ l⊑h x) refl (lowᴾ _ eˢ (join .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Joinχ .l⊑h x₁) = refl
        aux (Joinχ l⊑h x) refl (lowᴾ _ eˢ (join' .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ e₁) (Lift x₁) = refl
        aux (Joinχ l⊑h x) refl (lowᴾ _ eˢ (join' .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ e₁) (Join∙ .l⊑h) = refl
        aux (Join∙ l⊑h) refl (lowᴾ _ eˢ (join∙ .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Lift x) = refl
        aux (Join∙ l⊑h) refl (lowᴾ _ eˢ (join∙ .l⊑h e)) (lowᴾ _ eˢ₁ e₁) (Join∙ .l⊑h) = refl
        aux (Catch₁ step) refl (lowᴾ _ eˢ (catch e e₁)) (lowᴾ _ eˢ₁ (catch e₂ e₃)) (Lift ())
        aux (Catch₁ step) refl (lowᴾ _ eˢ (catch e e₁)) (lowᴾ _ eˢ₁ (catch e₂ e₃)) (Catch₁ step')
          = aux step refl (lowᴾ L⊑A eˢ e) (lowᴾ L⊑A eˢ₁ e₂) step'
        aux Catch₂ refl (lowᴾ _ eˢ (catch e e₁)) (lowᴾ _ eˢ₁ (return e₂)) (Lift ())
        aux Catch₂ refl (lowᴾ _ eˢ (catch e e₁)) (lowᴾ _ eˢ₁ (return e₂)) Catch₂ = refl
        aux Catch₃ refl (lowᴾ _ eˢ (catch e e₁)) (lowᴾ _ eˢ₁ (e₂ ∘ e₃)) (Lift ())
        aux Catch₃ refl (lowᴾ _ eˢ (catch e e₁)) (lowᴾ _ eˢ₁ (e₂ ∘ e₃)) Catch₃ = refl
        aux (New l⊑h) refl (lowᴾ _ eˢ (new .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (New l⊑h) refl (lowᴾ _ eˢ (new .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ e₁) (New .l⊑h) = refl
        aux (New l⊑h) refl (lowᴾ _ eˢ (new' .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (New l⊑h) refl (lowᴾ _ eˢ (new' .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ e₁) (New∙ .l⊑h) = refl
        aux (Write₁ l⊑h step) refl (lowᴾ _ eˢ (write .l⊑h h⊑A e e₂)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Write₁ l⊑h step) refl (lowᴾ _ eˢ (write .l⊑h h⊑A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write₁ .l⊑h step') = refl
        aux (Write₁ l⊑h step) refl (lowᴾ _ eˢ (write .l⊑h h⊑A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write₂ .l⊑h w) = refl
        aux (Write₁ l⊑h step) refl (lowᴾ _ eˢ (write' .l⊑h h⋤A e e₂)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Write₁ l⊑h step) refl (lowᴾ _ eˢ (write' .l⊑h h⋤A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write∙₁ .l⊑h step') = refl
        aux (Write₁ l⊑h step) refl (lowᴾ _ eˢ (write' .l⊑h h⋤A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write∙₂ .l⊑h) = refl
        aux (Write₂ l⊑h w) refl (lowᴾ _ eˢ (write .l⊑h h⊑A e e₂)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Write₂ l⊑h w) refl (lowᴾ _ eˢ (write .l⊑h h⊑A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write₁ .l⊑h step') = refl
        aux (Write₂ l⊑h w) refl (lowᴾ _ eˢ (write .l⊑h h⊑A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write₂ .l⊑h w₁) = refl
        aux (Write₂ l⊑h w) refl (lowᴾ _ eˢ (write' .l⊑h h⋤A e e₂)) (lowᴾ _ eˢ₁ e₁) (Lift ())
        aux (Write₂ l⊑h w) refl (lowᴾ _ eˢ (write' .l⊑h h⋤A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write∙₁ .l⊑h step') = refl
        aux (Write₂ l⊑h w) refl (lowᴾ _ eˢ (write' .l⊑h h⋤A e e₂)) (lowᴾ _ eˢ₁ e₁) (Write∙₂ .l⊑h) = refl
        aux (Read₁ l⊑h step) refl (lowᴾ _ eˢ (read .l⊑h e)) (lowᴾ _ eˢ₁ (read .l⊑h e₁)) (Lift ())
        aux (Read₁ l⊑h step) refl (lowᴾ _ eˢ (read .l⊑h e)) (lowᴾ _ eˢ₁ (read .l⊑h e₁)) (Read₁ .l⊑h step') = refl
        aux (Read₂ l⊑h r) refl (lowᴾ _ eˢ (read .l⊑h e)) (lowᴾ _ eˢ₁ (return e₁)) (Lift x) = refl
        aux (Read₂ l⊑h r) refl (lowᴾ _ eˢ (read .l⊑h e)) (lowᴾ _ eˢ₁ (return e₁)) (Read₂ .l⊑h r₁) = refl
        aux (New∙ l⊑h) refl (lowᴾ _ eˢ (new∙ .l⊑h e)) (lowᴾ _ eˢ₁ (return e₁)) (Lift ())
        aux (New∙ l⊑h) refl (lowᴾ _ eˢ (new∙ .l⊑h e)) (lowᴾ _ eˢ₁ (return e₁)) (New∙ .l⊑h) = refl
        aux (Write∙₁ l⊑h step) refl (lowᴾ _ eˢ (write∙ .l⊑h e e₁)) (lowᴾ _ eˢ₁ (write∙ .l⊑h e₂ e₃)) (Lift x) = refl
        aux (Write∙₁ l⊑h step) refl (lowᴾ _ eˢ (write∙ .l⊑h e e₁)) (lowᴾ _ eˢ₁ (write∙ .l⊑h e₂ e₃)) (Write∙₁ .l⊑h step') = refl
        aux (Write∙₂ l⊑h) refl (lowᴾ _ eˢ (write∙ .l⊑h e e₁)) (lowᴾ _ eˢ₁ (return e₂)) (Lift x) = refl
        aux (Write∙₂ l⊑h) refl (lowᴾ _ eˢ (write∙ .l⊑h e e₁)) (lowᴾ _ eˢ₁ (return e₂)) (Write∙₂ .l⊑h) = refl
        aux (Fork l⊑h) refl (lowᴾ _ eˢ (fork .l⊑h h⊑A e)) (lowᴾ _ eˢ₁ (return e₁)) (Lift ())
        aux (Fork l⊑h) refl (lowᴾ _ eˢ (fork {h = H} .l⊑h H⊑A e)) (lowᴾ _ eˢ₁ (return e₁)) (Fork .l⊑h) with H ⊑? A
        ... | yes H⊑A' rewrite unlift-ε e = refl
        ... | no H⋤A = ⊥-elim (H⋤A H⊑A)
        aux (Fork l⊑h) refl (lowᴾ _ eˢ (fork' .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ (return e₁)) (Lift ())
        aux (Fork l⊑h) refl (lowᴾ _ eˢ (fork' {h = H} .l⊑h h⋤A e)) (lowᴾ _ eˢ₁ (return e₁)) (Fork∙ .l⊑h) with H ⊑? A
        ... | yes H⊑A = ⊥-elim (h⋤A H⊑A)
        ... | no H⋤A rewrite unlift-ε e = refl
        aux (Fork∙ l⊑h) refl (lowᴾ _ eˢ (fork∙ .l⊑h e)) (lowᴾ _ eˢ₁ (return e₁)) (Lift ())
        aux (Fork∙ l⊑h) refl (lowᴾ _ eˢ (fork∙ .l⊑h e)) (lowᴾ _ eˢ₁ (return e₁)) (Fork∙ .l⊑h)
          rewrite unlift-ε e = refl
        aux (Take₁ step) refl (lowᴾ _ eˢ (take e)) (lowᴾ _ eˢ₁ (take e₁)) (Lift ())
        aux (Take₁ step) refl (lowᴾ _ eˢ (take e)) (lowᴾ _ eˢ₁ (take e₁)) (Take₁ step') = refl
        aux (Take₂ r w) refl (lowᴾ _ eˢ (take e)) (lowᴾ _ eˢ₁ (return e₁)) (Lift x) = refl
        aux (Take₂ r w) refl (lowᴾ _ eˢ (take e)) (lowᴾ _ eˢ₁ (return e₁)) (Take₂ r₁ w₁) = refl
        aux (Put₁ step) refl (lowᴾ _ eˢ (put e e₁)) (lowᴾ _ eˢ₁ (put e₂ e₃)) (Lift x) = refl
        aux (Put₁ step) refl (lowᴾ _ eˢ (put e e₁)) (lowᴾ _ eˢ₁ (put e₂ e₃)) (Put₁ step') = refl
        aux (Put₂ r w) refl (lowᴾ _ eˢ (put e e₁)) (lowᴾ _ eˢ₁ (return e₂)) (Lift x) = refl
        aux (Put₂ r w) refl (lowᴾ _ eˢ (put e e₁)) (lowᴾ _ eˢ₁ (return e₂)) (Put₂ r₁ w₁) = refl
        aux (NewMVar l⊑h) refl (lowᴾ _ eˢ (newMVar .l⊑h h⊑A)) (lowᴾ _ eˢ₁ (return e₁)) (Lift x) = refl
        aux (NewMVar l⊑h) refl (lowᴾ _ eˢ (newMVar .l⊑h h⊑A)) (lowᴾ _ eˢ₁ (return e₁)) (NewMVar .l⊑h) = refl
        aux (NewMVar l⊑h) refl (lowᴾ _ eˢ (newMVar' .l⊑h h⋤A)) (lowᴾ _ eˢ₁ (return e₁)) (Lift x) = refl
        aux (NewMVar l⊑h) refl (lowᴾ _ eˢ (newMVar' .l⊑h h⋤A)) (lowᴾ _ eˢ₁ (return e₁)) (NewMVar∙ .l⊑h) = refl
        aux (NewMVar∙ l⊑h) refl (lowᴾ _ eˢ (newMVar∙ .l⊑h)) (lowᴾ _ eˢ₁ (return e₁)) (Lift x) = refl
        aux (NewMVar∙ l⊑h) refl (lowᴾ _ eˢ (newMVar∙ .l⊑h)) (lowᴾ _ eˢ₁ (return e₁)) (NewMVar∙ .l⊑h) = refl

open import Scheduler.Base 𝓛
open import Scheduler.Security 𝓛 A as Sch

-- Low fork (scheduler) events are left untouched by the erasure function
εᴱ-Forkᴸ-≡ : ∀ {L H} -> (L⊑H : L ⊑ H) (H⊑A : H ⊑ A) (n : ℕ) -> Sch.εᴱ (Fork H n L⊑H) ≡ (Fork H n L⊑H)
εᴱ-Forkᴸ-≡ {H = H} L⊑H H⊑A n with H ⊑? A
... | yes _ = refl
... | no H⋤A = ⊥-elim (H⋤A H⊑A)

-- High fork (scheduler) events are erased to single step event
εᴱ-Forkᴴ-≡ : ∀ {L H} -> (L⊑H : L ⊑ H) (H⋤A : H ⋤ A) (n : ℕ) -> Sch.εᴱ (Fork H n L⊑H) ≡ Step
εᴱ-Forkᴴ-≡ {H = H} L⊑H H⋤A n with H ⊑? A
... | yes H⊑A =  ⊥-elim (H⋤A H⊑A)
... | no _ = refl
