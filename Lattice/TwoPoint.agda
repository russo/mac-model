-- In this module we give a simple instantion of the 2-points lattice,
-- 𝓛 = {Low, High}, where High ⋤ Low is the only disallowed flow.
-- We define the label set, the can-flow-to relation and all the operations
-- specified by the Lattice interface in Lattice.Base.

module Lattice.TwoPoint where

open import Lattice.Base using (Lattice)
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality
open import Data.Empty

-- A simple data-type that encodes low and high labels.
data Label : Set where
 Low : Label
 High : Label

-- Decidable equality for those labels
_≟_ : (l₁ l₂ : Label) -> Dec (l₁ ≡ l₂)
Low ≟ Low = yes refl
Low ≟ High = no (λ ())
High ≟ Low = no (λ ())
High ≟ High = yes refl

-- The can-flow-to relation between low and high labels.
data _⊑_ : Label -> Label -> Set where
 L⊑ : ∀ (l : Label) -> Low ⊑ l
 H⊑H : High ⊑ High

-- Can-flow-to is a decidable relation.
_⊑?_ : (l₁ l₂ : Label) -> Dec (l₁ ⊑ l₂)
Low ⊑? l₂ = yes (L⊑ l₂)
High ⊑? Low = no (λ ())
High ⊑? High = yes H⊑H

-- The can-flow-to relation is reflexive
refl-⊑ : ∀ {l} -> l ⊑ l
refl-⊑ {Low} = L⊑ Low
refl-⊑ {High} = H⊑H

-- The can-flow-to relation is transitive.
trans-⊑ : ∀ {l₁ l₂ l₃} -> l₁ ⊑ l₂ -> l₂ ⊑ l₃ -> l₁ ⊑ l₃
trans-⊑ (L⊑ .Low) (L⊑ l₃) = L⊑ l₃
trans-⊑ (L⊑ .High) H⊑H = L⊑ High
trans-⊑ H⊑H H⊑H = H⊑H

-- 𝓛⁽ᴸᴴ⁾ defines the 2-point lattice as a Lattice record, that
-- contains the types and functions defined in this module
𝓛⁽ᴸᴴ⁾ : Lattice
𝓛⁽ᴸᴴ⁾ = record
          { Label = Label
          ; _⊑_ = _⊑_
          ; _⊑?_ = _⊑?_
          ; _≟_ = _≟_
          ; refl-⊑ = refl-⊑
          ; trans-⊑ = trans-⊑
          }
