-- This module simply collects the basic definitions of the sequential
-- calculus and rexports them.  Observe that the whole calculus is
-- parametric in the lattice 𝓛.

open import Lattice

module Sequential (𝓛 : Lattice)  where

open import Sequential.Calculus 𝓛 public
open import Sequential.Semantics 𝓛 public
