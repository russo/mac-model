-- This module proves that round robin is non-interfering.

import Lattice as L

module Scheduler.RoundRobin.Security {𝓛 : L.Lattice} (A : L.Label 𝓛) where

open L.Lattice 𝓛
open import Scheduler.RoundRobin.Base 𝓛
open import Scheduler.Base 𝓛
open import Scheduler.Security 𝓛 A
open Scheduler RR using (Next)

open import Data.List
open import Data.List.All
open import Lemmas
open import Data.Empty
open import Data.Nat
open import Data.Product
open import Data.Product
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality hiding ([_])

-- The erasure function filters out the identifiers of high threads.
ε : State -> State
ε [] = []
ε ((l , n) ∷ s) with l ⊑? A
ε ((l , n) ∷ s) | yes p = (l , n) ∷ (ε s)
ε ((l , n) ∷ s) | no ¬p = ε s

--------------------------------------------------------------------------------
-- Helper lemmas

ε-append-yes : (xs : State) (l : Label) (n : ℕ) -> l ⊑ A -> ε (xs ++ [ l , n ]) ≡ ε xs ++ [ l , n ]
ε-append-yes [] l n ¬p with l ⊑? A
ε-append-yes [] l n p' | yes p = refl
ε-append-yes [] l n p | no ¬p = ⊥-elim (¬p p)
ε-append-yes ((l' , n') ∷ xs) l n p with l' ⊑? A
... | yes p' rewrite ε-append-yes xs _ n p = refl
... | no ¬p rewrite ε-append-yes xs _ n p = refl


ε-append-no : ∀ {l} {{n}} -> (xs : State) -> ¬ (l ⊑ A) -> ε xs ≡ ε (xs ++ [ l , n ])
ε-append-no {l} [] ¬p with l ⊑? A
ε-append-no [] ¬p | yes p = ⊥-elim (¬p p)
ε-append-no [] ¬p₁ | no ¬p = refl
ε-append-no {{n}} ((l' , n') ∷ xs) ¬p with l' ⊑? A
... | yes p rewrite ε-append-no {{n}} xs ¬p  = refl
... | no ¬p' rewrite ε-append-no {{n}} xs ¬p  = refl

ε-++ : (s₁ s₂ : State) -> ε (s₁ ++ s₂) ≡ (ε s₁) ++ (ε s₂)
ε-++ [] s₂ = refl
ε-++ ((l , n) ∷ s₁) s₂ with l ⊑? A
ε-++ ((l , n) ∷ s₁) s₂ | yes p rewrite ε-++ s₁ s₂ = refl
ε-++ ((l , n) ∷ s₁) s₂ | no ¬p = ε-++ s₁ s₂

--------------------------------------------------------------------------------

-- Restricted simulation
ε-simᴸ : ∀ {L} {ω₁ ω₂ : State} {m : Message L} -> (L⊑A : L ⊑ A) -> ω₁ ⟶ ω₂ ↑ m -> (ε ω₁) ⟶ (ε ω₂) ↑ (εᴹ m)
ε-simᴸ L⊑A (step l n) with l ⊑? A
ε-simᴸ L⊑A (step {ω} l n) | yes p rewrite ε-append-yes ω l n L⊑A = step l n
ε-simᴸ L⊑A (step l n) | no ¬p = ⊥-elim (¬p L⊑A)
ε-simᴸ L⊑A (fork L n p) with L ⊑? A
ε-simᴸ L⊑A (fork {ω} {H} {m} L n p₁) | yes p
  rewrite ε-++ ω ((H , m) ∷ ((L , n) ∷ [])) with H ⊑? A
ε-simᴸ L⊑A (fork L n p₂) | yes p₁ | yes p with L ⊑? A
ε-simᴸ L⊑A (fork L n p₃) | yes p₂ | yes p₁ | yes p = fork L n p₃
ε-simᴸ L⊑A (fork L n p₂) | yes p₁ | yes p | no ¬p = ⊥-elim (¬p L⊑A)
ε-simᴸ L⊑A (fork L n p₁) | yes p | no ¬p with L ⊑? A
ε-simᴸ L⊑A (fork L n p₂) | yes p₁ | no ¬p | yes p = step L n
ε-simᴸ L⊑A (fork L n p₁) | yes p | no ¬p₁ | no ¬p = ⊥-elim (¬p L⊑A)
ε-simᴸ L⊑A (fork L n p) | no ¬p = ⊥-elim (¬p L⊑A)
ε-simᴸ L⊑A (done L n) with L ⊑? A
ε-simᴸ L⊑A (done L n) | yes p = done L n
ε-simᴸ L⊑A (done L n) | no ¬p = ⊥-elim (¬p L⊑A)
ε-simᴸ L⊑A (skip L n) with L ⊑? A
ε-simᴸ L⊑A (skip {ω} L n) | yes p rewrite ε-append-yes ω L n L⊑A = skip L n
ε-simᴸ L⊑A (skip L n) | no ¬p = ⊥-elim (¬p L⊑A)

-- Structural low-equivalence
data _≈_ : State -> State -> Set where
  nil : [] ≈ []
  consᴸ : ∀ {L n ω₁ ω₂} -> (L⊑A : L ⊑ A) ->  ω₁ ≈ ω₂ -> ((L , n) ∷ ω₁) ≈ ((L , n) ∷ ω₂)
  cons₁ᴴ : ∀ {H n ω₁ ω₂ } -> (H⋤A  : H ⋤ A) -> ω₁ ≈ ω₂ -> ((H , n) ∷ ω₁) ≈ ω₂
  cons₂ᴴ : ∀ {H n ω₁ ω₂} -> (H⋤A  : H ⋤ A) -> ω₁ ≈ ω₂ -> ω₁ ≈ ((H , n) ∷ ω₂)

-- The definition of low-equivalence based on the erasure function
-- implies structural low-equivalence.
⌜_⌝ : ∀ {ω₁ ω₂} -> ε ω₁ ≡ ε ω₂ -> ω₁ ≈ ω₂
⌜_⌝ = aux _ _
  where
    ∷-≡ : ∀ {x y : Label × ℕ} {s₁ s₂ : State} -> _≡_ {A = State} (x ∷ s₁) (y ∷ s₂) -> x ≡ y × s₁ ≡ s₂
    ∷-≡ refl = refl , refl

    aux : ∀ (ω₁ ω₂ : _) -> ε ω₁ ≡ ε ω₂ -> ω₁ ≈ ω₂


    aux₁ : ∀ {l n} (ω₁ ω₂ : State) -> (l , n) ∷ ε ω₁ ≡ ε ω₂ -> ((l , n) ∷ ω₁) ≈ ω₂
    aux₁ ω₃ [] ()
    aux₁ ω₃ ((l' , n) ∷ ω₄) eq with l' ⊑? A
    aux₁ ω₃ ((l' , n₁) ∷ ω₄) eq | yes p with ∷-≡ eq
    aux₁ ω₃ ((l , n₁) ∷ ω₄) eq | yes p | refl , eq' = consᴸ p (aux ω₃ ω₄ eq')
    aux₁ ω₃ ((l' , n₁) ∷ ω₄) eq | no H⋤A = cons₂ᴴ H⋤A (aux₁ ω₃ ω₄ eq)


    aux [] [] eq = nil
    aux [] ((l , n) ∷ ω₂) eq with l ⊑? A
    aux [] ((l , n) ∷ ω₂) () | yes p
    aux [] ((l , n) ∷ ω₂) eq | no H⋤A = cons₂ᴴ H⋤A (aux [] ω₂ eq)
    aux ((l , n) ∷ ω₁) ω₂ eq with l ⊑? A
    aux ((l , n) ∷ ω₃) [] () | yes p
    aux ((l , n) ∷ ω₃) ((l' , m) ∷ ω₄) eq | yes p with l' ⊑? A
    aux ((l , n) ∷ ω₃) ((l' , m) ∷ ω₄) eq | yes p₁ | yes p with ∷-≡ eq
    aux ((l' , m) ∷ ω₃) ((.l' , .m) ∷ ω₄) eq | yes p₁ | yes p | refl , eq₂ = consᴸ p₁ (aux ω₃ ω₄ eq₂)
    aux ((l , n) ∷ ω₃) ((l' , m) ∷ ω₄) eq | yes p | no H⋤A = cons₂ᴴ H⋤A (aux₁ ω₃ ω₄ eq)
    aux ((l , n) ∷ ω₃) ω₄ eq | no H⋤A = cons₁ᴴ H⋤A (aux ω₃ ω₄ eq)

-- Structural low-equivalence implies the definition of
-- low-equivalence based on the erasure function.
⌞_⌟ : ∀ {ω₁ ω₂} -> ω₁ ≈ ω₂ -> ε ω₁ ≡ ε ω₂
⌞ nil ⌟ = refl
⌞ (consᴸ {l} p x) ⌟ with l ⊑? A
⌞ (consᴸ p₁ x) ⌟ | yes p rewrite ⌞_⌟ x = refl
⌞ (consᴸ p x) ⌟ | no H⋤A = ⊥-elim (H⋤A p)
⌞ (cons₁ᴴ {h} H⋤A x) ⌟ with h ⊑? A
⌞ (cons₁ᴴ H⋤A x) ⌟ | yes p = ⊥-elim (H⋤A p)
⌞ (cons₁ᴴ H⋤A₁ x) ⌟ | no H⋤A =  ⌞_⌟ x
⌞ (cons₂ᴴ {h} H⋤A x) ⌟ with h ⊑? A
⌞ (cons₂ᴴ H⋤A x) ⌟ | yes p = ⊥-elim (H⋤A p)
⌞ (cons₂ᴴ H⋤A₁ x) ⌟ | no H⋤A = ⌞ x ⌟

refl-≈ : ∀ {ω} -> ω ≈ ω
refl-≈ = ⌜ refl ⌝

sym-≈ : ∀ {ω₁ ω₂} -> ω₁ ≈ ω₂ -> ω₂ ≈ ω₁
sym-≈ eq = ⌜ (sym ⌞ eq ⌟) ⌝

trans-≈ : ∀ {ω₁ ω₂ ω₃} -> ω₁ ≈ ω₂ -> ω₂ ≈ ω₃ -> ω₁ ≈ ω₃
trans-≈ eq₁ eq₂ = ⌜ (trans ⌞ eq₁ ⌟ ⌞ eq₂ ⌟) ⌝

append-≈ : ∀ {ω₁ ω₂ ω₃} -> All (λ x → proj₁ x ⋤ A) ω₃ -> ω₁ ≈ ω₂ -> ω₁ ≈ (ω₂ ++ ω₃)
append-≈ [] nil = nil
append-≈ (px ∷ xs) nil = cons₂ᴴ px (append-≈ xs nil)
append-≈ xs (consᴸ L⊑A eq) = consᴸ L⊑A (append-≈ xs eq)
append-≈ xs (cons₁ᴴ H⋤A eq) = cons₁ᴴ H⋤A (append-≈ xs eq)
append-≈ xs (cons₂ᴴ H⋤A eq) = cons₂ᴴ H⋤A (append-≈ xs eq)

-- No observable effect
ε-simᴴ : ∀ {ω₁ ω₂ H} {m : Message H} -> H ⋤ A -> ω₁ ⟶ ω₂ ↑ m -> ω₁ ≈ ω₂
ε-simᴴ H⋤A (step l n) = cons₁ᴴ H⋤A (append-≈ (H⋤A ∷ []) refl-≈)
ε-simᴴ H⋤A (fork H n L⊑A) = cons₁ᴴ H⋤A (append-≈ ((trans-⋤ L⊑A H⋤A) ∷ (H⋤A ∷ [])) refl-≈)
ε-simᴴ H⋤A (done l n) = cons₁ᴴ H⋤A refl-≈
ε-simᴴ H⋤A (skip l n) = cons₁ᴴ H⋤A (append-≈ (H⋤A ∷ []) refl-≈)

-- Count the number of high thread in the left state.
offset₁ : ∀ {s₁ s₂} -> s₁ ≈ s₂ -> ℕ
offset₁ nil = 0
offset₁ (consᴸ L⊑A x) = 0
offset₁ (cons₁ᴴ H⋤A x) = suc (offset₁ x)
offset₁ (cons₂ᴴ H⋤A x) = offset₁ x

-- Count the number of high thread in the right state.
offset₂ : ∀ {s₁ s₂} -> s₁ ≈ s₂ -> ℕ
offset₂ nil = 0
offset₂ (consᴸ L⊑A x) = 0
offset₂ (cons₁ᴴ H⋤A x) = offset₂ x
offset₂ (cons₂ᴴ H⋤A x) = suc (offset₂ x)

-- Annotated low-equivalence
data _≈⟨_,_⟩_ : State -> ℕ -> ℕ -> State -> Set where
  nil : [] ≈⟨ 0 , 0 ⟩ []
  consᴸ : ∀ {L n ω₁ ω₂} -> (L⊑A : L ⊑ A) ->  ω₁ ≈ ω₂ -> ((L , n) ∷ ω₁) ≈⟨ 0 , 0 ⟩ ((L , n) ∷ ω₂)
  cons₁ᴴ : ∀ {H n ω₁ ω₂ i j} -> (H⋤A  : H ⋤ A) -> ω₁ ≈⟨ i , j ⟩ ω₂ -> ((H , n) ∷ ω₁) ≈⟨ suc i , j ⟩ ω₂
  cons₂ᴴ : ∀ {H n ω₁ ω₂ i j} -> (H⋤A  : H ⋤ A) -> ω₁ ≈⟨ i , j ⟩ ω₂ -> ω₁ ≈⟨ i , suc j ⟩ ((H , n) ∷ ω₂)

-- Decorates structural low-equivalence with annotations.
align : ∀ {s₁ s₂} -> (eq : s₁ ≈ s₂) -> s₁ ≈⟨ offset₁ eq , offset₂ eq ⟩ s₂
align nil = nil
align (consᴸ L⊑A x) = consᴸ L⊑A x
align (cons₁ᴴ H⋤A x) = cons₁ᴴ H⋤A (align x)
align (cons₂ᴴ H⋤A x) = cons₂ᴴ H⋤A (align x)

-- Removes the annotations from the annotated low-equivalence.
forget : ∀ {s₁ s₂ i j} -> s₁ ≈⟨ i , j ⟩ s₂ -> s₁ ≈ s₂
forget nil = nil
forget (consᴸ L⊑A x) = consᴸ L⊑A x
forget (cons₁ᴴ H⋤A x) = cons₁ᴴ H⋤A (forget x)
forget (cons₂ᴴ H⋤A x) = cons₂ᴴ H⋤A (forget x)

-- Helper lemma
append-≈′ : ∀ {ω₁ ω₁' ω₂ ω₃ n₁ n₂ L} {m : Message L} -> (L⊑A : L ⊑ A) -> ω₁ ⟶ ω₁' ↑ m ->
             ω₁ ≈⟨ n₁ , n₂ ⟩ ω₂ -> All (λ x → proj₁ x ⋤ A) ω₃ -> ω₁ ≈⟨ n₁ , n₂ ⟩ (ω₂ ++ ω₃)
append-≈′ L⊑A () nil ps
append-≈′ L⊑A s (consᴸ L⊑A₁ x) ps = consᴸ L⊑A₁ (append-≈ ps x)
append-≈′ L⊑A (step l n) (cons₁ᴴ H⋤A eq) ps = ⊥-elim (H⋤A L⊑A)
append-≈′ L⊑A (fork L n p) (cons₁ᴴ H⋤A eq) ps = ⊥-elim (H⋤A L⊑A)
append-≈′ L⊑A (done l n) (cons₁ᴴ H⋤A eq) ps = ⊥-elim (H⋤A L⊑A)
append-≈′ L⊑A (skip l n) (cons₁ᴴ H⋤A eq) ps = ⊥-elim (H⋤A L⊑A)
append-≈′ L⊑A s (cons₂ᴴ H⋤A eq) ps = cons₂ᴴ H⋤A (append-≈′ L⊑A s eq ps)


-- No starvation
triangle : ∀ {L H n m i j e e' ω₁ ω₂ ω₁' ω₂'}  -> L ⊑ A -> ω₁ ≈⟨ i , suc j ⟩ ω₂ ->
                   ω₁ ⟶ ω₁' ↑ ⟪ L , n , e ⟫ -> ω₂ ⟶ ω₂' ↑ ⟪ H , m , e' ⟫ -> (H ⋤ A) × (ω₁ ≈⟨ i , j ⟩ ω₂')
triangle L⊑A (cons₁ᴴ H⋤A eq) (step l n) s₂ = ⊥-elim (H⋤A L⊑A)
triangle L⊑A (cons₁ᴴ H⋤A eq) (fork L n p) s₂ = ⊥-elim (H⋤A L⊑A)
triangle L⊑A (cons₁ᴴ H⋤A eq) (done l n) s₂ = ⊥-elim (H⋤A L⊑A)
triangle L⊑A (cons₁ᴴ H⋤A eq) (skip l n) s₂ = ⊥-elim (H⋤A L⊑A)
triangle L⊑A (cons₂ᴴ H⋤A eq) s₁ (step H m) = H⋤A , append-≈′ L⊑A s₁ eq (H⋤A ∷ [])
triangle L⊑A (cons₂ᴴ H⋤A eq) s₁ (fork H m p) = H⋤A , append-≈′ L⊑A s₁ eq (trans-⋤ p H⋤A ∷ H⋤A ∷ [])
triangle L⊑A (cons₂ᴴ H⋤A eq) s₁ (done H m) = H⋤A , eq
triangle L⊑A (cons₂ᴴ H⋤A eq) s₁ (skip H m) = H⋤A , append-≈′ L⊑A s₁ eq (H⋤A ∷ [])

-- If there is at least one alive thread, the round robin scheduler
-- will schedule it.
stepWith : ∀ {l n ω} (e : Event l) → ∃ (λ ω' → ((l , n) ∷ ω) ⟶ ω' ↑ ⟪ l , n , e ⟫)
stepWith {l} {n} {ω} Stuck = ω ++ (l , n) ∷ [] , skip l n
stepWith {l} {n} {ω} Step = ω ++ (l , n) ∷ [] , step l n
stepWith {l} {n} {ω} Done = ω , done l n
stepWith {l} {n} {ω} (Fork h n₁ x) = ω ++ (h , n₁) ∷ (l , n) ∷ [] , fork l n x

-- 1-step progress (low-step)
progressᴸ : ∀ {L i n e₁ ω₁ ω₂ ω₁'} -> (L⊑A : L ⊑ A) -> ω₁ ⟶ ω₁' ↑ ⟪ L , n , e₁ ⟫ -> ω₁ ≈⟨ i , 0 ⟩ ω₂ →
               (((L , n) ∈ ω₂) × (Next ω₂ (L , n)))
progressᴸ L⊑A () nil
progressᴸ L⊑A (step l n) (consᴸ {ω₂ = ω₂} L⊑A₁ x) = here , stepWith
progressᴸ L⊑A (fork l n p) (consᴸ L⊑A₁ x) = here , stepWith
progressᴸ L⊑A (done l n) (consᴸ L⊑A₁ x) = here , stepWith
progressᴸ L⊑A (skip l n) (consᴸ L⊑A₁ x) = here , stepWith
progressᴸ L⊑A (step l n) (cons₁ᴴ L⋤A ω₁≈ω₂) = ⊥-elim (L⋤A L⊑A)
progressᴸ L⊑A (fork l n p) (cons₁ᴴ L⋤A ω₁≈ω₂) = ⊥-elim (L⋤A L⊑A)
progressᴸ L⊑A (done l n) (cons₁ᴴ L⋤A ω₁≈ω₂) = ⊥-elim (L⋤A L⊑A)
progressᴸ L⊑A (skip l n) (cons₁ᴴ L⋤A ω₁≈ω₂) = ⊥-elim (L⋤A L⊑A)

-- 1-step progress (high-step)
progressᴴ : ∀ {ω₁ ω₁' ω₂ L e n n₁ n₂} -> L ⊑ A -> ω₁ ≈⟨ n₁ , suc n₂ ⟩ ω₂ -> ω₁ ⟶ ω₁' ↑ ⟪ L , n , e ⟫ ->
                  ∃ (λ x → x ∈ ω₂ × Next ω₂ x)
progressᴴ L⊑A (cons₁ᴴ H⋤A eq) (step l n) = ⊥-elim (H⋤A L⊑A)
progressᴴ L⊑A (cons₁ᴴ H⋤A eq) (fork L n p) = ⊥-elim (H⋤A L⊑A)
progressᴴ L⊑A (cons₁ᴴ H⋤A eq) (done l n) = ⊥-elim (H⋤A L⊑A)
progressᴴ L⊑A (cons₁ᴴ H⋤A eq) (skip l n) = ⊥-elim (H⋤A L⊑A)
progressᴴ L⊑A (cons₂ᴴ {H} {n} {ω₁} {ω₂} H⋤A eq) s₁ = (H , n) , here , stepWith

-- Round robin fulfills all the requirements of a non-interferent
-- scheduler.
RR-is-NI : NIˢ RR
RR-is-NI = record
             { εˢ = ε
             ; _≈ˢ_ = _≈_
             ; ⌞_⌟ˢ = ⌞_⌟
             ; ⌜_⌝ˢ = ⌜_⌝
             ; εˢ-simᴸ = ε-simᴸ
             ; εˢ-simᴴ = ε-simᴴ
             ; _≈ˢ⟨_,_⟩_ = _≈⟨_,_⟩_
             ; offset₁ = offset₁
             ; offset₂ = offset₂
             ; align = align
             ; forget = forget
             ; triangleˢ = triangle
             ; progressᴸ = progressᴸ
             ; progressᴴ = progressᴴ
             }
