-- This module defines the round-robin scheduler.

import Lattice as L

module Scheduler.RoundRobin.Base (𝓛 : L.Lattice) where

open import Lemmas
open L.Lattice 𝓛

open import Scheduler.Base 𝓛 as S
open Scheduler using (Next)

open import Data.Product
open import Data.List
open import Data.Nat

-- The state of the scheduler is a list that contains the identifier
-- (Label and thread number) of the alive threads.
State : Set
State = List (Label × ℕ)

-- The scheduler semantics schedules the first alive thread and
-- updates its state according to the event. Dead threads are removed
-- from the list (done) and new forked threads are added to the state
-- (fork). The list is used as a queue, which ensures that each alive
-- thread is run with the same quantum.
data _⟶_↑_ : ∀ {l} -> State -> State -> Message l -> Set where
  step : ∀ {ω} -> (l : Label) (n : ℕ) -> ((l , n) ∷ ω) ⟶ ω ++ [ (l , n) ] ↑ ⟪ l , n , Step ⟫
  fork : ∀ {ω h m} -> (l : Label) (n : ℕ) (p : l ⊑ h) -> ((l , n) ∷ ω) ⟶ ω ++ ((h , m) ∷ (l , n) ∷ []) ↑ ⟪ l , n , Fork h m p ⟫
  done : ∀ {ω} -> (l : Label) (n : ℕ) -> ((l , n) ∷ ω) ⟶ ω ↑ ⟪ l , n , Done ⟫
  skip : ∀ {ω} -> (l : Label) (n : ℕ) -> ((l , n) ∷ ω) ⟶ ω ++ [ (l , n) ] ↑ ⟪ l , n , Stuck ⟫

open import Relation.Binary.PropositionalEquality hiding ([_])

-- Determinancy of the scheduler semantics relation.
determinism : ∀ {s₁ s₂ s₃ l n e} ->
                                   s₁ ⟶ s₂ ↑ ⟪ l , n , e ⟫ ->
                                   s₁ ⟶ s₃ ↑ ⟪ l , n , e ⟫ ->
                                   s₂ ≡ s₃
determinism (step l n) (step .l .n) = refl
determinism (fork l n p) (fork .l .n .p) = refl
determinism (done l n) (done .l .n) = refl
determinism (skip l n) (skip .l .n) = refl


-- Round robin fulfills the interface of a scheduler.
RR : Scheduler
RR = record
       { State = State
       ; _⟶_↑_ = _⟶_↑_
       ; determinismˢ = determinism
       ; _∈ˢ_ = _∈_   -- Standard list membership
       }
       where
