-- This module proves that the round robin scheduler is valid with
-- respect to the concurrent semantics, i.e., any thread scheduled by
-- the round robin, has been in fact allocated in the thread pool.
-- Note that this is the only proof that is specific to both the
-- concurrent semantics and to the scheduler.

import Lattice as L

module Scheduler.RoundRobin.Valid (𝓛 : L.Lattice) where

open import Lemmas
open L.Lattice 𝓛 public
open import Scheduler.RoundRobin.Base 𝓛 as S
open import Scheduler.Base 𝓛
open Scheduler RR

open import Sequential.Calculus 𝓛 as S₁ public
import Concurrent.Calculus as C₁
open C₁ 𝓛 RR
open import Concurrent.Semantics 𝓛 RR as C
open import Sequential.Valid 𝓛 using (valid ; valid⟼)
open import Concurrent.Valid 𝓛 RR

open import Function
open import Data.Empty
open import Data.List hiding (drop)
open import Data.Product as P
open import Data.Nat hiding (_≟_)
open import Relation.Binary.PropositionalEquality hiding ([_])
open import Relation.Nullary

-- We can swap two lists of alive threads and the scheduler remains valid.
validˢ-swap : ∀ Φ xs ys → validˢ (xs ++ ys) Φ → validˢ (ys ++ xs) Φ
validˢ-swap Φ xs ys v (l' , n') p = v (l' , n') (swap-∈ ys xs p)

-- After enqueuing a thread id, the new state is still valid with
-- respect to the old pool map.
validˢ-snoc : ∀ {l n ω Φ} → validˢ ((l , n) ∷ ω) Φ → validˢ (ω ++ [ (l , n )]) Φ
validˢ-snoc {l} {n} = validˢ-swap _ [ (l , n) ] _

-- If a state is valid with respect to a thread pool, then it is also
-- valid after replacing a pool in the map with an updated pool.
update-validˢ : ∀ {l n t ω Φ} {P' : Pool l} → validˢ ω Φ → P' ≔ (Φ l) [ n ↦ t ]ᴾ → validˢ ω (Φ [ l ↦ P' ]ᴹ)
update-validˢ {l} v w (l' , n') p with l ≟ l'
update-validˢ {l} v w (l' , n') p | no ¬p = v (l' , n') p
update-validˢ {.l} v w (l , n') p | yes refl = aux w (proj₂ (v (l , n') p))
  where aux : ∀ {n n' t t'} {P P' : Pool l} → P' ≔ P [ n ↦ t ]ᴾ → n' ↦ t' ∈ᴾ P → ∃ (λ t' → n' ↦ t' ∈ᴾ P')
        aux C₁.here C₁.here = _ , here
        aux C₁.here (C₁.there y) = _ , there y
        aux (C₁.there x) C₁.here = _ , here
        aux (C₁.there x) (C₁.there y) = P.map id there (aux x y)

-- After removing a dead thread, the scheduler is still valid with respect to the others.
valid-tail : ∀ {l n ω Φ} → validˢ ((l , n) ∷ ω) Φ →  validˢ ω Φ
valid-tail vˢ (l' , n') x = vˢ (l' , n') (there x)

-- Updating a pool map and reading gives the new pool right back
lookupᴾ : (Φ : Mapᴾ) (l : Label) (P : Pool l) → (Φ [ l ↦ P ]ᴹ) l ≡ P
lookupᴾ Φ l P with l ≟ l
... | yes refl = refl
... | no ¬p = ⊥-elim (¬p refl)

-- If thread t is present in pool P at address n, then it is available
-- at the same address in any extended pool.
wken-∈ᴾ-▻ : ∀ {l n} {P} {t t' : Thread l} → n ↦ t ∈ᴾ P → n ↦ t ∈ᴾ (P ▻ t')
wken-∈ᴾ-▻ C₁.here = here
wken-∈ᴾ-▻ (C₁.there r) = there (wken-∈ᴾ-▻ r)

-- If thread t is present in pool map Φ at address n and label L, then
-- it is available at the same address (L , n) in any extended pool
-- map.
fork-∈ᴾ : ∀ {L H n} {t : Thread L} → (Φ : Mapᴾ) (t' : Thread H) → n ↦ t ∈ᴾ (Φ L) → n ↦ t ∈ᴾ (Φ [ H ↦ (Φ H) ▻ t' ]ᴹ) L
fork-∈ᴾ {H} {L} Φ t' r with H ≟ L
fork-∈ᴾ {L} {.L} Φ t' r | yes refl with L ≟ L
fork-∈ᴾ {L} {.L} Φ t' r | yes refl | yes refl = wken-∈ᴾ-▻ r
fork-∈ᴾ {L} {.L} Φ t' r | yes refl | no ¬p = ⊥-elim (¬p refl)
fork-∈ᴾ {L} {H} Φ t' r | no ¬p with H ≟ L
fork-∈ᴾ {L} {.L} Φ t' r | no ¬p | yes refl = ⊥-elim (¬p refl)
fork-∈ᴾ {L} {H} Φ t' r | no ¬p | no ¬p₁ = r

-- If thread t is present in pool Φ l at address n, then the scheduler
-- is valid also with it in the list of alive threads.
wken-validˢ : ∀ {l H n t ω} → (Φ : Mapᴾ) (t' : Thread H) →
              let Φ' = Φ [ H ↦ (Φ H) ▻ t' ]ᴹ in
                n ↦ t ∈ᴾ (Φ l) → validˢ ω Φ' → validˢ ((l , n) ∷ ω) Φ'
wken-validˢ {l} {H} Φ t r v .(_ , _) here with H ≟ l
wken-validˢ {l} {.l} Φ t r v .(l , _) here | yes refl = _ , wken-∈ᴾ-▻ r
wken-validˢ {l} {H} Φ t r v .(l , _) here | no ¬p = _ , r
wken-validˢ Φ t _ v x (there p') = v x p'

-- Extending the list of alive threads and the pool-map preserves validity.
validˢ-▻ : ∀ {ω Φ H Σ l n} → (tᴴ : Thread H) (vᴹ : validᴹ Σ Φ) →
               let Pᴴ = Φ H
                   nᴴ = lengthᴾ Pᴴ in validˢ ((l , n) ∷ ω) Φ → validˢ ((H , nᴴ) ∷ (l , n) ∷ ω) (Φ [ H ↦ Pᴴ ▻ tᴴ ]ᴹ )
validˢ-▻ {Φ = Φ} tᴴ vᴹ vˢ (l , n) (there p) with vˢ (l , n) p
... | t' , r = t' , fork-∈ᴾ Φ tᴴ r
validˢ-▻ {_} {Φ} {H} tᴴ vᴹ vˢ (l , n)  here rewrite lookupᴾ Φ H (Φ H ▻ tᴴ) = tᴴ , aux (Φ H) (vᴹ H)
  where aux : ∀ {Σ} (P : Pool H) (v : validᴾ Σ P)  → lengthᴾ P ↦ tᴴ ∈ᴾ (P ▻ tᴴ)
        aux C₁.[] _ = here
        aux (t C₁.◅ P) (_ , v) = there (aux P v)
        aux C₁.∙ ()

-- Fork preserves validity.
valid-fork : ∀ {L H ω Σ Φ nᴸ} {t' : Thread L} {P' : Pool L} → (tᴴ : Thread H) →
            let  P = Φ L
                 Φ' = Φ [ L ↦ P' ]ᴹ
                 Pᴴ = Φ' H
                 nᴴ = lengthᴾ Pᴴ in
           P' ≔ P [ nᴸ ↦ t' ]ᴾ → validᴹ Σ Φ → valid Σ t' → validˢ ((L , nᴸ) ∷ ω ) Φ
           → validˢ (ω ++ ((H , nᴴ) ∷ (L , nᴸ) ∷ [])) (Φ' [ H ↦ Pᴴ ▻ tᴴ ]ᴹ)
valid-fork {L} {H} {ω} {Σ} {Φ} {nᴸ} {t'} {P'} tᴴ u vᴹ v vˢ with vᴹ L
... | vᴾ with writeᴾ-valid u vᴾ v
... | vᴾ' =  validˢ-swap _ ((H , lengthᴾ ((Φ [ L ↦ P' ]ᴹ) H)) ∷ (L , nᴸ) ∷ []) ω
                        (validˢ-▻ {Φ = Φ [ L ↦ P' ]ᴹ} {Σ = Σ} tᴴ (updateᴹ-valid vᴹ vᴾ') (update-validˢ vˢ u))


-- The global semantics preserves validity of the scheduler.
validˢ↪ : ∀ {x c₁ c₂} → x ⊢ c₁ ↪ c₂ → validᴳ c₁ → validˢ (state c₂) (mapᴾ c₂)
validˢ↪ (C.Step (S.step l₁ n₁) r step₁ s≡∅ u) (vˢ , _ , _) = update-validˢ (validˢ-snoc vˢ) u
validˢ↪ {.(l , n)} (C.CFork {H = h} {Φ = Φ} {tᴴ = tᴴ} L⊑H (S.fork {ω = ω} l n .L⊑H) r step₁ s≡f u) (vˢ , Σⱽ , vᴹ)
  with readᴾ-valid r (vᴹ l)
... | v with valid⟼ (Σⱽ , v) step₁
... | Σ'ⱽ , v' = valid-fork tᴴ u vᴹ v' vˢ
validˢ↪ (C.CFork∙ L⊑H (S.step l n) r step₁ s≡f u) (vˢ , _ , _) = update-validˢ (validˢ-snoc vˢ) u
validˢ↪ (C.Done (S.done l n) r done₁) (vˢ , _ , _) = valid-tail vˢ
validˢ↪ (C.Stuck (S.skip l n) r stuck) (vˢ , _ , _) = validˢ-snoc vˢ

-- The proof that Round Robin preserves is a valid scheduler, i.e., it
-- preserves the property of scheduling only alive threads.
valid-RR : Validˢ
valid-RR = record { validˢ↪ = validˢ↪ }
