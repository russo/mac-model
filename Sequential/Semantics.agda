-- This module defines the operational semantics of the sequential
-- calculus.

import Lattice as L

module Sequential.Semantics (𝓛 : L.Lattice) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Data.Nat
open import Relation.Nullary

--------------------------------------------------------------------------------

-- Semantics of the pure calculus, t₁ ⇝ t₂ denotes that closed term t₁
-- steps to closed term t₂ in one step.  Observe that the type of the
-- semantics relation, i.e., _⇝_ : ∀ {τ} -> CTerm τ -> CTerm τ -> Set,
-- ensures by construction type-preservation.
data _⇝_ : ∀ {τ} -> CTerm τ -> CTerm τ -> Set where

  -- Reduces the function in an application
  App : ∀ {τ₁ τ₂} {t₁ t₁' : CTerm (τ₁ ➔ τ₂)} {t₂ : CTerm τ₁} -> t₁ ⇝ t₁' -> t₁ ∘ t₂ ⇝ t₁' ∘ t₂

  -- Pushes a term in the environment
  Beta : ∀ {τ₁ τ₂} {t₁ : Term (τ₁ ∷ []) τ₂} {t₂ : CTerm τ₁} -> (Λ t₁) ∘ t₂ ⇝ subst t₂ t₁

  If₁ : ∀ {τ} {t₁ t₁' : CTerm Bool} {t₂ t₃ : CTerm τ} -> t₁ ⇝ t₁' ->
             (if t₁ then t₂ else t₃) ⇝ (if t₁' then t₂ else t₃)

  If₂ : ∀ {τ} {t₁ t₂ : CTerm τ} -> (if True then t₁ else t₂) ⇝ t₁

  If₃ : ∀ {τ} {t₁ t₂ : CTerm τ} -> (if False then t₁ else t₂) ⇝ t₂

  --------------------------------------------------------------------------------
  -- Flexible Labeled Values

  Fmap : ∀ {τ₁ τ₂ l} {t₁ : CTerm (τ₁ ➔ τ₂)} {t₂ : CTerm (Labeled l τ₁)} -> t₁ ⟨$⟩ t₂ ⇝ (Labeled l t₁) ⟨✴⟩ t₂

  ⟨✴⟩₁ : ∀ {τ₁ τ₂ l} {t₁ t₁' : CTerm (Labeled l (τ₁ ➔ τ₂))} {t₂ : CTerm (Labeled l τ₁)} ->
          t₁ ⇝ t₁' -> t₁ ⟨✴⟩ t₂ ⇝ t₁' ⟨✴⟩ t₂

  ⟨✴⟩₂ : ∀ {τ₁ τ₂ l} {t₁ : CTerm (τ₁ ➔ τ₂)} {t₂ t₂' : CTerm (Labeled l τ₁)} ->
          t₂ ⇝ t₂' -> (Labeled l t₁) ⟨✴⟩ t₂ ⇝ (Labeled l t₁) ⟨✴⟩ t₂'

  ⟨✴⟩₃ : ∀ {τ₁ τ₂ l} {t₁ : CTerm (τ₁ ➔ τ₂)} {t₂ : CTerm τ₁} ->
          (Labeled l t₁) ⟨✴⟩ (Labeled l t₂) ⇝ (Labeled l (t₁ ∘ t₂))

  Relabel₁ : ∀ {τ l h} {t t' : CTerm (Labeled l τ)} -> (l⊑h : l ⊑ h) ->
             t ⇝ t' -> relabel l⊑h t ⇝ relabel l⊑h t'

  Relabel₂ : ∀ {τ l h} {t : CTerm τ} -> (l⊑h : l ⊑ h) ->
             relabel l⊑h (Labeled l t) ⇝ Labeled h t

  --------------------------------------------------------------------------------
  -- Flexible Labeled values with ∙

  Fmap∙ : ∀ {τ₁ τ₂ l} {t₁ : CTerm (τ₁ ➔ τ₂)} {t₂ : CTerm (Labeled l τ₁)} -> t₁ ⟨$⟩∙ t₂ ⇝ (Labeled l ∙) ⟨✴⟩∙ t₂

  ⟨✴⟩∙₁ : ∀ {τ₁ τ₂ l} {t₁ t₁' : CTerm (Labeled l (τ₁ ➔ τ₂))} {t₂ : CTerm (Labeled l τ₁)} ->
          t₁ ⇝ t₁' -> t₁ ⟨✴⟩∙ t₂ ⇝ t₁' ⟨✴⟩∙ t₂

  ⟨✴⟩∙₂ : ∀ {τ₁ τ₂ l} {t₁ : CTerm (τ₁ ➔ τ₂)} {t₂ t₂' : CTerm (Labeled l τ₁)} ->
          t₂ ⇝ t₂' -> (Labeled l t₁) ⟨✴⟩∙ t₂ ⇝ (Labeled l t₁) ⟨✴⟩∙ t₂'

  ⟨✴⟩∙₃ : ∀ {τ₁ τ₂ l} {t₁ : CTerm (τ₁ ➔ τ₂)} {t₂ : CTerm τ₁} ->
          (Labeled l t₁) ⟨✴⟩∙ (Labeled l t₂) ⇝ (Labeled l ∙)

  Relabel∙₁ : ∀ {τ l h} {t t' : CTerm (Labeled l τ)} -> (l⊑h : l ⊑ h) ->
             t ⇝ t' -> relabel∙ l⊑h t ⇝ relabel∙ l⊑h t'

  Relabel∙₂ : ∀ {τ l h} {t : CTerm τ} -> (l⊑h : l ⊑ h) ->
             relabel∙ l⊑h (Labeled l t) ⇝ Labeled h ∙

  --------------------------------------------------------------------------------
  -- Exceptions in Labeled values (sequential calculus only)

  ⟨✴⟩₁χ : ∀ {τ₁ τ₂ l} {t₁ : CTerm χ} {t₂ t₂' : CTerm (Labeled l τ₁)} ->
           t₂ ⇝ t₂' -> _⟨✴⟩_ {τ₂ = τ₂} (Labeledχ l t₁) t₂ ⇝ (Labeledχ l t₁) ⟨✴⟩ t₂'

  ⟨✴⟩₂χ : ∀ {τ₁ τ₂ l} {t₁ : CTerm χ} {t₂ : CTerm τ₁} ->
          _⟨✴⟩_ {τ₂ = τ₂} (Labeledχ l t₁) (Labeled l t₂) ⇝ (Labeledχ l t₁)

  ⟨✴⟩₃χ : ∀ {τ₁ τ₂ l} {t₁ : CTerm (τ₁ ➔ τ₂)} {t₂ : CTerm χ} ->
          _⟨✴⟩_ {τ₂ = τ₂} (Labeled l t₁) (Labeledχ l t₂) ⇝ (Labeledχ l t₂)

  ⟨✴⟩₄χ : ∀ {τ₁ τ₂ l} {t₁ : CTerm χ} {t₂ : CTerm χ} ->
          _⟨✴⟩_ {τ₁ = τ₁} {τ₂ = τ₂} (Labeledχ l t₁) (Labeledχ l t₂) ⇝ (Labeledχ l t₁)

  Relabelχ : ∀ {τ l h} {t : CTerm χ} -> (l⊑h : l ⊑ h) ->
             relabel {τ = τ} l⊑h (Labeledχ l t) ⇝ Labeledχ h t

  Relabel∙χ : ∀ {τ l h} {t : CTerm χ} -> (l⊑h : l ⊑ h) ->
             relabel∙ {τ = τ} l⊑h (Labeledχ l t) ⇝ (Labeled h ∙)

  -- Bullet reduces to itself. We need this rule because ∙ is not a value.
  Hole : ∀ {τ : Ty} -> (∙ {τ = τ}) ⇝ ∙

infixr 3 _⇝_

-- A term t is a pure redex (PRedex t) if it can step further under
-- the pure semantics (⇝).
data PRedex {τ : Ty} (t₁ : CTerm τ) : Set where
  Step : {t₂ : CTerm τ} -> t₁ ⇝ t₂ -> PRedex t₁

--------------------------------------------------------------------------------
-- Semantics of impure calculus, i.e., MAC computations.

mutual
  infixl 1 _⟼_

  -- Small step semantics of programs
  data _⟼_ : ∀ {τ} -> Program τ -> Program τ -> Set where
    Lift : ∀ {τ} {Σ : Store} {t₁ t₂ : CTerm τ} -> t₁ ⇝ t₂ -> ⟨ Σ , t₁ ⟩ ⟼ ⟨ Σ , t₂ ⟩

    Label' : ∀ {l h τ Σ} {t : CTerm τ} -> (l⊑h : l ⊑ h) -> ⟨ Σ , label l⊑h t ⟩ ⟼ ⟨ Σ , return l (Labeled h t)⟩
    Unlabel₁ : ∀ {l h τ Σ Σ'} {t t' : CTerm (Labeled l τ)} (l⊑h : l ⊑ h)
             -> ⟨ Σ , t ⟩ ⟼ ⟨ Σ' , t' ⟩
             -> ⟨ Σ , unlabel l⊑h t ⟩ ⟼ ⟨ Σ' , unlabel l⊑h t' ⟩
    Unlabel₂ : ∀ {l h τ Σ} {t : CTerm τ} -> (l⊑h : l ⊑ h) -> ⟨ Σ , unlabel l⊑h (Labeled l t) ⟩ ⟼ ⟨ Σ , return h t ⟩
    Unlabelχ : ∀ {l h τ Σ} {t : CTerm χ} -> (l⊑h : l ⊑ h) -> ⟨ Σ , unlabel {τ = τ} l⊑h (Labeledχ l t) ⟩ ⟼ ⟨ Σ , throw h t ⟩

    Bind₁ : ∀ {l τ₁ τ₂ Σ₁ Σ₂} {t₁ t₁' : CTerm (Mac l τ₁)} {t₂ : CTerm (τ₁ ➔ Mac l τ₂)} ->
                 ⟨ Σ₁ , t₁ ⟩ ⟼ ⟨ Σ₂ , t₁' ⟩ ->
                ⟨ Σ₁ , t₁ >>= t₂ ⟩ ⟼ ⟨ Σ₂ , t₁' >>= t₂ ⟩

    Bind₂ : ∀ {l τ₁ τ₂ Σ} {t₁ : CTerm τ₁} {t₂ : CTerm (τ₁ ➔ Mac l τ₂)} ->
                ⟨ Σ , return l t₁ >>= t₂ ⟩ ⟼ ⟨ Σ , t₂ ∘ t₁ ⟩

    Bind₃ : ∀ {l τ₁ τ₂ Σ} {t₁ : CTerm χ} {t₂ : CTerm (τ₁ ➔ Mac l τ₂)} ->
                ⟨ Σ , throw l t₁ >>= t₂ ⟩ ⟼ ⟨ Σ , throw l t₁ ⟩

    Join : ∀ {l h τ Σ Σ'} {t : CTerm (Mac h τ)} {t' : CTerm τ} (l⊑h : l ⊑ h)
           -> ⟨ Σ , t ⟩ ⇓ ⟨ Σ' , return h t' ⟩
           -> ⟨ Σ , join l⊑h t ⟩ ⟼ ⟨ Σ' , return l (Labeled h t') ⟩

    Joinχ : ∀ {l h τ Σ Σ'} {t : CTerm (Mac h τ)} {t' : CTerm χ} (l⊑h : l ⊑ h)
           -> ⟨ Σ , t ⟩ ⇓ ⟨ Σ' , throw h t' ⟩
           -> ⟨ Σ , join l⊑h t ⟩ ⟼ ⟨ Σ' , return l (Labeledχ h t') ⟩

    Join∙ : ∀ {l h Σ} {τ : Ty} {t : CTerm (Mac h τ)} (l⊑h : l ⊑ h)
          -> ⟨ Σ , join∙ l⊑h t ⟩ ⟼ ⟨ Σ , return l (Labeled h ∙) ⟩

    Catch₁ : ∀ {l τ Σ Σ'} {t₁ t₁' : CTerm (Mac l τ)} {t₂ : CTerm (χ ➔ (Mac l τ))}
             -> ⟨ Σ , t₁ ⟩ ⟼ ⟨ Σ' , t₁' ⟩
             -> ⟨ Σ , catch t₁ t₂ ⟩ ⟼ ⟨ Σ' , catch t₁' t₂ ⟩

    Catch₂ : ∀ {l τ Σ} {t₁ : CTerm τ} {t₂ : CTerm (χ ➔ (Mac l τ))}
             -> ⟨ Σ , catch (return l t₁) t₂ ⟩ ⟼ ⟨ Σ ,  return l t₁ ⟩

    Catch₃ : ∀ {l τ Σ} {t₁ : CTerm χ} {t₂ : CTerm (χ ➔ (Mac l τ))}
             -> ⟨ Σ , catch (throw l t₁) t₂ ⟩ ⟼ ⟨ Σ ,  t₂ ∘ t₁ ⟩

    --------------------------------------------------------------------------------
    -- Memory operations

    New : ∀ {Σ l h τ} {t : CTerm τ} (l⊑h : l ⊑ h) ->
            let M = Σ h in
            ⟨ Σ , new l⊑h t ⟩ ⟼ ⟨ Σ [ h ↦ (newᴹ ⟦ t ⟧ M) ]ˢ , return l (Ref h #[ lengthᴹ M ]) ⟩

    Write₁ : ∀ {Σ Σ' l h τ} {t₁ : CTerm τ} {t₂ t₂' : CTerm (Ref h τ)} (l⊑h : l ⊑ h)
             -> ⟨ Σ , t₂ ⟩ ⟼ ⟨ Σ' , t₂' ⟩
             -> ⟨ Σ , write l⊑h t₁ t₂ ⟩ ⟼ ⟨ Σ' , write l⊑h t₁ t₂' ⟩

    Write₂ : ∀ {Σ l h τ} {n : ℕ} {t : CTerm τ} {M₂ : Memory h} (l⊑h : l ⊑ h)
             -> let M₁ = Σ h in
                (w : M₂ ≔ M₁ [ n ↦ ⟦ t ⟧ ]ᴹ)
             -> ⟨ Σ , write l⊑h t (Ref h #[ n ]) ⟩ ⟼ ⟨ Σ [ h ↦ M₂ ]ˢ , return l （） ⟩

    Read₁ : ∀ {Σ Σ' l h τ} {t t' : CTerm (Ref l τ)} (l⊑h : l ⊑ h)
             -> ⟨ Σ , t ⟩ ⟼ ⟨ Σ' , t' ⟩
             -> ⟨ Σ , read l⊑h t  ⟩ ⟼ ⟨ Σ' , read l⊑h t' ⟩

    Read₂ : ∀ {Σ l h τ} {n : ℕ} {t : CTerm τ} (l⊑h : l ⊑ h)
            -> (r : n ↦ ⟦ t ⟧ ∈ᴹ (Σ l))
            -> ⟨ Σ , read l⊑h (Ref l #[ n ]) ⟩ ⟼ ⟨ Σ , return h t ⟩

    -- Memory operations with ∙
    New∙ : ∀ {Σ l h τ} {t : CTerm τ} (l⊑h : l ⊑ h) ->
            ⟨ Σ , new∙ l⊑h t ⟩ ⟼ ⟨ Σ , return l (Ref h ∙) ⟩

    Write∙₁ : ∀ {Σ Σ' l h τ} {t₁ : CTerm τ} {t₂ t₂' : CTerm (Ref h τ)} (l⊑h : l ⊑ h)
             -> ⟨ Σ , t₂ ⟩ ⟼ ⟨ Σ' , t₂' ⟩
             -> ⟨ Σ , write∙ l⊑h t₁ t₂ ⟩ ⟼ ⟨ Σ' , write∙ l⊑h t₁ t₂' ⟩

    Write∙₂ : ∀ {Σ l h τ} {t₁ : CTerm τ} {t₂ : CTerm _} (l⊑h : l ⊑ h) ->
              ⟨ Σ , write∙ l⊑h t₁ (Ref h t₂) ⟩ ⟼ ⟨ Σ , return l （） ⟩

    --------------------------------------------------------------------------------
    -- Concurrency

    Fork : ∀ {l h Σ} {t : CTerm (Mac h （）)} (l⊑h : l ⊑ h) ->
             ⟨ Σ , fork l⊑h t ⟩ ⟼ ⟨ Σ , return l （） ⟩

    Fork∙ : ∀ {l h Σ} {t : CTerm (Mac h （）)} (l⊑h : l ⊑ h) ->
             ⟨ Σ , fork∙ l⊑h t ⟩ ⟼ ⟨ Σ , return l （） ⟩

    --------------------------------------------------------------------------------
    -- Operations on synchronization variables

    NewMVar : ∀ {Σ l h τ} -> (l⊑h : l ⊑ h) ->
              let M = Σ h in
            ⟨ Σ , newMVar {τ = τ} l⊑h ⟩ ⟼ ⟨ Σ [ h ↦ (newᴹ {τ = τ} ⊗ M) ]ˢ , return l (MVar h #[ lengthᴹ M ]) ⟩

    Take₁ : ∀ {Σ Σ' l τ} {t t' : CTerm (MVar l τ)}
             -> ⟨ Σ , t ⟩ ⟼ ⟨ Σ' , t' ⟩
             -> ⟨ Σ , take t  ⟩ ⟼ ⟨ Σ' , take t' ⟩

    Take₂ : ∀ {Σ l τ} {n : ℕ} {t : CTerm τ} {M₂ : Memory l} ->
            let M₁ = Σ l in
               (r : n ↦ ⟦ t ⟧ ∈ᴹ M₁)
            -> (w : M₂ ≔ M₁ [ n ↦ (⊗ {τ = τ}) ]ᴹ)
            -> ⟨ Σ , take (MVar l #[ n ]) ⟩ ⟼ ⟨ Σ [ l ↦ M₂ ]ˢ , return l t ⟩

    Put₁ : ∀ {Σ Σ' l τ} {t₁ : CTerm τ} {t₂ t₂' : CTerm (MVar l τ)}
             -> ⟨ Σ , t₂ ⟩ ⟼ ⟨ Σ' , t₂' ⟩
             -> ⟨ Σ , put t₁ t₂  ⟩ ⟼ ⟨ Σ' , put t₁ t₂' ⟩

    Put₂ : ∀ {Σ l τ} {n : ℕ} {t : CTerm τ} {M₂ : Memory l} ->
             let M₁ = Σ l in
               (r : n ↦ (⊗ {τ}) ∈ᴹ (Σ l))
            -> (w : M₂ ≔ M₁ [ n ↦ ⟦ t ⟧ ]ᴹ)
            -> ⟨ Σ , put t (MVar l #[ n ]) ⟩ ⟼ ⟨ Σ [ l ↦ M₂ ]ˢ , return l （） ⟩

    NewMVar∙ : ∀ {Σ l h τ} -> (l⊑h : l ⊑ h) ->
            ⟨ Σ , newMVar∙ {τ = τ} l⊑h ⟩ ⟼ ⟨ Σ , return l (MVar h ∙) ⟩

  -- The transitive reflexive closure of the small step relation
  data _⟼⋆_  {τ : Ty} : Program τ -> Program τ -> Set where
    [] : {Σ : Store} {c : CTerm τ} -> ⟨ Σ , c ⟩  ⟼⋆ ⟨ Σ , c ⟩
    _∷_ : {Σ₁ Σ₂ Σ₃ : Store} {c₁ c₂ c₃ : CTerm τ}
          -> (s : ⟨ Σ₁ , c₁ ⟩ ⟼ ⟨ Σ₂ , c₂ ⟩)
          -> (ss : ⟨ Σ₂ , c₂ ⟩ ⟼⋆ ⟨ Σ₃ , c₃ ⟩)
          -> ⟨ Σ₁ , c₁ ⟩ ⟼⋆ ⟨ Σ₃ , c₃ ⟩

  -- A big step is made of a finite number (possibly 0) of reduction steps that reduces a term to a value.
  data _⇓_ {τ : Ty} : Program τ -> Program τ -> Set where
    ⌞_,_⌟ : ∀ {Σ Σ' : Store} {t t' : CTerm τ}
            -> (ss : ⟨ Σ , t ⟩ ⟼⋆ ⟨ Σ' , t' ⟩)
            -> (v : Value t')
            -> ⟨ Σ , t ⟩ ⇓ ⟨ Σ' , t' ⟩

--------------------------------------------------------------------------------
-- Program Status: the operational semantics of this language and its
-- values give rise to three statuses, that are formally defined here.

open import Data.Product
open import Data.Sum

-- A program is done if the term is a value.
Doneᴾ : ∀ {τ} -> Program τ -> Set
Doneᴾ p = Value (term p)

-- A program p is a redex (Redexᴾ p), if it can step further.
data Redexᴾ {τ} (p : Program τ) : Set where
  Step : ∀ {p'} -> p ⟼ p' -> Redexᴾ p

-- A program is stuck if it cannot step and it is not done.
Stuckᴾ : ∀ {τ} -> Program τ -> Set
Stuckᴾ p = (¬ Doneᴾ p) × (¬ (Redexᴾ p))

-- A program can be in one of these three status.
Stateᴾ : ∀ {τ} -> Program τ → Set
Stateᴾ p = Redexᴾ p ⊎ Doneᴾ p ⊎ Stuckᴾ p

-- Proof that done programs do not step.
¬doneSteps : ∀ {τ} {p₁ : Program τ} -> Doneᴾ p₁ -> ¬ (Redexᴾ p₁)
¬doneSteps （） (Step (Lift ()))
¬doneSteps True (Step (Lift ()))
¬doneSteps False (Step (Lift ()))
¬doneSteps (Λ t) (Step (Lift ()))
¬doneSteps ξ (Step (Lift ()))
¬doneSteps (return t) (Step (Lift ()))
¬doneSteps (throw t) (Step (Lift ()))
¬doneSteps (Labeled t) (Step (Lift ()))
¬doneSteps (Labeledχ t) (Step (Lift ()))
¬doneSteps (Ref t) (Step (Lift ()))
¬doneSteps (MVar t) (Step (Lift ()))

--------------------------------------------------------------------------------
