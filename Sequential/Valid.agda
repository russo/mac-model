-- In this module we define the valid portion of the calculus and we
-- show that an initial configuration is valid and that validity is
-- preserved by the operational semantics.
--
-- In particular the valid statement restricts the calculus to the
-- surface concurrent fragment and ensures memory safety.
--
-- Concretely the statement is defined as a function that maps invalid
-- terms, e.g., ∙, to the empty type (⊥), and constraints the
-- addresses contained in labeled references to be within the bounds
-- of the corresponding memory.

import Lemmas
import Lattice as L

module Sequential.Valid (𝓛 : L.Lattice) where

open import Types 𝓛 hiding (wken-∈)
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality hiding (subst ; [_])
open import Function using (id)
open import Data.Nat hiding (_≟_)
open import Data.Empty
open import Data.Product as P

--------------------------------------------------------------------------------
-- Memory Safety

-- ValidAddr τ M n is the proof that M [ n ] contains a cell of type
-- τ.  At the same time it guarantees that n is a valid index and that
-- the memory is well-typed.
data ValidAddr {l} (τ : Ty) : Memory l -> ℕ -> Set where
  here : ∀ {M : Memory l} {c : Cell τ} -> ValidAddr τ (c ∷ M) 0
  there : ∀ {n τ'} {c : Cell τ'} {M : Memory l} -> ValidAddr τ M n -> ValidAddr τ (c ∷ M) (suc n)

-- The proof that a term of type Addr is a value address
data IsAddr {Γ} : Term Γ Addr -> Set where
  is#[_] : (n : ℕ) -> IsAddr #[ n ]

-- Convienence function that extracts the raw address (a number) from
-- an address term.
getAddr : ∀ {Γ} {t : Term Γ Addr} -> IsAddr t -> ℕ
getAddr is#[ n ] = n

readᴹ-∈ : ∀ {l n τ} {M : Memory l} → ValidAddr τ M n → P.Σ (Cell τ) (λ c → n ↦ c ∈ᴹ M)
readᴹ-∈ here = _ , here
readᴹ-∈ (there p) = P.map id there (readᴹ-∈ p)

writeᴹ↦ : ∀ {l n τ} {M : Memory l} {c c' : Cell τ} → n ↦ c ∈ᴹ M → ∃ (λ M' → M' ≔ M [ n ↦ c' ]ᴹ)
writeᴹ↦ here = _ , here
writeᴹ↦ (there p) = P.map (_∷_ _) there (writeᴹ↦ p)

--------------------------------------------------------------------------------

-- A valid term contains valid references, i.e., references with a
-- valid address in memory.  Bullet terms such as ∙, write∙, new∙
-- etc., are not valid, because they are not part of the surface
-- syntax (they are a device of the proof-technique). Furthermore, we
-- consider join and Labeledχ invalid constructs, because they are
-- avaialble only in the sequential calculus.
valid : ∀ {τ Γ} -> Store -> Term Γ τ -> Set
valid Σ (var τ∈Γ) = ⊤
valid Σ (Λ t) = valid Σ t
valid Σ (t ∘ t₁) = valid Σ t × valid Σ t₁
valid Σ （） = ⊤
valid Σ True = ⊤
valid Σ False = ⊤
valid Σ (if t then t₁ else t₂) = valid Σ t × valid Σ t₁ × valid Σ t₂
valid Σ (return l t) = valid Σ t
valid Σ (t >>= t₁) = valid Σ t × valid Σ t₁
valid Σ ξ = ⊤
valid Σ (throw l t) = valid Σ t
valid Σ (catch t t₁) = valid Σ t × valid Σ t₁
valid Σ (Labeled l t) = valid Σ t
valid Σ (Labeledχ l t) = ⊥
valid Σ (label l⊑h t) = valid Σ t
valid Σ (unlabel l⊑h t) = valid Σ t
valid Σ (join l⊑h t) = ⊥
valid Σ (join∙ l⊑h t) = ⊥
valid Σ (fork l⊑h t) = valid Σ t
valid Σ (fork∙ l⊑h t) = ⊥
valid Σ #[ n ] = ⊤
valid Σ (Ref {τ = τ} l t) = P.Σ (IsAddr t) (λ a -> ValidAddr τ (Σ l) (getAddr a))
valid Σ (read l⊑h t) = valid Σ t
valid Σ (write l⊑h t t₁) = valid Σ t × valid Σ t₁
valid Σ (write∙ l⊑h t t₁) = ⊥
valid Σ (new l⊑h t) = valid Σ t
valid Σ (new∙ l⊑h t) = ⊥
valid Σ (MVar {τ = τ} l t) = P.Σ (IsAddr t) (λ a -> ValidAddr τ (Σ l) (getAddr a))
valid Σ (newMVar l⊑h) = ⊤
valid Σ (newMVar∙ l⊑h) = ⊥
valid Σ (take t) = valid Σ t
valid Σ (put t t₁) = valid Σ t × valid Σ t₁
valid Σ (t ⟨$⟩ t₁) = valid Σ t × valid Σ t₁
valid Σ (t ⟨$⟩∙ t₁) = ⊥
valid Σ (t ⟨✴⟩ t₁) = valid Σ t × valid Σ t₁
valid Σ (t ⟨✴⟩∙ t₁) = ⊥
valid Σ (relabel l⊑h t) = valid Σ t
valid Σ (relabel∙ l⊑h t) = ⊥
valid Σ ∙ = ⊥

-- A valid cell is either empty or contains a valid term
validᶜ : ∀ {τ} -> (Σ : Store) (c : Cell τ) -> Set
validᶜ Σ ⊗ = ⊤
validᶜ Σ ⟦ t ⟧ = valid Σ t

-- A valid memory contains valid cells
validᴹ : ∀ {l} -> (Σ : Store) (M : Memory l) -> Set
validᴹ _ [] = ⊤
validᴹ Σ (c ∷ M) = validᶜ Σ c × validᴹ Σ M
validᴹ _ ∙ = ⊥

-- A store is valid, if all memories are valid with respect to it.
validˢ : Store -> Set
validˢ Σ = ∀ (l : Label) → validᴹ Σ (Σ l)

-- A valid program contains a valid store and a valid term
validᴾ : ∀ {τ} -> Program τ -> Set
validᴾ ⟨ Σ , t ⟩ = validˢ Σ × valid Σ t

--------------------------------------------------------------------------------
-- The semantics extends the memory and we prove that whatever was
-- already valid remains valid after the extensions.

-- Memory Extension
-- M₁ ⊆ᴹ M₂ denotes that M₂ is an extension of memory M₁.
data _⊆ᴹ_ {l : Label} : Memory l -> Memory l -> Set where
  ∙ : ∙ ⊆ᴹ ∙
  nil : ∀ {M} -> [] ⊆ᴹ M
  cons : ∀ {τ M₁ M₂} {c₁ c₂ : Cell τ} -> M₁ ⊆ᴹ M₂ -> (c₁ ∷ M₁) ⊆ᴹ (c₂ ∷ M₂)

-- Store extension
-- Σ₁ ⊆ˢ Σ₂ iff the second store refines extends the first
_⊆ˢ_ : Store -> Store -> Set
Σ₁ ⊆ˢ Σ₂ = ∀ (l : Label) -> (Σ₁ l) ⊆ᴹ (Σ₂ l)

-- _⊆ᴹ_ is reflexive
refl-⊆ᴹ : ∀ {l} {M : Memory l} -> M ⊆ᴹ M
refl-⊆ᴹ {M = []} = nil
refl-⊆ᴹ {M = cᴸ ∷ M} = cons refl-⊆ᴹ
refl-⊆ᴹ {M = ∙} = ∙

-- _⊆ˢ_ is reflexive
refl-⊆ˢ : ∀ {Σ : Store} -> Σ ⊆ˢ Σ
refl-⊆ˢ {Σ} = λ l → refl-⊆ᴹ

-- Any valid address in some memory M₁ remains valid in any memory M₂ that extends M₁
addr-⊆ : ∀ {n l τ} {M₁ M₂ : Memory l} -> M₁ ⊆ᴹ M₂ -> ValidAddr τ M₁ n -> ValidAddr τ M₂ n
addr-⊆ (cons x) here = here
addr-⊆ (cons x) (there y) = there (addr-⊆ x y)

-- If a term t is valid in a store Σ, then it is also valid in any extended store Σ'.
valid-⊆ : ∀ {τ Γ Σ Σ'} -> (t : Term Γ τ) -> valid Σ t -> Σ ⊆ˢ Σ' -> valid Σ' t
valid-⊆ (var τ∈Γ) v Σ⊆Σ' = tt
valid-⊆ (Λ t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (t ∘ t₁) v Σ⊆Σ' = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₂ v) Σ⊆Σ')
valid-⊆ （） v Σ⊆Σ' = tt
valid-⊆ True v Σ⊆Σ' = tt
valid-⊆ False v Σ⊆Σ' = tt
valid-⊆ (if t then t₁ else t₂) v Σ⊆Σ'
  = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₁ (proj₂ v)) Σ⊆Σ') , valid-⊆ t₂ (proj₂ (proj₂ v)) Σ⊆Σ'
valid-⊆ (return l t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (t >>= t₁) v Σ⊆Σ' = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₂ v) Σ⊆Σ')
valid-⊆ ξ v Σ⊆Σ' = tt
valid-⊆ (throw l t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (catch t t₁) v Σ⊆Σ' = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₂ v) Σ⊆Σ')
valid-⊆ (Labeled l t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (Labeledχ l t) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (label l⊑h t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (unlabel l⊑h t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (join l⊑h t) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (join∙ l⊑h t) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (fork l⊑h t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (fork∙ l⊑h t) v Σ⊆Σ' = ⊥-elim v
valid-⊆ #[ n ] v Σ⊆Σ' = tt
valid-⊆ (Ref l t) (proj₃ , proj₄) Σ⊆Σ' = proj₃ , (addr-⊆ (Σ⊆Σ' l) proj₄)
valid-⊆ (read l⊑h t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (write l⊑h t t₁) v Σ⊆Σ' = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₂ v) Σ⊆Σ')
valid-⊆ (write∙ l⊑h t t₁) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (new l⊑h t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (new∙ l⊑h t) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (MVar l t) (proj₃ , proj₄) Σ⊆Σ' = proj₃ , (addr-⊆ (Σ⊆Σ' l) proj₄)
valid-⊆ (newMVar l⊑h) v Σ⊆Σ' = tt
valid-⊆ (newMVar∙ l⊑h) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (take t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (put t t₁) v Σ⊆Σ' = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₂ v) Σ⊆Σ')
valid-⊆ (t ⟨$⟩ t₁) v Σ⊆Σ' = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₂ v) Σ⊆Σ')
valid-⊆ (t ⟨$⟩∙ t₁) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (t ⟨✴⟩ t₁) v Σ⊆Σ' = (valid-⊆ t (proj₁ v) Σ⊆Σ') , (valid-⊆ t₁ (proj₂ v) Σ⊆Σ')
valid-⊆ (t ⟨✴⟩∙ t₁) v Σ⊆Σ' = ⊥-elim v
valid-⊆ (relabel l⊑h t) v Σ⊆Σ' = valid-⊆ t v Σ⊆Σ'
valid-⊆ (relabel∙ l⊑h t) v Σ⊆Σ' = ⊥-elim v
valid-⊆ ∙ v Σ⊆Σ' = ⊥-elim v

-- Any valid cell remains valid in an extended store.
valid-⊆ᶜ : ∀ {τ Σ Σ'} -> (c : Cell τ) -> validᶜ Σ c -> Σ ⊆ˢ Σ' -> validᶜ Σ' c
valid-⊆ᶜ ⊗ tt Σ⊆Σ' = tt
valid-⊆ᶜ ⟦ x ⟧ v Σ⊆Σ' = valid-⊆ x v Σ⊆Σ'

-- Any valid memory remains valid in an extended store.
validᴹ-⊆ : ∀ {l Σ Σ'} -> (M : Memory l) -> validᴹ Σ M -> Σ ⊆ˢ Σ' -> validᴹ Σ' M
validᴹ-⊆ ∙ () Σ⊆Σ'
validᴹ-⊆ [] tt Σ⊆Σ' = tt
validᴹ-⊆ (x ∷ M) (proj₃ , proj₄) Σ⊆Σ' = valid-⊆ᶜ x proj₃ Σ⊆Σ' , validᴹ-⊆ M proj₄ Σ⊆Σ'

-- Note that we cannot upgrade stores directly, i.e., the function:
-- validˢ-⊆ : ∀ {l Σ Σ'} -> validˢ Σ -> Σ ⊆ˢ Σ' -> valid Σ' After
-- upgrading pointwise the store that the memories point to (using
-- validᴹ-⊆), we need to upgrade the memories as well (Σ' might
-- contain extended memories).  We cannot do that because we need
-- further assumptions about the extended memories M' namely that it
-- is valid with respect to the new store Σ'.  We get around this
-- limitiation, by working on specific memories operations as follows.

--------------------------------------------------------------------------------
-- Helper lemmas.
-- Memory and store operations extend memories and stores.

-- Allocating a new cell on a memory extends the memory
newᴹ-⊆ᴹ : ∀ {τ l} -> (c : Cell τ) (M : Memory l) ->
          M ⊆ᴹ newᴹ c M
newᴹ-⊆ᴹ c ∙ = ∙
newᴹ-⊆ᴹ c [] = nil
newᴹ-⊆ᴹ c (x ∷ M) = cons (newᴹ-⊆ᴹ c M)

-- Writing in a memory satisfies the extend relation
writeᴹ-⊆ᴹ : ∀ {l n τ} {M₁ M₂ : Memory l} {c : Cell τ} ->
              M₂ ≔ M₁ [ n ↦ c ]ᴹ ->  M₁ ⊆ᴹ M₂
writeᴹ-⊆ᴹ here = cons refl-⊆ᴹ
writeᴹ-⊆ᴹ (there x) = cons (writeᴹ-⊆ᴹ x)

-- Updating a store with an extending memory preserves results in an extended store
updateˢ-⊆ˢ : (Σ : Store) (l : Label) {M' : Memory l} ->
           let M = Σ l in
               M ⊆ᴹ M' ->  Σ ⊆ˢ (Σ [ l ↦ M' ]ˢ)
updateˢ-⊆ˢ Σ l M⊆M' l' with l ≟ l'
... | yes refl = M⊆M'
... | no _ = refl-⊆ᴹ

-- Allocating a new cell in a memory in a store extends the store
newᴹ-⊆ˢ : ∀ {τ} (Σ : Store) (l : Label) (c : Cell τ) ->
           let M = Σ l
               Σ' = Σ [ l ↦ newᴹ c M ]ˢ in  Σ ⊆ˢ Σ'
newᴹ-⊆ˢ Σ l c l' with l ≟ l'
newᴹ-⊆ˢ Σ l c .l | yes refl = newᴹ-⊆ᴹ c (Σ l)
newᴹ-⊆ˢ Σ l c l' | no ¬p = refl-⊆ᴹ

-- Our semantics extends the store
step-⊆ˢ : ∀ {τ} {p₁ p₂ : Program τ} -> validᴾ p₁ -> p₁ ⟼ p₂ -> (store p₁) ⊆ˢ (store p₂)
step-⊆ˢ v (Lift x) = refl-⊆ˢ
step-⊆ˢ v (Label' l⊑h) = refl-⊆ˢ
step-⊆ˢ v (Unlabel₁ l⊑h (Lift x)) = refl-⊆ˢ
step-⊆ˢ v (Unlabel₂ l⊑h) = refl-⊆ˢ
step-⊆ˢ v (Unlabelχ l⊑h) = ⊥-elim (proj₂ v)
step-⊆ˢ v (Bind₁ step) = step-⊆ˢ ((proj₁ v) , (proj₁ (proj₂ v))) step
step-⊆ˢ v Bind₂ = refl-⊆ˢ
step-⊆ˢ v Bind₃ = refl-⊆ˢ
step-⊆ˢ v (Join l⊑h x) = ⊥-elim (proj₂ v)
step-⊆ˢ v (Joinχ l⊑h x) = ⊥-elim (proj₂ v)
step-⊆ˢ v (Join∙ l⊑h) = ⊥-elim (proj₂ v)
step-⊆ˢ v (Catch₁ step) = step-⊆ˢ ((proj₁ v) , (proj₁ (proj₂ v))) step
step-⊆ˢ v Catch₂ = refl-⊆ˢ
step-⊆ˢ v Catch₃ = refl-⊆ˢ
step-⊆ˢ v (New {h = h} l⊑h) = newᴹ-⊆ˢ _ h ⟦ _ ⟧
step-⊆ˢ v (Write₁ l⊑h (Lift x)) = refl-⊆ˢ
step-⊆ˢ v (Write₂ l⊑h w) = updateˢ-⊆ˢ _ _ (writeᴹ-⊆ᴹ w)
step-⊆ˢ v (Read₁ l⊑h (Lift x)) = refl-⊆ˢ
step-⊆ˢ v (Read₂ l⊑h r) = refl-⊆ˢ
step-⊆ˢ v (New∙ l⊑h) = ⊥-elim (proj₂ v)
step-⊆ˢ v (Write∙₁ l⊑h step) = ⊥-elim (proj₂ v)
step-⊆ˢ v (Write∙₂ l⊑h) = ⊥-elim (proj₂ v)
step-⊆ˢ _ (Fork l⊑h) = refl-⊆ˢ
step-⊆ˢ v (Fork∙ l⊑h) = ⊥-elim (proj₂ v)
step-⊆ˢ v (NewMVar l⊑h) = newᴹ-⊆ˢ _ _ ⊗
step-⊆ˢ v (Take₁ (Lift x)) = refl-⊆ˢ
step-⊆ˢ v (Take₂ r w) = updateˢ-⊆ˢ _ _ (writeᴹ-⊆ᴹ w)
step-⊆ˢ v (Put₁ (Lift x)) = refl-⊆ˢ
step-⊆ˢ v (Put₂ r w) = updateˢ-⊆ˢ _ _ (writeᴹ-⊆ᴹ w)
step-⊆ˢ v (NewMVar∙ l⊑h) = ⊥-elim (proj₂ v)

--------------------------------------------------------------------------------
-- Initial Valid configurations

validˢ₀ : validˢ Σ₀
validˢ₀ = λ l → tt

-- Given a valid term, the initial configuration is valid.
valid₀ : ∀ {τ} -> (t : CTerm τ) -> {{v : valid Σ₀ t}} -> validᴾ ⟨ Σ₀ , t ⟩
valid₀ t {{v}} = validˢ₀ , v

--------------------------------------------------------------------------------
-- The operational semantics preserves validity of terms and stores

-- Weakening preserves validity of terms
valid-wken : ∀ {τ Γ₁ Γ₂} {Σ : Store} (t : Term Γ₁ τ) -> (v : valid Σ t) -> (Γ₁⊆Γ₂ : Γ₁ ⊆ Γ₂) -> valid Σ (wken t Γ₁⊆Γ₂)
valid-wken (var τ∈Γ) v Γ₁⊆Γ₂ = tt
valid-wken (Λ t) v Γ₁⊆Γ₂ = valid-wken t v (cons Γ₁⊆Γ₂)
valid-wken (t ∘ t₁) v Γ₁⊆Γ₂ = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , valid-wken t₁ (proj₂ v) Γ₁⊆Γ₂
valid-wken （） v Γ₁⊆Γ₂ = tt
valid-wken True v Γ₁⊆Γ₂ = tt
valid-wken False v Γ₁⊆Γ₂ = tt
valid-wken (if t then t₁ else t₂) v Γ₁⊆Γ₂
  = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , ((valid-wken t₁ (proj₁ (proj₂ v)) Γ₁⊆Γ₂) , (valid-wken t₂ (proj₂ (proj₂ v)) Γ₁⊆Γ₂))
valid-wken (return l t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (t >>= t₁) v Γ₁⊆Γ₂ = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , (valid-wken t₁ (proj₂ v) Γ₁⊆Γ₂)
valid-wken ξ v Γ₁⊆Γ₂ = tt
valid-wken (throw l t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (catch t t₁) v Γ₁⊆Γ₂ = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , (valid-wken t₁ (proj₂ v) Γ₁⊆Γ₂)
valid-wken (Labeled l t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (Labeledχ l t) () Γ₁⊆Γ₂
valid-wken (label l⊑h t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (unlabel l⊑h t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (join l⊑h t) () Γ₁⊆Γ₂
valid-wken (join∙ l⊑h t) () Γ₁⊆Γ₂
valid-wken (fork l⊑h t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (fork∙ l⊑h t) () Γ₁⊆Γ₂
valid-wken #[ n ] v Γ₁⊆Γ₂ = tt
valid-wken (Ref l .(#[ n ])) (is#[ n ] , proj₄) Γ₁⊆Γ₂ = is#[ n ] , proj₄
valid-wken (read l⊑h t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (write l⊑h t t₁) v Γ₁⊆Γ₂ = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , (valid-wken t₁ (proj₂ v) Γ₁⊆Γ₂)
valid-wken (write∙ l⊑h t t₁) () Γ₁⊆Γ₂
valid-wken (new l⊑h t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (new∙ l⊑h t) () Γ₁⊆Γ₂
valid-wken (MVar l .(#[ n ])) (is#[ n ] , proj₄) Γ₁⊆Γ₂ = is#[ n ] , proj₄
valid-wken (newMVar l⊑h) v Γ₁⊆Γ₂ = tt
valid-wken (newMVar∙ l⊑h) () Γ₁⊆Γ₂
valid-wken (take t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (put t t₁) v Γ₁⊆Γ₂ = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , (valid-wken t₁ (proj₂ v) Γ₁⊆Γ₂)
valid-wken (t ⟨$⟩ t₁) v Γ₁⊆Γ₂ = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , (valid-wken t₁ (proj₂ v) Γ₁⊆Γ₂)
valid-wken (t ⟨$⟩∙ t₁) () Γ₁⊆Γ₂
valid-wken (t ⟨✴⟩ t₁) v Γ₁⊆Γ₂ = (valid-wken t (proj₁ v) Γ₁⊆Γ₂) , (valid-wken t₁ (proj₂ v) Γ₁⊆Γ₂)
valid-wken (t ⟨✴⟩∙ t₁) () Γ₁⊆Γ₂
valid-wken (relabel l⊑h t) v Γ₁⊆Γ₂ = valid-wken t v Γ₁⊆Γ₂
valid-wken (relabel∙ l⊑h t) () Γ₁⊆Γ₂
valid-wken ∙ () Γ₁⊆Γ₂

-- Term substitution preserves validity
valid-subst : ∀ {τ τ'} {Σ : Store} {t₁ : CTerm τ} {t₂ : Term (τ ∷ []) τ'} -> valid Σ t₁ -> valid Σ t₂ -> valid Σ (subst t₁ t₂)
valid-subst {Σ = Σ} {t₁'} {t₂'} = aux [] [] t₁' t₂'
  where
        aux' : ∀ {τ₁ τ₂} (Γ₁ Γ₂ : Context) (t₁ : Term Γ₂ τ₂) (x : τ₁ ∈ (Γ₁ ++ [ τ₂ ] ++ Γ₂))
                 -> valid Σ t₁ -> valid Σ (var-subst Γ₁ Γ₂ t₁ x)
        aux' [] Γ₂ t₁ here isV = isV
        aux' [] Γ₂ t₁ (there τ∈Γ) isV = tt
        aux' (_ ∷ Γ₁) Γ₂ t₁ here isV = tt
        aux'  (_ ∷ Γ₁) Γ₂ t₁ (there x) isV
          = valid-wken (var-subst Γ₁ Γ₂ t₁ x) (aux' Γ₁ Γ₂ t₁ x isV) (drop refl-⊆)

        aux : ∀ {τ α} (Γ₁ Γ₂ : Context) (t₁ : Term Γ₂ α) (t₂ : Term (Γ₁ ++ [ α ] ++ Γ₂) τ) ->
                      valid Σ t₁ -> valid Σ t₂ -> valid Σ (tm-subst Γ₁ Γ₂ t₁ t₂)
        aux Γ₁ Γ₂ t₁ （） v₁ v₂ = tt
        aux Γ₁ Γ₂ t₁ True v₁ v₂ = tt
        aux Γ₁ Γ₂ t₁ False v₁ v₂ = tt
        aux Γ₁ Γ₂ t₁ ξ v₁ v₂ = tt

        aux Γ₁ Γ₂ t₁ (var τ∈Γ) v₁ v₂ = aux' Γ₁ Γ₂ t₁ (∈ᴿ-∈ τ∈Γ) v₁
        aux Γ₁ Γ₂ t₁ (Λ t₂) v₁ v₂ = aux (_ ∷ Γ₁) Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (t₂ ∘ t₃) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ (proj₁ v₂) , aux Γ₁ Γ₂ t₁ t₃ v₁ (proj₂ v₂)
        aux Γ₁ Γ₂ t₁ (if t₂ then t₃ else t₄) v₁ (proj₁ , proj₂ , proj₃)
          = aux Γ₁ Γ₂ t₁ t₂ v₁ proj₁ , aux Γ₁ Γ₂ t₁ t₃ v₁ proj₂ , aux Γ₁ Γ₂ t₁ t₄ v₁ proj₃
        aux Γ₁ Γ₂ t₁ (return l t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (throw l t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (catch t₂ t₃) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ (proj₁ v₂) , aux Γ₁ Γ₂ t₁ t₃ v₁ (proj₂ v₂)
        aux Γ₁ Γ₂ t₁ (Labeled l t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (Labeledχ l t₂) v₁ ()
        aux Γ₁ Γ₂ t₁ (join l⊑h t₂) v₁ ()
        aux Γ₁ Γ₂ t₁ (join∙ l⊑h t₂) v₁ ()
        aux Γ₁ Γ₂ t₁ (t₂ ⟨$⟩ t₃) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ (proj₁ v₂) , aux Γ₁ Γ₂ t₁ t₃ v₁ (proj₂ v₂)
        aux Γ₁ Γ₂ t₁ (t₂ ⟨$⟩∙ t₃) v₁ ()
        aux Γ₁ Γ₂ t₁ (t₂ ⟨✴⟩ t₃) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ (proj₁ v₂) , aux Γ₁ Γ₂ t₁ t₃ v₁ (proj₂ v₂)
        aux Γ₁ Γ₂ t₁ (t₂ ⟨✴⟩∙ t₃) v₁ ()
        aux Γ₁ Γ₂ t₁ (relabel l⊑h t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (relabel∙ l⊑h t₂) v₁ ()
        aux Γ₁ Γ₂ t₁ (t₂ >>= t₃) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ (proj₁ v₂) , aux Γ₁ Γ₂ t₁ t₃ v₁ (proj₂ v₂)
        aux Γ₁ Γ₂ t₁ (label l⊑h t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (unlabel l⊑h t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (Ref l .(#[ n ])) v₁ (is#[ n ] , proj₄) = is#[ n ] , proj₄
        aux Γ₁ Γ₂ t₁ (read x t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (write x t₂ t₃) v₁ (proj₁ , proj₂) = aux Γ₁ Γ₂ t₁ t₂ v₁ proj₁ , aux Γ₁ Γ₂ t₁ t₃ v₁ proj₂
        aux Γ₁ Γ₂ t₁ (write∙ x t₂ t₃) v₁ ()
        aux Γ₁ Γ₂ t₁ (new x t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (new∙ x t₂) v₁ ()
        aux Γ₁ Γ₂ t₁ (MVar l .(#[ n ])) v₁ (is#[ n ] , proj₄) = is#[ n ] , proj₄
        aux Γ₁ Γ₂ t₁ (newMVar l⊑h) v₁ v₂ = tt
        aux Γ₁ Γ₂ t₁ (newMVar∙ l⊑h) v₁ ()
        aux Γ₁ Γ₂ t₁ (take t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (put t₂ t₃) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ (proj₁ v₂) , aux Γ₁ Γ₂ t₁ t₃ v₁ (proj₂ v₂)
        aux Γ₁ Γ₂ t₁ #[ x ] v₁ v₂ = tt
        aux Γ₁ Γ₂ t₁ (fork l⊑h t₂) v₁ v₂ = aux Γ₁ Γ₂ t₁ t₂ v₁ v₂
        aux Γ₁ Γ₂ t₁ (fork∙ l⊑h t₂) v₁ ()
        aux Γ₁ Γ₂ t₁ ∙ v₁ ()

-- The pure semantics preserves validity of terms
valid⇝ : ∀ {τ Σ} {t₁ t₂ : CTerm τ} -> valid Σ t₁ -> t₁ ⇝ t₂ -> valid Σ t₂
valid⇝ v (App step) = (valid⇝ (proj₁ v) step) , proj₂ v
valid⇝ (v₁ , v₂) (Beta {t₁ = t₁} {t₂ = t₂}) = valid-subst {t₁ = t₂} {t₁} v₂ v₁
valid⇝ v (If₁ step) = (valid⇝ (proj₁ v) step) , (proj₁ (proj₂ v) , proj₂ (proj₂ v))
valid⇝ v If₂ = proj₁ (proj₂ v)
valid⇝ v If₃ = proj₂ (proj₂ v)
valid⇝ v Fmap = v
valid⇝ v (⟨✴⟩₁ step) = (valid⇝ (proj₁ v) step) , (proj₂ v)
valid⇝ v (⟨✴⟩₂ step) = (proj₁ v) , (valid⇝ (proj₂ v) step)
valid⇝ v ⟨✴⟩₃ = v
valid⇝ v (Relabel₁ l⊑h step) = valid⇝ v step
valid⇝ v (Relabel₂ l⊑h) = v
valid⇝ () Fmap∙
valid⇝ () (⟨✴⟩∙₁ step)
valid⇝ () (⟨✴⟩∙₂ step)
valid⇝ () ⟨✴⟩∙₃
valid⇝ () (Relabel∙₁ l⊑h step)
valid⇝ () (Relabel∙₂ l⊑h)
valid⇝ (() , proj₄) (⟨✴⟩₁χ step)
valid⇝ (() , proj₄) ⟨✴⟩₂χ
valid⇝ (_ , ()) ⟨✴⟩₃χ
valid⇝ (() , proj₄) ⟨✴⟩₄χ
valid⇝ () (Relabelχ l⊑h)
valid⇝ () (Relabel∙χ l⊑h)
valid⇝ () Hole

--------------------------------------------------------------------------------
-- Helper lemmas to show validity preservation memories.
-- Memory operations preserves validity

-- Allocating a valid cell on a valid memory results in a valid memory
new-validᴹ : ∀ {l τ Σ} (c : Cell τ) (M : Memory l)
             -> validᶜ Σ c -> validᴹ Σ M -> validᴹ Σ (newᴹ c M)
new-validᴹ c ∙ vᶜ ()
new-validᴹ c [] vᶜ vᴹ = vᶜ , tt
new-validᴹ c (x ∷ M) vᶜ (proj₃ , proj₄) = proj₃ , new-validᴹ c M vᶜ proj₄

-- Writing a valid cell in a valid memory results in a valid memory
write-validᴹ : ∀ {n l τ Σ} {c : Cell τ} {M₁ M₂ : Memory l}
               -> validᶜ Σ c -> validᴹ Σ M₁ -> M₂ ≔ M₁ [ n ↦ c ]ᴹ -> validᴹ Σ M₂
write-validᴹ vᶜ vᴹ  here = vᶜ , proj₂ vᴹ
write-validᴹ vᶜ (proj₃ , proj₄) (there w) = proj₃ , write-validᴹ vᶜ proj₄ w

-- Reading from a valid memory returns a valid cell
read-validᴹ : ∀ {l n τ} {M : Memory l} {c : Cell τ} {Σ : Store} -> n ↦ c ∈ᴹ M -> validᴹ Σ M -> validᶜ Σ c
read-validᴹ here (proj₃ , proj₄) = proj₃
read-validᴹ (there r) (proj₃ , proj₄) = read-validᴹ r proj₄

-- lengthᴹ M is a valid address for a new allocated cell
newᴹ-validAddr : ∀ {l τ} {M : Memory l} {Σ : Store} (c : Cell τ) -> validᴹ Σ M -> ValidAddr τ (newᴹ c M) (lengthᴹ M)
newᴹ-validAddr {M = []} c v = here
newᴹ-validAddr {M = _ ∷ M} c (_ , v) = there (newᴹ-validAddr {M = M} c v)
newᴹ-validAddr {M = ∙} c ()

--------------------------------------------------------------------------------

-- Writing a valid cell in a valid store results in a valid store
write-validˢ : ∀ {l Σ n τ } {M' : Memory l} {c : Cell τ} ->
               let M = Σ l
                   Σ' = Σ [ l ↦ M' ]ˢ in
                   M' ≔ M [ n ↦ c ]ᴹ -> validᶜ Σ c -> validˢ Σ -> validˢ Σ'
write-validˢ {l} {Σ} {M' = M'} w vᶜ vˢ l' with l ≟ l'
... | yes refl = validᴹ-⊆ M' (write-validᴹ vᶜ (vˢ l) w) (updateˢ-⊆ˢ Σ l (writeᴹ-⊆ᴹ w))
... | no ¬p = validᴹ-⊆ (Σ l') (vˢ l') (updateˢ-⊆ˢ Σ _ (writeᴹ-⊆ᴹ w))

-- Allocating a valid cell on a memory in a store leads to a valid store
newᴹ-validˢ : ∀ {l Σ τ } (c : Cell τ) ->
               let M = Σ l
                   Σ' = Σ [ l ↦ newᴹ c M ]ˢ in
                   validᶜ Σ c -> validˢ Σ -> validˢ Σ'
newᴹ-validˢ {l} {Σ} c vᶜ vˢ l' with l ≟ l'
... | yes refl = validᴹ-⊆ (newᴹ c (Σ l)) (new-validᴹ c (Σ l') vᶜ (vˢ l')) (newᴹ-⊆ˢ Σ l' c)
... | no ¬p = validᴹ-⊆ (Σ l') (vˢ l') (newᴹ-⊆ˢ Σ l c)

-- Uninteresting lemma needed to convince Agda.
new-aux : ∀ {l τ} (Σ : Store) (c : Cell τ) ->
          let M = Σ l
              M' = newᴹ c M in
              M' ⊆ᴹ ((Σ [ l ↦ M' ]ˢ) l)
new-aux {l} Σ c with l ≟ l
... | yes refl = refl-⊆ᴹ
... | no ¬p = ⊥-elim (¬p refl)


-- The impure semantics preserves validity
valid⟼ : ∀ {τ} {p₁ p₂ : Program τ} -> validᴾ p₁ -> p₁ ⟼ p₂ -> validᴾ p₂
valid⟼ (vˢ , vᵗ) (Lift x) = vˢ , (valid⇝ vᵗ x)
valid⟼ vᴾ (Label' l⊑h) = (proj₁ vᴾ) , (proj₂ vᴾ)
valid⟼ vᴾ (Unlabel₁ l⊑h step) = valid⟼ vᴾ step
valid⟼ vᴾ (Unlabel₂ l⊑h) = vᴾ
valid⟼ vᴾ (Unlabelχ l⊑h) = ⊥-elim (proj₂ vᴾ)
valid⟼ (vˢ , vᵗ , proj₅) (Bind₁ step) with valid⟼ (vˢ , vᵗ) step
valid⟼ (vˢ , vᵗ , proj₅) (Bind₁ {t₂ = t₂} step) | proj₆ , proj₇ = proj₆ , (proj₇ , valid-⊆ t₂ proj₅ (step-⊆ˢ (vˢ , vᵗ) step))
valid⟼ vᴾ Bind₂ = (proj₁ vᴾ) , ((proj₂ (proj₂ vᴾ)) , (proj₁ (proj₂ vᴾ)))
valid⟼ vᴾ Bind₃ = (proj₁ vᴾ) , (proj₁ (proj₂ vᴾ))
valid⟼ vᴾ (Join l⊑h x) = ⊥-elim (proj₂ vᴾ)
valid⟼ vᴾ (Joinχ l⊑h x) = ⊥-elim (proj₂ vᴾ)
valid⟼ vᴾ (Join∙ l⊑h) = ⊥-elim (proj₂ vᴾ)
valid⟼ (vˢ , vᵗ , proj₅) (Catch₁ step) with valid⟼ (vˢ , vᵗ) step
valid⟼ (vˢ , vᵗ , proj₅) (Catch₁ {t₂ = t₂} step) | proj₆ , proj₇ = proj₆ , proj₇ , valid-⊆ t₂ proj₅ (step-⊆ˢ (vˢ , vᵗ) step)
valid⟼ (vˢ , vᵗ) Catch₂ = vˢ , proj₁ vᵗ
valid⟼ (vˢ , vᵗ) Catch₃ = vˢ , ((proj₂ vᵗ) , (proj₁ vᵗ))
valid⟼ (vˢ , vᵗ) (New {Σ = Σ} {h = h} {t = t} l⊑h)
  = newᴹ-validˢ ⟦ t ⟧ vᵗ vˢ , (is#[ lengthᴹ (Σ h) ] , addr-⊆ (new-aux Σ ⟦ _ ⟧) (newᴹ-validAddr ⟦ t ⟧ (vˢ h)))
valid⟼ (vˢ , vᵗ) (Write₁ l⊑h (Lift x)) = vˢ , (proj₁ vᵗ) , valid⇝ (proj₂ vᵗ) x
valid⟼ (vˢ , vᵗ) (Write₂ l⊑h w) = write-validˢ w (proj₁ vᵗ) vˢ , tt
valid⟼ (vˢ , vᵗ) (Read₁ l⊑h (Lift x)) = vˢ , (valid⇝ vᵗ x)
valid⟼ (vˢ , vᵗ) (Read₂ {l = l} l⊑h r) = vˢ , read-validᴹ r (vˢ l)
valid⟼ vᴾ (New∙ l⊑h) = ⊥-elim (proj₂ vᴾ)
valid⟼ vᴾ (Write∙₁ l⊑h step) = ⊥-elim (proj₂ vᴾ)
valid⟼ vᴾ (Write∙₂ l⊑h) = ⊥-elim (proj₂ vᴾ)
valid⟼ vᴾ (Fork l⊑h) = (proj₁ vᴾ) , tt
valid⟼ vᴾ (Fork∙ l⊑h) =  ⊥-elim (proj₂ vᴾ)
valid⟼ (vˢ , vᵗ) (NewMVar {Σ = Σ} {h = h} l⊑h)
  = newᴹ-validˢ ⊗ tt vˢ , (is#[ lengthᴹ (Σ h) ] , addr-⊆ (new-aux Σ ⊗) (newᴹ-validAddr ⊗ (vˢ h)))
valid⟼ (vˢ , vᵗ) (Take₁ (Lift x)) = vˢ , (valid⇝ vᵗ x)
valid⟼ (vˢ , vᵗ) (Take₂ {Σ} {l = l} r w)
  =  write-validˢ w tt vˢ , read-validᴹ r (validᴹ-⊆ (Σ l) (vˢ l) (updateˢ-⊆ˢ Σ l (writeᴹ-⊆ᴹ w)))
valid⟼ (vˢ , vᵗ) (Put₁ (Lift x)) = vˢ , ((proj₁ vᵗ) , (valid⇝ (proj₂ vᵗ) x))
valid⟼ vᴾ (Put₂ r w) = write-validˢ w (proj₁ (proj₂ vᴾ)) (proj₁ vᴾ) , tt
valid⟼ vᴾ (NewMVar∙ l⊑h) = ⊥-elim (proj₂ vᴾ)

--------------------------------------------------------------------------------

open import Data.Sum
open import Sequential.Determinism 𝓛

-- Each valid program can be in either three statuses, Doneᴾ, Redexᴾ or Stuckᴾ.
-- This function is a constructive proof that we can decide which status is that.
-- This proof is rather boring and repetitive. It would be better to use proof automation.
stateᴾ : ∀ {τ} (p : Program τ) {{v : validᴾ p}} -> Stateᴾ p
stateᴾ ⟨ Σ , var () ⟩
stateᴾ ⟨ Σ , Λ t ⟩ = inj₂ (inj₁ (Λ t))
stateᴾ ⟨ Σ , t ∘ t₁ ⟩ {{v}} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , (proj₁ (proj₂ v)) }}
stateᴾ ⟨ Σ , t ∘ t₁ ⟩ | inj₁ (Step (Lift s)) = inj₁ (Step (Lift (App s)))
stateᴾ ⟨ Σ , .(Λ t) ∘ t₁ ⟩ | inj₂ (inj₁ (Λ t)) = inj₁ (Step (Lift Beta))
stateᴾ ⟨ Σ , t ∘ t₁ ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift (App x))) → ¬r (Step (Lift x)) ; (Step (Lift Beta)) → ¬v (Λ _)}))
stateᴾ ⟨ Σ , （） ⟩ = inj₂ (inj₁ （）)
stateᴾ ⟨ Σ , True ⟩ = inj₂ (inj₁ True)
stateᴾ ⟨ Σ , False ⟩ = inj₂ (inj₁ False)
stateᴾ ⟨ Σ , if t then t₁ else t₂ ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , (proj₁ (proj₂ v)) }}
stateᴾ ⟨ Σ , if t then t₁ else t₂ ⟩ | inj₁ (Step (Lift s)) = inj₁ (Step (Lift (If₁ s)))
stateᴾ ⟨ Σ , if .True then t₁ else t₂ ⟩ | inj₂ (inj₁ True) = inj₁ (Step (Lift If₂))
stateᴾ ⟨ Σ , if .False then t₁ else t₂ ⟩ | inj₂ (inj₁ False) = inj₁ (Step (Lift If₃))
stateᴾ ⟨ Σ , if t then t₁ else t₂ ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift (If₁ s))) → ¬r (Step (Lift s)) ;
                               (Step (Lift If₂)) → ¬v True ;
                               (Step (Lift If₃)) → ¬v False} ))
stateᴾ ⟨ Σ , return l t ⟩ = inj₂ (inj₁ (return t))
stateᴾ ⟨ Σ , t >>= t₁ ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , (proj₁ (proj₂ v)) }}
stateᴾ ⟨ Σ , t >>= t₁ ⟩ | inj₁ (Step x) = inj₁ (Step (Bind₁ x))
stateᴾ ⟨ Σ , .(return _ t) >>= t₁ ⟩ | inj₂ (inj₁ (return t)) = inj₁ (Step Bind₂)
stateᴾ ⟨ Σ , .(throw _ t) >>= t₁ ⟩ | inj₂ (inj₁ (throw t)) = inj₁ (Step Bind₃)
stateᴾ ⟨ Σ , t >>= t₁ ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) ;
                               (Step (Bind₁ x)) → ¬r (Step x) ;
                               (Step Bind₂) → ¬v (return _) ;
                               (Step Bind₃) → ¬v (throw _) } ))
stateᴾ ⟨ Σ , ξ ⟩ = inj₂ (inj₁ ξ)
stateᴾ ⟨ Σ , throw l t ⟩ = inj₂ (inj₁ (throw t))
stateᴾ ⟨ Σ , catch t t₁ ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , (proj₁ (proj₂ v)) }}
stateᴾ ⟨ Σ , catch t t₁ ⟩ | inj₁ (Step x) = inj₁ (Step (Catch₁ x))
stateᴾ ⟨ Σ , catch .(return _ t) t₁ ⟩ | inj₂ (inj₁ (return t)) = inj₁ (Step Catch₂)
stateᴾ ⟨ Σ , catch .(throw _ t) t₁ ⟩ | inj₂ (inj₁ (throw t)) = inj₁ (Step Catch₃)
stateᴾ ⟨ Σ , catch t t₁ ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) ;
                               (Step (Catch₁ x)) → ¬r (Step x) ;
                               (Step Catch₂) → ¬v (return _) ;
                               (Step Catch₃) → ¬v (throw _) }))
stateᴾ ⟨ Σ , Labeled l t ⟩ = inj₂ (inj₁ (Labeled t))
stateᴾ ⟨ Σ , Labeledχ l t ⟩ = inj₂ (inj₁ (Labeledχ t))
stateᴾ ⟨ Σ , label l⊑h t ⟩ = inj₁ (Step (Label' l⊑h))
stateᴾ ⟨ Σ , unlabel l⊑h t ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , proj₂ v }}
stateᴾ ⟨ Σ , unlabel l⊑h t ⟩ | inj₁ (Step x) = inj₁ (Step (Unlabel₁ l⊑h x))
stateᴾ ⟨ Σ , unlabel l⊑h .(Labeled _ t) ⟩ | inj₂ (inj₁ (Labeled t)) = inj₁ (Step (Unlabel₂ l⊑h))
stateᴾ ⟨ Σ , unlabel l⊑h .(Labeledχ _ t) ⟩ | inj₂ (inj₁ (Labeledχ t)) = inj₁ (Step (Unlabelχ l⊑h))
stateᴾ ⟨ Σ , unlabel l⊑h t ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) ;
                               (Step (Unlabel₁ _ x)) → ¬r (Step x) ;
                               (Step (Unlabel₂ _)) → ¬v (Labeled _) ;
                               (Step (Unlabelχ _)) → ¬v (Labeledχ _) }))
stateᴾ ⟨ Σ , join l⊑h t ⟩ {{ v }}= ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , join∙ l⊑h t ⟩ {{ v }} = ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , fork l⊑h t ⟩ = inj₁ (Step (Fork l⊑h))
stateᴾ ⟨ Σ , fork∙ l⊑h t ⟩ = inj₁ (Step (Fork∙ l⊑h))
stateᴾ ⟨ Σ , #[ n ] ⟩ = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) }))
stateᴾ ⟨ Σ , Ref l t ⟩ = inj₂ (inj₁ (Ref t))
stateᴾ ⟨ Σ , read l⊑h t ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , (proj₂ v) }}
stateᴾ ⟨ Σ , read l⊑h t ⟩ | inj₁ (Step x) = inj₁ (Step (Read₁ l⊑h x))
stateᴾ ⟨ Σ , read l⊑h .(Ref _ #[ n ]) ⟩ {{v₁ , is#[ n ] , v₃}} | inj₂ (inj₁ (Ref .(#[ n ]))) with readᴹ-∈ v₃
stateᴾ ⟨ Σ , read l⊑h .(Ref _ #[ n ]) ⟩ {{v₁ , is#[ n ] , v₃}} | inj₂ (inj₁ (Ref .(#[ n ]))) | ⊗ , r
  = inj₂ (inj₂ ((λ ()) , λ { (Step (Lift ())) ;
                             (Step (Read₁ _ (Lift ()))) ;
                             (Step (Read₂ _ r')) → ⊥-elim (⊗≢⟦⟧ (det-readᴹ r r'))}))
stateᴾ ⟨ Σ , read l⊑h .(Ref _ #[ n ]) ⟩ {{v₁ , is#[ n ] , v₃}} | inj₂ (inj₁ (Ref .(#[ n ]))) | ⟦ x ⟧ , r = inj₁ (Step (Read₂ l⊑h r))
stateᴾ ⟨ Σ , read l⊑h t ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) ;
                               (Step (Read₁ _ x)) → ¬r (Step x) ;
                               (Step (Read₂ _ r)) → ¬v (Ref #[ _ ])}))
stateᴾ ⟨ Σ , write l⊑h t t₁ ⟩ {{ v }} with stateᴾ ⟨ Σ , t₁ ⟩ {{ (proj₁ v) , (proj₂ (proj₂ v)) }}
stateᴾ ⟨ Σ , write l⊑h t t₁ ⟩ | inj₁ (Step x) = inj₁ (Step (Write₁ l⊑h x))
stateᴾ ⟨ Σ , write l⊑h t .(Ref _ #[ n ]) ⟩ {{v₁ , v₂ , is#[ n ] , v₄}} | inj₂ (inj₁ (Ref .(#[ n ]))) with readᴹ-∈ v₄
stateᴾ ⟨ Σ , write l⊑h t .(Ref _ #[ n ]) ⟩ {{v₁ , v₂ , is#[ n ] , v₄}} | inj₂ (inj₁ (Ref .(#[ n ]))) | _ , r
  = inj₁ (Step (Write₂ l⊑h (proj₂ (writeᴹ↦ r))))
stateᴾ ⟨ Σ , write l⊑h t t₁ ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) ;
                               (Step (Write₁ _ x)) → ¬r (Step x) ;
                               (Step (Write₂ _ w)) → ¬v (Ref #[ _ ])} ))
stateᴾ ⟨ Σ , write∙ l⊑h t t₁ ⟩ {{ v }} = ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , new l⊑h t ⟩ = inj₁ (Step (New l⊑h))
stateᴾ ⟨ Σ , new∙ l⊑h t ⟩ {{ v }} = ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , MVar l t ⟩ = inj₂ (inj₁ (MVar t))
stateᴾ ⟨ Σ , newMVar l⊑h ⟩ = inj₁ (Step (NewMVar l⊑h))
stateᴾ ⟨ Σ , newMVar∙ l⊑h ⟩ {{ v }} = ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , take t ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , (proj₂ v) }}
stateᴾ ⟨ Σ , take t ⟩ | inj₁ (Step x) = inj₁ (Step (Take₁ x))
stateᴾ ⟨ Σ , take .(MVar _ #[ n ]) ⟩ {{_ , is#[ n ] , v₂}} | inj₂ (inj₁ (MVar .(#[ n ]))) with readᴹ-∈ v₂
stateᴾ ⟨ Σ , take .(MVar _ #[ n ]) ⟩ {{_ , is#[ n ] , v₂}} | inj₂ (inj₁ (MVar .(#[ n ]))) | ⊗ , r'
  = inj₂ (inj₂ ((λ ()) , λ { (Step (Lift ())) ;
                             (Step (Take₁ (Lift ()))) ;
                             (Step (Take₂ r w)) → ⊥-elim (⊗≢⟦⟧ (det-readᴹ r' r))}))
stateᴾ ⟨ Σ , take .(MVar _ #[ n ]) ⟩ {{_ , is#[ n ] , v₂}} | inj₂ (inj₁ (MVar .(#[ n ]))) | ⟦ x ⟧ , r
  = inj₁ (Step (Take₂ r (proj₂ (writeᴹ↦ r))))
stateᴾ ⟨ Σ , take t ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) ;
                               (Step (Take₁ x)) → ¬r (Step x) ;
                               (Step (Take₂ r w)) → ¬v (MVar #[ _ ])}))
stateᴾ ⟨ Σ , put t t₁ ⟩ {{ v }} with stateᴾ ⟨ Σ , t₁ ⟩ {{ (proj₁ v) , (proj₂ (proj₂ v)) }}
stateᴾ ⟨ Σ , put t t₁ ⟩ | inj₁ (Step x) = inj₁ (Step (Put₁ x))
stateᴾ ⟨ Σ , put t .(MVar _ #[ n ]) ⟩ {{v₁ , v₂ , is#[ n ] , v₄}} | inj₂ (inj₁ (MVar .(#[ n ]))) with readᴹ-∈ v₄
stateᴾ ⟨ Σ , put t .(MVar _ #[ n ]) ⟩ {{v₁ , v₂ , is#[ n ] , v₄}} | inj₂ (inj₁ (MVar .(#[ n ]))) | ⊗ , r
  = inj₁ (Step (Put₂ r (proj₂ (writeᴹ↦ r))))
stateᴾ ⟨ Σ , put t .(MVar _ #[ n ]) ⟩ {{v₁ , v₂ , is#[ n ] , v₄}} | inj₂ (inj₁ (MVar .(#[ n ]))) | ⟦ x ⟧ , r'
  = inj₂ (inj₂ ((λ ()) , λ { (Step (Lift ())) ;
                             (Step (Put₁ (Lift ()))) ;
                             (Step (Put₂ r w)) → ⊥-elim (⊗≢⟦⟧ (det-readᴹ r r'))}))
stateᴾ ⟨ Σ , put t t₁ ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift ())) ;
                               (Step (Put₁ x)) → ¬r (Step x) ;
                               (Step (Put₂ r w)) → ¬v (MVar #[ _ ])}))
stateᴾ ⟨ Σ , t ⟨$⟩ t₁ ⟩ = inj₁ (Step (Lift Fmap))
stateᴾ ⟨ Σ , t ⟨$⟩∙ t₁ ⟩ {{ v }} = ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , t ⟨✴⟩ t₁ ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , (proj₁ (proj₂ v)) }}
stateᴾ ⟨ Σ , t ⟨✴⟩ t₁ ⟩ | inj₁ (Step (Lift x)) = inj₁ (Step (Lift (⟨✴⟩₁ x)))
stateᴾ ⟨ Σ , t ⟨✴⟩ t₁ ⟩ {{ v }} | inj₂ (inj₁ x) with stateᴾ ⟨ Σ , t₁ ⟩ {{ (proj₁ v) , (proj₂ (proj₂ v)) }}
stateᴾ ⟨ Σ , .(Labeled _ t) ⟨✴⟩ t₁ ⟩ | inj₂ (inj₁ (Labeled t)) | inj₁ (Step (Lift x)) = inj₁ (Step (Lift (⟨✴⟩₂ x)))
stateᴾ ⟨ Σ , .(Labeledχ _ t) ⟨✴⟩ t₁ ⟩ | inj₂ (inj₁ (Labeledχ t)) | inj₁ (Step (Lift x)) = inj₁ (Step (Lift (⟨✴⟩₁χ x)))
stateᴾ ⟨ Σ , .(Labeled _ t₁) ⟨✴⟩ .(Labeled _ t₂) ⟩ | inj₂ (inj₁ (Labeled t₁)) | inj₂ (inj₁ (Labeled t₂)) = inj₁ (Step (Lift ⟨✴⟩₃))
stateᴾ ⟨ Σ , .(Labeled _ t₁) ⟨✴⟩ .(Labeledχ _ t₂) ⟩ | inj₂ (inj₁ (Labeled t₁)) | inj₂ (inj₁ (Labeledχ t₂)) = inj₁ (Step (Lift ⟨✴⟩₃χ))
stateᴾ ⟨ Σ , .(Labeledχ _ t₁) ⟨✴⟩ .(Labeled _ t₂) ⟩ | inj₂ (inj₁ (Labeledχ t₁)) | inj₂ (inj₁ (Labeled t₂)) = inj₁ (Step (Lift ⟨✴⟩₂χ))
stateᴾ ⟨ Σ , .(Labeledχ _ t₁) ⟨✴⟩ .(Labeledχ _ t₂) ⟩ | inj₂ (inj₁ (Labeledχ t₁)) | inj₂ (inj₁ (Labeledχ t₂)) = inj₁ (Step (Lift ⟨✴⟩₄χ))
stateᴾ ⟨ Σ , .(Labeled _ t) ⟨✴⟩ t₁ ⟩ | inj₂ (inj₁ (Labeled t)) | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift (⟨✴⟩₁ ()))) ;
                               (Step (Lift (⟨✴⟩₂ x₁))) → ¬r (Step (Lift x₁)) ;
                               (Step (Lift ⟨✴⟩₃)) → ¬v (Labeled _) ;
                               (Step (Lift ⟨✴⟩₃χ)) → ¬v (Labeledχ _) }))
stateᴾ ⟨ Σ , .(Labeledχ _ t) ⟨✴⟩ t₁ ⟩ | inj₂ (inj₁ (Labeledχ t)) | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift (⟨✴⟩₁ ()))) ;
                               (Step (Lift (⟨✴⟩₁χ x))) → ¬r (Step (Lift x)) ;
                               (Step (Lift ⟨✴⟩₂χ)) → ¬v (Labeled _) ;
                               (Step (Lift ⟨✴⟩₄χ)) → ¬v (Labeledχ _) }))
stateᴾ ⟨ Σ , t ⟨✴⟩ t₁ ⟩ | inj₂ (inj₂ (¬v P., ¬r)) =
  inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift (⟨✴⟩₁ x))) → ¬r (Step (Lift x)) ;
                             (Step (Lift (⟨✴⟩₂ x))) → ¬v (Labeled _) ;
                             (Step (Lift ⟨✴⟩₃)) → ¬v (Labeled _) ;
                             (Step (Lift (⟨✴⟩₁χ x))) → ¬v (Labeledχ _) ;
                             (Step (Lift ⟨✴⟩₂χ)) → ¬v (Labeledχ _) ;
                             (Step (Lift ⟨✴⟩₃χ)) → ¬v (Labeled _) ;
                             (Step (Lift ⟨✴⟩₄χ)) → ¬v (Labeledχ _)}))
stateᴾ ⟨ Σ , t ⟨✴⟩∙ t₁ ⟩ {{ v }} = ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , relabel l⊑h t ⟩ {{ v }} with stateᴾ ⟨ Σ , t ⟩ {{ (proj₁ v) , proj₂ v }}
stateᴾ ⟨ Σ , relabel l⊑h t ⟩ | inj₁ (Step (Lift x)) = inj₁ (Step (Lift (Relabel₁ l⊑h x)))
stateᴾ ⟨ Σ , relabel l⊑h .(Labeled _ t) ⟩ | inj₂ (inj₁ (Labeled t)) = inj₁ (Step (Lift (Relabel₂ l⊑h)))
stateᴾ ⟨ Σ , relabel l⊑h .(Labeledχ _ t) ⟩ | inj₂ (inj₁ (Labeledχ t)) = inj₁ (Step (Lift (Relabelχ l⊑h)))
stateᴾ ⟨ Σ , relabel l⊑h t ⟩ | inj₂ (inj₂ (¬v P., ¬r))
  = inj₂ (inj₂ ((λ ()) P., λ { (Step (Lift (Relabel₁ _ x))) → ¬r (Step (Lift x)) ;
                               (Step (Lift (Relabel₂ _))) → ¬v (Labeled _) ;
                               (Step (Lift (Relabelχ _))) → ¬v (Labeledχ _)}))
stateᴾ ⟨ Σ , relabel∙ l⊑h t ⟩ {{ v }} = ⊥-elim (proj₂ v)
stateᴾ ⟨ Σ , ∙ ⟩ {{ v }} = ⊥-elim (proj₂ v)
