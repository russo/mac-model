-- This module proves that the sequential calculus is deterministic.

import Lattice as L

module Sequential.Determinism (𝓛 : L.Lattice) where

open import Types 𝓛
open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛

open import Data.Product
open import Relation.Nullary
open import Relation.Binary.PropositionalEquality
open import Data.Empty

-- The pure semantics relation (⇝) is deterministic.
determinism : ∀ {τ} {t₁ t₂ t₃ : CTerm τ } -> t₁ ⇝ t₂ -> t₁ ⇝ t₃ -> t₂ ≡ t₃
determinism (App step₁) (App step₂)
  rewrite determinism step₁ step₂ = refl
determinism (App ()) Beta
determinism Beta (App ())
determinism Beta Beta = refl
determinism (If₁ step₁) (If₁ step₂)
  rewrite determinism step₁ step₂ = refl
determinism (If₁ ()) If₂
determinism (If₁ ()) If₃
determinism If₂ (If₁ ())
determinism If₂ If₂ = refl
determinism If₃ (If₁ ())
determinism If₃ If₃ = refl
determinism Fmap Fmap = refl
determinism (⟨✴⟩₁ step₁) (⟨✴⟩₁ step₂)
  rewrite determinism step₁ step₂ = refl
determinism (⟨✴⟩₁ ()) (⟨✴⟩₂ step₂)
determinism (⟨✴⟩₁ ()) ⟨✴⟩₃
determinism (⟨✴⟩₁ ()) (⟨✴⟩₁χ step₂)
determinism (⟨✴⟩₁ ()) ⟨✴⟩₃χ
determinism (⟨✴⟩₁ ()) ⟨✴⟩₄χ
determinism (⟨✴⟩₂ step₁) (⟨✴⟩₁ ())
determinism (⟨✴⟩₂ step₁) (⟨✴⟩₂ step₂)
  rewrite determinism step₁ step₂ = refl
determinism (⟨✴⟩₂ ()) ⟨✴⟩₃
determinism ⟨✴⟩₃ (⟨✴⟩₁ ())
determinism ⟨✴⟩₃ (⟨✴⟩₂ ())
determinism ⟨✴⟩₃ ⟨✴⟩₃ = refl
determinism (Relabel₁ l⊑h step₁) (Relabel₁ .l⊑h step₂)
  rewrite determinism step₁ step₂ = refl
determinism (Relabel₁ l⊑h ()) (Relabel₂ .l⊑h)
determinism (Relabel₁ l⊑h ()) (Relabelχ .l⊑h)
determinism (Relabel₂ l⊑h) (Relabel₁ .l⊑h ())
determinism (Relabel₂ l⊑h) (Relabel₂ .l⊑h) = refl
determinism Fmap∙ Fmap∙ = refl
determinism (⟨✴⟩∙₁ step₁) (⟨✴⟩∙₁ step₂)
  rewrite determinism step₁ step₂ = refl
determinism (⟨✴⟩∙₁ ()) (⟨✴⟩∙₂ step₂)
determinism (⟨✴⟩∙₁ ()) ⟨✴⟩∙₃
determinism (⟨✴⟩∙₂ step₁) (⟨✴⟩∙₁ ())
determinism (⟨✴⟩∙₂ step₁) (⟨✴⟩∙₂ step₂)
  rewrite determinism step₁ step₂ = refl
determinism (⟨✴⟩∙₂ ()) ⟨✴⟩∙₃
determinism ⟨✴⟩∙₃ (⟨✴⟩∙₁ ())
determinism ⟨✴⟩∙₃ (⟨✴⟩∙₂ ())
determinism ⟨✴⟩∙₃ ⟨✴⟩∙₃ = refl
determinism (Relabel∙₁ l⊑h step₁) (Relabel∙₁ .l⊑h step₂)
  rewrite determinism step₁ step₂ = refl
determinism (Relabel∙₁ l⊑h ()) (Relabel∙₂ .l⊑h)
determinism (Relabel∙₁ l⊑h ()) (Relabel∙χ .l⊑h)
determinism (Relabel∙₂ l⊑h) (Relabel∙₁ .l⊑h ())
determinism (Relabel∙₂ l⊑h) (Relabel∙₂ .l⊑h) = refl
determinism (⟨✴⟩₁χ step₁) (⟨✴⟩₁ ())
determinism (⟨✴⟩₁χ step₁) (⟨✴⟩₁χ step₂)
  rewrite determinism step₁ step₂ = refl
determinism (⟨✴⟩₁χ ()) ⟨✴⟩₄χ
determinism ⟨✴⟩₃χ (⟨✴⟩₁ ())
determinism ⟨✴⟩₃χ ⟨✴⟩₃χ = refl
determinism ⟨✴⟩₄χ (⟨✴⟩₁ ())
determinism ⟨✴⟩₄χ (⟨✴⟩₁χ ())
determinism ⟨✴⟩₄χ ⟨✴⟩₄χ = refl
determinism ⟨✴⟩₂χ (⟨✴⟩₁ ())
determinism ⟨✴⟩₃χ (⟨✴⟩₂ ())
determinism ⟨✴⟩₂χ (⟨✴⟩₁χ ())
determinism (⟨✴⟩₁ ()) ⟨✴⟩₂χ
determinism (⟨✴⟩₁χ ()) ⟨✴⟩₂χ
determinism ⟨✴⟩₂χ ⟨✴⟩₂χ = refl
determinism (⟨✴⟩₂ ()) ⟨✴⟩₃χ
determinism (Relabelχ l⊑h) (Relabel₁ .l⊑h ())
determinism (Relabelχ l⊑h) (Relabelχ .l⊑h) = refl
determinism (Relabel∙χ l⊑h) (Relabel∙₁ .l⊑h ())
determinism (Relabel∙χ l⊑h) (Relabel∙χ .l⊑h) = refl
determinism Hole Hole = refl

-- A term either makes either a pure or an impure step
impure-vs-pure : ∀ {τ} {Σ₁ Σ₂} {t₁ t₂ t₃ : CTerm τ} ->  ⟨ Σ₁ , t₁ ⟩ ⟼ ⟨ Σ₂ , t₂ ⟩ -> (t₁ ⇝ t₃) -> (Σ₁ ≡ Σ₂) × (t₂ ≡ t₃)
impure-vs-pure (Lift step₁) step₂ = refl , determinism step₁ step₂
impure-vs-pure (Label' l⊑h) ()
impure-vs-pure (Unlabel₁ l⊑h istep) ()
impure-vs-pure (Unlabel₂ l⊑h) ()
impure-vs-pure (Unlabelχ l⊑h) ()
impure-vs-pure (Bind₁ istep) ()
impure-vs-pure Bind₂ ()
impure-vs-pure Bind₃ ()
impure-vs-pure (Join l⊑h x) ()
impure-vs-pure (Joinχ l⊑h x) ()
impure-vs-pure (Join∙ l⊑h) ()
impure-vs-pure (Catch₁ istep) ()
impure-vs-pure Catch₂ ()
impure-vs-pure Catch₃ ()
impure-vs-pure (New l⊑h) ()
impure-vs-pure (Write₁ l⊑h istep) ()
impure-vs-pure (Write₂ l⊑h x) ()
impure-vs-pure (Read₁ l⊑h istep) ()
impure-vs-pure (Read₂ l⊑h x) ()
impure-vs-pure (New∙ l⊑h) ()
impure-vs-pure (Write∙₁ l⊑h istep) ()
impure-vs-pure (Write∙₂ l⊑h) ()
impure-vs-pure (Fork _) ()
impure-vs-pure (Fork∙ _) ()
impure-vs-pure (NewMVar l⊑h) ()
impure-vs-pure (Take₁ istep) ()
impure-vs-pure (Take₂ r w) ()
impure-vs-pure (Put₁ istep) ()
impure-vs-pure (Put₂ r w) ()
impure-vs-pure (NewMVar∙ l⊑h) ()


-- Values do not reduce further.
valueNotStep : ∀ {τ} {Σ₁ Σ₂} {t₁ t₂ : CTerm τ}
               -> ⟨ Σ₁ , t₁ ⟩ ⟼ ⟨ Σ₂ , t₂ ⟩
               -> ¬ (Value t₁)
valueNotStep (Lift ()) （）
valueNotStep (Lift ()) True
valueNotStep (Lift ()) False
valueNotStep (Lift ()) (Λ t)
valueNotStep (Lift ()) ξ
valueNotStep (Lift ()) (return t)
valueNotStep (Lift ()) (throw t)
valueNotStep (Lift ()) (Labeled t)
valueNotStep (Lift ()) (Labeledχ t)
valueNotStep (Lift ()) (Ref t)
valueNotStep (Lift ()) (MVar t)

-- The impure operational semantics relation (⟼) is deterministic.
determinismᶜ : ∀ {τ} {Σ₁ Σ₂ Σ₃} {t₁ t₂ t₃ : CTerm τ}
               -> ⟨ Σ₁ , t₁ ⟩ ⟼ ⟨ Σ₂ , t₂ ⟩
               -> ⟨ Σ₁ , t₁ ⟩ ⟼ ⟨ Σ₃ , t₃ ⟩
               -> ⟨ Σ₂ , t₂ ⟩ ≡ ⟨ Σ₃ , t₃ ⟩

-- The transitive closure of the impure operational semantics (⟼⋆)
-- relation is deterministic.
determinismᶜ⋆ : ∀ {τ} {Σ₁ Σ₂ Σ₃} {t₁ t₂ t₃ : CTerm τ}
               -> ⟨ Σ₁ , t₁ ⟩ ⟼⋆ ⟨ Σ₂ , t₂ ⟩
               -> ⟨ Σ₁ , t₁ ⟩ ⟼⋆ ⟨ Σ₃ , t₃ ⟩
               -> Value t₂ -> Value t₃ -> ⟨ Σ₂ , t₂ ⟩ ≡ ⟨ Σ₃ , t₃ ⟩
determinismᶜ⋆ [] [] v₁ v₂ = refl
determinismᶜ⋆ [] (s ∷ ss₂) v₁ v₂ = ⊥-elim (valueNotStep s v₁)
determinismᶜ⋆ (s ∷ ss₁) [] v₁ v₂ = ⊥-elim (valueNotStep s v₂)
determinismᶜ⋆ (s ∷ ss₁) (s₁ ∷ ss₂) v₁ v₂
  with  determinismᶜ s s₁
... | refl with determinismᶜ⋆ ss₁ ss₂ v₁ v₂
... | refl = refl

-- The write proofs are functional relation, i.e., the resulting
-- memory is uniquely determined by the inputs indexes.
det-writeᴹ : ∀ {l n τ} {c : Cell τ} {M₁ M₂ M₃ : Memory l}
             -> M₂ ≔ M₁ [ n ↦ c ]ᴹ
             -> M₃ ≔ M₁ [ n ↦ c ]ᴹ
             -> M₂ ≡ M₃
det-writeᴹ here here = refl
det-writeᴹ (there w₁) (there w₂)
  rewrite det-writeᴹ w₁ w₂ = refl

-- The read proofs are functional relation, i.e., the term read is
-- uniquely determined by the inputs indexes.
det-readᴹ : ∀ {l n τ} {c₁ c₂ : Cell τ} {M : Memory l}
             -> n ↦ c₁ ∈ᴹ M
             -> n ↦ c₂ ∈ᴹ M
             -> c₁ ≡ c₂
det-readᴹ here here = refl
det-readᴹ (there w₁) (there w₂)
  rewrite det-readᴹ w₁ w₂ = refl


determinismᶜ (Lift step₁) (Lift step₂)
  rewrite determinism step₁ step₂ = refl
determinismᶜ (Label' l⊑h) (Label' .l⊑h) = refl
determinismᶜ (Unlabel₁ l⊑h step₁) (Unlabel₁ .l⊑h step₂)
  with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Unlabel₁ l⊑h (Lift ())) (Unlabel₂ .l⊑h)
determinismᶜ (Unlabel₁ l⊑h (Lift ())) (Unlabelχ .l⊑h)
determinismᶜ (Unlabel₂ l⊑h) (Unlabel₁ .l⊑h (Lift ()))
determinismᶜ (Unlabel₂ l⊑h) (Unlabel₂ .l⊑h) = refl
determinismᶜ (Unlabelχ l⊑h) (Unlabel₁ .l⊑h (Lift ()))
determinismᶜ (Unlabelχ l⊑h) (Unlabelχ .l⊑h) = refl
determinismᶜ (Bind₁ step₁) (Bind₁ step₂)
  with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Bind₁ (Lift ())) Bind₂
determinismᶜ (Bind₁ (Lift ())) Bind₃
determinismᶜ Bind₂ (Bind₁ (Lift ()))
determinismᶜ Bind₂ Bind₂ = refl
determinismᶜ Bind₃ (Bind₁ (Lift ()))
determinismᶜ Bind₃ Bind₃ = refl
determinismᶜ (Join l⊑h ⌞ ss₁ , v₁ ⌟ ) (Join .l⊑h ⌞ ss₂ , v₂ ⌟)
  with determinismᶜ⋆ ss₁ ss₂ v₁ v₂
... | refl = refl
determinismᶜ (Join l⊑h  ⌞ ss₁ , v₁ ⌟) (Joinχ .l⊑h ⌞ ss₂ , v₂ ⌟)
  with determinismᶜ⋆ ss₁ ss₂ v₁ v₂
... | ()
determinismᶜ (Joinχ l⊑h ⌞ ss₁ , v₁ ⌟) (Join .l⊑h ⌞ ss₂ , v₂ ⌟)
  with determinismᶜ⋆ ss₁ ss₂ v₁ v₂
... | ()
determinismᶜ (Joinχ l⊑h ⌞ ss₁ , v₁ ⌟) (Joinχ .l⊑h  ⌞ ss₂ , v₂ ⌟)
  with determinismᶜ⋆ ss₁ ss₂ v₁ v₂
... | refl = refl
determinismᶜ (Join∙ l⊑h) (Join∙ .l⊑h) = refl
determinismᶜ (Catch₁ step₁) (Catch₁ step₂)
  with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Catch₁ (Lift ())) Catch₂
determinismᶜ (Catch₁ (Lift ())) Catch₃
determinismᶜ Catch₂ (Catch₁ (Lift ()))
determinismᶜ Catch₂ Catch₂ = refl
determinismᶜ Catch₃ (Catch₁ (Lift ()))
determinismᶜ Catch₃ Catch₃ = refl
determinismᶜ (New l⊑h) (New .l⊑h) = refl
determinismᶜ (Write₁ l⊑h step₁) (Write₁ .l⊑h step₂)
  with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Write₁ l⊑h (Lift ())) (Write₂ .l⊑h x)
determinismᶜ (Write₂ l⊑h x) (Write₁ .l⊑h (Lift ()))
determinismᶜ (Write₂ l⊑h w₁) (Write₂ .l⊑h w₂)
  rewrite det-writeᴹ w₁ w₂ = refl
determinismᶜ (Read₁ l⊑h step₁) (Read₁ .l⊑h step₂)
  with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Read₁ l⊑h (Lift ())) (Read₂ .l⊑h x)
determinismᶜ (Read₂ l⊑h x) (Read₁ .l⊑h (Lift ()))
determinismᶜ (Read₂ l⊑h r₁) (Read₂ .l⊑h r₂)
  with det-readᴹ r₁ r₂
... | refl = refl
determinismᶜ (New∙ l⊑h) (New∙ .l⊑h) = refl
determinismᶜ (Write∙₁ l⊑h step₁) (Write∙₁ .l⊑h step₂)
  with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Write∙₁ l⊑h (Lift ())) (Write∙₂ .l⊑h)
determinismᶜ (Write∙₂ l⊑h) (Write∙₁ .l⊑h (Lift ()))
determinismᶜ (Write∙₂ l⊑h) (Write∙₂ .l⊑h) = refl
determinismᶜ (Fork _) (Fork _) = refl
determinismᶜ (Fork∙ _) (Fork∙ _) = refl
determinismᶜ (NewMVar l⊑h) (NewMVar .l⊑h) = refl
determinismᶜ (Take₁ step₁) (Take₁ step₂) with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Take₁ (Lift ())) (Take₂ r w)
determinismᶜ (Take₂ r w) (Take₁ (Lift ()))
determinismᶜ (Take₂ r₁ w₁) (Take₂ r₂ w₂)
  with  det-readᴹ r₁ r₂
... | refl rewrite det-writeᴹ w₁ w₂ = refl
determinismᶜ (Put₁ step₁) (Put₁ step₂) with determinismᶜ step₁ step₂
... | refl = refl
determinismᶜ (Put₁ (Lift ())) (Put₂ r w)
determinismᶜ (Put₂ r w) (Put₁ (Lift ()))
determinismᶜ (Put₂ r₁ w₁) (Put₂ r₂ w₂)
  with  det-readᴹ r₁ r₂
... | refl rewrite det-writeᴹ w₁ w₂ = refl
determinismᶜ (NewMVar∙ l⊑h) (NewMVar∙ .l⊑h) = refl
determinismᶜ (Lift step₁) step₂
  with impure-vs-pure step₂ step₁
... | refl , refl = refl
determinismᶜ step₁ (Lift step₂)
  with impure-vs-pure step₁ step₂
... | refl , refl = refl

-- The impure operational semantics relation (⟼) is deterministic.
determinismᴾ : ∀ {τ} {p₁ p₂ p₃ : Program τ} -> p₁ ⟼ p₂ -> p₁ ⟼ p₃ -> p₂ ≡ p₃
determinismᴾ {_} {⟨ Σ₁ , t₁ ⟩} {⟨ Σ₂ , t₂ ⟩} {⟨ Σ₃ , t₃ ⟩} step₁ step₂ = determinismᶜ step₁ step₂
