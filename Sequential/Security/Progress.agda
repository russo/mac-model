-- This module proves 1-step progress for the sequential calculus,
-- i.e., if an erased valid low program steps so does the original
-- program.

import Lattice as L

module Sequential.Security.Progress (𝓛 : L.Lattice) (A : L.Label 𝓛) where

open import Types 𝓛
open import Sequential.Calculus  𝓛
open import Sequential.Security.Erasure 𝓛 A
open import Sequential.Semantics 𝓛
open import Sequential.Valid 𝓛
open import Sequential.Security.Graph 𝓛 A

open import Relation.Nullary
open import Data.Empty
open import Data.Maybe
open import Data.Nat
open import Data.Product

-- 1-step progress for pure semantics: if ε(t) ⇝ ε(t') then t ⇝ t'
-- Note that we use PRedex and existentially quantify on t' to avoid
-- unification issues (there could be many terms that get erased to
-- ε(t')
--
-- This proof is very boring and repeatitive. Proof automation would
-- be very valuable here (lots of cases are void).
progress⇝ : ∀ {Σ τ} {t tᴱ t'ᴱ : CTerm τ} (v : valid Σ t) -> tᴱ ⇝ t'ᴱ -> Erase t tᴱ -> PRedex t
progress⇝ v (App step) (e₁ ∘ e₃) with progress⇝ (proj₁ v) step e₁
... | Step step' = Step (App step')
progress⇝ v Beta (Λ e₁ ∘ e₃) = Step Beta
progress⇝ v (If₁ step) (if e₁ then e₂ else e₃) with progress⇝ (proj₁ v) step e₁
... | Step step' = Step (If₁ step')
progress⇝ v If₂ (if True then e₃ else e₄) = Step If₂
progress⇝ v If₃ (if False then e₃ else e₄) = Step If₃
progress⇝ v Fmap (fmap l⊑A e₁ e₃) = Step Fmap
progress⇝ v (⟨✴⟩₁ step) (app l⊑A e₁ e₃) with progress⇝ (proj₁ v) step e₁
... | Step step' = Step (⟨✴⟩₁ step')
progress⇝ v (⟨✴⟩₂ step) (app l⊑A (Labeled l⊑A₂ e₁) e₃) with progress⇝ (proj₂ v) step e₃
... | Step step' =  Step (⟨✴⟩₂ step')
progress⇝ v (⟨✴⟩₂ step) (app l⊑A (Labeled' l⋤A) e₃) = ⊥-elim (l⋤A l⊑A)
progress⇝ v (⟨✴⟩₂ step) (app l⊑A (Labeledχ' l⋤A) e₃) = ⊥-elim (l⋤A l⊑A)
progress⇝ v ⟨✴⟩₃ (app l⊑A (Labeled l⊑A₁ e) (Labeled l⊑A₂ e₁)) = Step ⟨✴⟩₃
progress⇝ v ⟨✴⟩₃ (app l⊑A (Labeled l⊑A₁ e) (Labeled' l⋤A)) = ⊥-elim (l⋤A l⊑A₁)
progress⇝ v ⟨✴⟩₃ (app l⊑A (Labeled l⊑A₁ e) (Labeledχ' l⋤A)) = ⊥-elim (l⋤A l⊑A₁)
progress⇝ v ⟨✴⟩₃ (app l⊑A (Labeled' l⋤A) e₁) = ⊥-elim (l⋤A l⊑A)
progress⇝ v ⟨✴⟩₃ (app l⊑A (Labeledχ' l⋤A) e₁) = ⊥-elim (l⋤A l⊑A)
progress⇝ v (Relabel₁ l⊑h step) (relabel .l⊑h h⊑A e₁)
  with progress⇝ v step e₁
... | Step step' = Step (Relabel₁ l⊑h step')
progress⇝ v (Relabel₂ l⊑h) (relabel .l⊑h h⊑A (Labeled l⊑A e₁)) = Step (Relabel₂ l⊑h)
progress⇝ v (Relabel₂ l⊑h) (relabel .l⊑h h⊑A (Labeled' l⋤A)) = ⊥-elim (l⋤A (trans-⊑ l⊑h h⊑A))
progress⇝ v (Relabel₂ l⊑h) (relabel .l⊑h h⊑A (Labeledχ' l⋤A)) = ⊥-elim v
progress⇝ v Fmap∙ (fmap' l⋤A e₁ e₃) = Step Fmap
progress⇝ v Fmap∙ (fmap∙ e₁ e₃)  = ⊥-elim v
progress⇝ v (⟨✴⟩∙₁ step) (app' l⋤A e₁ e₃)
  with progress⇝ (proj₁ v) step e₁
... | Step step' = Step (⟨✴⟩₁ step')
progress⇝ v (⟨✴⟩∙₁ step) (app∙ e₁ e₃) = ⊥-elim v
progress⇝ v (⟨✴⟩∙₂ step) (app' l⋤A (Labeled l⊑A e₁) e₃) = ⊥-elim (l⋤A l⊑A)
progress⇝ v (⟨✴⟩∙₂ step) (app' l⋤A (Labeled' l⋤A₂) e₃)
  with progress⇝ (proj₂ v) step e₃
... | Step step' = Step (⟨✴⟩₂ step')
progress⇝ v (⟨✴⟩∙₂ step) (app' l⋤A (Labeledχ' l⋤A₂) e₃) = ⊥-elim (proj₁ v)
progress⇝ v (⟨✴⟩∙₂ step) (app∙ e₁ e₃) = ⊥-elim v
progress⇝ v ⟨✴⟩∙₃ (app' l⋤A (Labeled l⊑A e₁) e₃) = ⊥-elim (l⋤A l⊑A)
progress⇝ v ⟨✴⟩∙₃ (app' l⋤A (Labeled' l⋤A₁) (Labeled l⊑A e₃)) = ⊥-elim (l⋤A₁ l⊑A)
progress⇝ v ⟨✴⟩∙₃ (app' l⋤A (Labeled' l⋤A₁) (Labeled' l⋤A₂)) = Step ⟨✴⟩₃
progress⇝ v ⟨✴⟩∙₃ (app' l⋤A (Labeled' l⋤A₁) (Labeledχ' l⋤A₂)) = ⊥-elim (proj₂ v)
progress⇝ v ⟨✴⟩∙₃ (app' l⋤A (Labeledχ' l⋤A₁) e₃) = ⊥-elim (proj₁ v)
progress⇝ v ⟨✴⟩∙₃ (app∙ e₁ e₃) = ⊥-elim v
progress⇝ v (Relabel∙₁ l⊑h step) (relabel' .l⊑h h⋤A e₁)
  with progress⇝ v step e₁
... | Step step' = Step (Relabel₁ l⊑h step')
progress⇝ v (Relabel∙₁ l⊑h step) (relabel∙ .l⊑h e₁) = ⊥-elim v
progress⇝ v (Relabel∙₂ l⊑h) (relabel' .l⊑h h⋤A (Labeled l⊑A e₁)) = Step (Relabel₂ l⊑h)
progress⇝ v (Relabel∙₂ l⊑h) (relabel' .l⊑h h⋤A (Labeled' l⋤A)) = Step (Relabel₂ l⊑h)
progress⇝ v (Relabel∙₂ l⊑h) (relabel' .l⊑h h⋤A (Labeledχ' l⋤A)) = ⊥-elim v
progress⇝ v (Relabel∙₂ l⊑h) (relabel∙ .l⊑h e₁) = ⊥-elim v
progress⇝ v (⟨✴⟩₁χ step) (app l⊑A (Labeledχ l⊑A₂ e₁) e₃) = ⊥-elim (proj₁ v)
progress⇝ v ⟨✴⟩₂χ (app l⊑A (Labeledχ l⊑A₁ e₁) e₃) = ⊥-elim (proj₁ v)
progress⇝ v ⟨✴⟩₃χ (app l⊑A _ (Labeledχ l⊑A₁ e₁)) = ⊥-elim (proj₂ v)
progress⇝ v ⟨✴⟩₄χ (app l⊑A (Labeledχ l⊑A₁ e₁) e₃) = ⊥-elim (proj₁ v)
progress⇝ v (Relabelχ l⊑h) (relabel .l⊑h h⊑A (Labeledχ l⊑A e₁)) = ⊥-elim v
progress⇝ v (Relabel∙χ l⊑h) (relabel' .l⊑h h⋤A (Labeledχ l⊑A e₁)) = ⊥-elim v
progress⇝ v (Relabel∙χ l⊑h) (relabel∙ .l⊑h (Labeledχ l⊑A e₁)) = ⊥-elim v
progress⇝ v Hole ∙ = ⊥-elim v

--------------------------------------------------------------------------------

-- If we can write at address n to the low erased-memory Mᴱ, then we
-- can also write to the same address from the non-erased memory M.
writeᴸ : ∀ {τ l n} {M₁ M₁ᴱ M₂ᴱ  : Memory l} {c cᴱ : Cell τ} -> Eraseᶜ c cᴱ ->
              M₂ᴱ ≔ M₁ᴱ [ n ↦ cᴱ ]ᴹ -> Eraseᴹ M₁ M₁ᴱ ->
                (∃ (λ M₂ -> Eraseᴹ M₂ M₂ᴱ × M₂ ≔ M₁ [ n ↦ c ]ᴹ))
writeᴸ c here (_ ∷ e) = _ , (c ∷ e) , here
writeᴸ c (there w) (c' ∷ e) with writeᴸ c w e
... | M₂ , e' , w' = (_ ∷ M₂) , (c' ∷ e') , there w'

-- If we can read form address n of the low erased-memory Mᴱ, then we
-- can also read at the same address from the non-erased memory M.
readᴸ : ∀ {τ l n} {M Mᴱ : Memory l} {cᴱ : Cell τ} -> n ↦ cᴱ ∈ᴹ Mᴱ -> Eraseᴹ M Mᴱ -> ∃ (λ c -> Eraseᶜ c cᴱ × n ↦ c ∈ᴹ M)
readᴸ here (x ∷ eᴹ) = _ , x , here
readᴸ (there r) (x ∷ eᴹ) with readᴸ r eᴹ
... | c , eᶜ , r' = c , eᶜ , there r'


-- If the address (n) before erasure was valid, then we can write to the non-erased memory.
writeᴴ : ∀ {n τ l} {M₁ : Memory l} -> (c : Cell τ) -> ValidAddr τ M₁ n -> ∃ (λ M₂ -> M₂ ≔ M₁ [ n ↦ c ]ᴹ)
writeᴴ c here = _ , here
writeᴴ c (there v) with writeᴴ c v
... | M₂ , w = (_ ∷ M₂) , there w

-- 1-step progress for impure semantics: if ε(p) ⟼ ε(p') then p ⟼ p'.
-- Note that we use Redex and existentially quantify on p' to avoid
-- unification issues (there could be many terms that get erased to
-- ε(p').
progress⟼ : ∀ {τ L} {L⊑A : L ⊑ A} {p₁ p₁' p₂' : Program (Mac L τ)}
              -> validᴾ p₁ -> p₁' ⟼ p₂' -> Eraseᴾ (yes L⊑A) p₁ p₁' -> Redexᴾ p₁
progress⟼ {p₁ = ⟨ Σ₁ , t₁ ⟩} v (Lift step) (lowᴾ _ eˢ e) with progress⇝ (proj₂ v) step e
... | Step step' = Step (Lift step')
progress⟼ v (Label' l⊑h) (lowᴾ _ eˢ (label h⊑A e)) = Step (Label' l⊑h)
progress⟼ v (Label' l⊑h) (lowᴾ _ eˢ (label' h⋤A)) = Step (Label' l⊑h)
progress⟼ v (Unlabel₁ l⊑h (Lift step)) (lowᴾ _ eˢ (unlabel .l⊑h e)) with progress⇝ (proj₂ v) step e
... | Step step' = Step (Unlabel₁ l⊑h (Lift step'))
progress⟼ v (Unlabel₂ l⊑h) (lowᴾ _ eˢ (unlabel .l⊑h (Labeled l⊑A e))) = Step (Unlabel₂ l⊑h)
progress⟼ v (Unlabel₂ l⊑h) (lowᴾ L⊑A eˢ (unlabel .l⊑h (Labeled' l⋤A))) = ⊥-elim (l⋤A (trans-⊑ l⊑h L⊑A))
progress⟼ v (Unlabel₂ l⊑h) (lowᴾ _ eˢ (unlabel .l⊑h (Labeledχ' l⋤A)))  = ⊥-elim (proj₂ v)
progress⟼ v (Unlabelχ l⊑h) (lowᴾ _ eˢ (unlabel .l⊑h (Labeledχ l⊑A e))) = Step (Unlabelχ l⊑h)
progress⟼ (vˢ , vᵗ) (Bind₁ step) (lowᴾ L⊑A eˢ (e >>= e₁)) with progress⟼ (vˢ , proj₁ vᵗ) step (lowᴾ L⊑A eˢ e)
... | Step step' = Step (Bind₁ step')
progress⟼ v Bind₂ (lowᴾ _ eˢ (return e >>= e₂)) = Step Bind₂
progress⟼ v Bind₃ (lowᴾ _ eˢ (throw e >>= e₂)) = Step Bind₃
progress⟼ v (Join l⊑h x) (lowᴾ _ eˢ (join .l⊑h h⊑A e)) = ⊥-elim (proj₂ v)
progress⟼ v (Joinχ l⊑h x) (lowᴾ _ eˢ (join .l⊑h h⊑A e)) = ⊥-elim (proj₂ v)
progress⟼ v (Join∙ l⊑h) (lowᴾ _ eˢ (join' .l⊑h h⋤A e)) = ⊥-elim (proj₂ v)
progress⟼ v (Join∙ l⊑h) (lowᴾ _ eˢ (join∙ .l⊑h e)) = ⊥-elim (proj₂ v)
progress⟼ (vˢ , vᵗ) (Catch₁ step) (lowᴾ L⊑A eˢ (catch e e₂)) with progress⟼ (vˢ , proj₁ vᵗ) step (lowᴾ L⊑A eˢ e)
... | Step step' = Step (Catch₁ step')
progress⟼ v Catch₂ (lowᴾ _ eˢ (catch (return e) e₂)) = Step Catch₂
progress⟼ v Catch₃ (lowᴾ _ eˢ (catch (throw e) e₂)) = Step Catch₃
progress⟼ v (New l⊑h) (lowᴾ _ eˢ (new .l⊑h h⊑A e)) = Step (New l⊑h)
progress⟼ (vˢ , vᵗ) (Write₁ l⊑h (Lift step)) (lowᴾ _ eˢ (write .l⊑h h⊑A e e₂)) with progress⇝ (proj₂ vᵗ) step e₂
... | Step step' = Step (Write₁ l⊑h (Lift step'))
progress⟼ v (Write₂ l⊑h w) (lowᴾ _ eˢ (write {h = h} .l⊑h h⊑A e (Ref l⊑A #[ n ])))
  with writeᴸ ⟦ e ⟧ w (getᴸ eˢ l⊑A)
... | _ , _ , w'  = Step (Write₂ l⊑h w')
progress⟼ v (Read₁ l⊑h (Lift step)) (lowᴾ _ eˢ (read .l⊑h e)) with progress⇝ (proj₂ v) step e
... | Step step' = Step (Read₁ l⊑h (Lift step'))
progress⟼ v (Read₂ l⊑h r) (lowᴾ _ eˢ (read .l⊑h (Ref l⊑A #[ n ])))
  with readᴸ r (getᴸ eˢ l⊑A)
... | .(⟦ _ ⟧) , ⟦ x ⟧ , r' = Step (Read₂ l⊑h r')
progress⟼ v (New∙ l⊑h) (lowᴾ _ eˢ (new' .l⊑h h⋤A e)) = Step (New l⊑h)
progress⟼ v (New∙ l⊑h) (lowᴾ _ eˢ (new∙ .l⊑h e)) = ⊥-elim (proj₂ v)
progress⟼ (vˢ , vᵗ) (Write∙₁ l⊑h (Lift step)) (lowᴾ _ eˢ (write' .l⊑h h⋤A e e₂))
  with progress⇝ (proj₂ vᵗ ) step e₂
... | Step step' = Step (Write₁ l⊑h (Lift step'))
progress⟼ v (Write∙₁ l⊑h step) (lowᴾ _ eˢ (write∙ .l⊑h e e₂)) = ⊥-elim (proj₂ v)
progress⟼ v (Write∙₂ l⊑h) (lowᴾ _ eˢ (write' .l⊑h h⋤A e (Ref l⊑A e₂))) = ⊥-elim (h⋤A l⊑A)
progress⟼ (vˢ , proj₃ , is#[ n ] , vᴬ) (Write∙₂ l⊑h) (lowᴾ _ eˢ (write' {t₁ = t₁} .l⊑h h⋤A e (Ref' l⋤A)))
  with writeᴴ ⟦ t₁ ⟧ vᴬ
... | M₂ , w = Step (Write₂ l⊑h w)
progress⟼ v (Write∙₂ l⊑h) (lowᴾ _ eˢ (write∙ .l⊑h e e₂)) = ⊥-elim (proj₂ v)
progress⟼ v (Fork l⊑h) (lowᴾ _ eˢ (fork .l⊑h h⊑A e)) = Step (Fork l⊑h)
progress⟼ v (Fork∙ l⊑h) (lowᴾ _ eˢ (fork' .l⊑h h⋤A e)) = Step (Fork l⊑h)
progress⟼ v (Fork∙ l⊑h) (lowᴾ _ eˢ (fork∙ .l⊑h e)) = ⊥-elim (proj₂ v)
progress⟼ v (NewMVar l⊑h) (lowᴾ _ eˢ (newMVar .l⊑h h⊑A)) = Step (NewMVar l⊑h)
progress⟼ v (Take₁ (Lift step)) (lowᴾ _ eˢ (take e)) with progress⇝ (proj₂ v) step e
... | Step step' = Step (Take₁ (Lift step'))
progress⟼ v (Take₂ r w) (lowᴾ _ eˢ (take (MVar l⊑A #[ n ])))
  with readᴸ r (getᴸ eˢ l⊑A)
... | _ , ⟦ _ ⟧ , r' with writeᴸ ⊗ w (getᴸ eˢ l⊑A)
... | _ , _ , w' =  Step (Take₂ r' w')
progress⟼ (_ , vᵗ) (Put₁ (Lift step)) (lowᴾ _ eˢ (put e e₁))
  with progress⇝ (proj₂ vᵗ) step e₁
... | Step step' = Step (Put₁ (Lift step'))
progress⟼ v (Put₂ r w) (lowᴾ _ eˢ (put e (MVar l⊑A #[ n ])))
  with readᴸ r (getᴸ eˢ l⊑A)
... | _ , ⊗ , r' with writeᴸ ⟦ e ⟧ w (getᴸ eˢ l⊑A)
... | _ , _ , w' = Step (Put₂ r' w')
progress⟼ v (NewMVar∙ l⊑h) (lowᴾ _ eˢ (newMVar' .l⊑h h⋤A)) = Step (NewMVar l⊑h)
progress⟼ v (NewMVar∙ l⊑h) (lowᴾ _ eˢ (newMVar∙ .l⊑h)) = ⊥-elim (proj₂ v)

-- 1-step progress for impure semantics: if ε(p) ⟼ ε(p') then p ⟼ p'.
-- Note that we use Redex and existentially quantify on p' to avoid
-- unification issues (there could be many terms that get erased to
-- ε(p').
progress⟼' : ∀ {L τ} {p₁ p₂ : Program (Mac L τ)} {{p₁ⱽ : validᴾ p₁}} (L⊑A : L ⊑ A) -> εᴾ (yes L⊑A) p₁ ⟼ εᴾ (yes L⊑A) p₂ -> Redexᴾ p₁
progress⟼' {L} {p₁ = p₁} {p₂} {{vᴾ}} L⊑A step = progress⟼ vᴾ step (lift-εᴾ (yes L⊑A) p₁)

--------------------------------------------------------------------------------

open import Sequential.Security.Simulation 𝓛 A

-- If a term is a value, then the erased term is also a value
valᴱ : ∀ {Γ τ} {t t' : Term Γ τ} -> Value t -> Erase t t' -> Value t'
valᴱ （） （） = （）
valᴱ True True = True
valᴱ False False = False
valᴱ (Λ t) (Λ e) = Λ _
valᴱ ξ ξ = ξ
valᴱ (return t) (return e) = return _
valᴱ (throw t) (throw e) = throw _
valᴱ (Labeled t) (Labeled l⊑A e) = Labeled _
valᴱ (Labeled t) (Labeled' l⋤A) = Labeled ∙
valᴱ (Labeledχ t) (Labeledχ l⊑A e) = Labeledχ _
valᴱ (Labeledχ t) (Labeledχ' l⋤A) = Labeled ∙
valᴱ (Ref t) (Ref l⊑A e) = Ref _
valᴱ (Ref t) (Ref' l⋤A) = Ref ∙
valᴱ (MVar t) (MVar l⊑A e) = MVar _
valᴱ (MVar t) (MVar' l⋤A) = MVar ∙

-- If a term is a value, then the erased term is also a value
val-ε : ∀ {τ Γ} → {t : Term Γ τ} → Value t → Value (ε t)
val-ε {t = t} v = valᴱ v (lift-ε t)

-- If the erased term is not a value, then the original term is not a value
¬val⁻ᴱ : ∀ {Γ τ} {t t' : Term Γ τ} -> Erase t t' -> ¬ (Value t') -> ¬ (Value t)
¬val⁻ᴱ （） ¬val （） = ¬val （）
¬val⁻ᴱ True ¬val True = ¬val True
¬val⁻ᴱ False ¬val False = ¬val False
¬val⁻ᴱ (Λ e) ¬val (Λ t) = ¬val (Λ _)
¬val⁻ᴱ ξ ¬val ξ = ¬val ξ
¬val⁻ᴱ (return e) ¬val (return t) = ¬val (return _)
¬val⁻ᴱ (throw e) ¬val (throw t) = ¬val (throw _)
¬val⁻ᴱ (Labeled l⊑A e) ¬val (Labeled t) = ¬val (Labeled _)
¬val⁻ᴱ (Labeled' l⋤A) ¬val (Labeled t) = ¬val (Labeled ∙)
¬val⁻ᴱ (Labeledχ l⊑A e) ¬val (Labeledχ t) = ¬val (Labeledχ _)
¬val⁻ᴱ (Labeledχ' l⋤A) ¬val (Labeledχ t) = ¬val (Labeled ∙)
¬val⁻ᴱ (Ref l⊑A e) ¬val (Ref t) = ¬val (Ref _)
¬val⁻ᴱ (Ref' l⋤A) ¬val (Ref t) = ¬val (Ref ∙)
¬val⁻ᴱ (MVar l⊑A e) ¬val (MVar t) = ¬val (MVar _)
¬val⁻ᴱ (MVar' l⋤A) ¬val (MVar t) = ¬val (MVar ∙)

-- If the original term is not a value, then the erased term is not a value
¬valᴱ : ∀ {Γ τ} {t t' : Term Γ τ}  -> Erase t t' -> ¬ (Value t) -> ¬ (Value t')
¬valᴱ （） ¬val （） = ¬val （）
¬valᴱ True ¬val True = ¬val True
¬valᴱ False ¬val False = ¬val False
¬valᴱ (Λ e) ¬val (Λ t) = ¬val (Λ _)
¬valᴱ ξ ¬val ξ = ¬val ξ
¬valᴱ (return e) ¬val (return t) = ¬val (return _)
¬valᴱ (throw e) ¬val (throw t) = ¬val (throw _)
¬valᴱ (Labeled l⊑A e) ¬val (Labeled t) = ¬val (Labeled _)
¬valᴱ (Labeled' l⋤A) ¬val (Labeled t) = ¬val (Labeled _)
¬valᴱ (Labeledχ l⊑A e) ¬val (Labeledχ t) = ¬val (Labeledχ _)
¬valᴱ (Labeledχ' l⋤A) ¬val (Labeled ._) = ¬val (Labeledχ _)
¬valᴱ (Ref l⊑A e) ¬val (Ref t) = ¬val (Ref _)
¬valᴱ (Ref' l⋤A) ¬val (Ref t) = ¬val (Ref _)
¬valᴱ (MVar l⊑A e) ¬val (MVar t) = ¬val (MVar _)
¬valᴱ (MVar' l⋤A) ¬val (MVar t) = ¬val (MVar _)

-- If a low valid erased program steps, so does the original program.
redex⁻ᴱ : ∀ {l τ} {p p' : Program (Mac l τ)} {{pⱽ : validᴾ p}} {l⊑A : l ⊑ A}  -> Eraseᴾ (yes l⊑A) p p' -> Redexᴾ p' -> Redexᴾ p
redex⁻ᴱ {{pⱽ}} {l⊑A} e (Step step) = progress⟼ pⱽ step e

-- If a low program steps, then its erasure steps.
redexᴱ : ∀ {l τ} {p p' : Program (Mac l τ)} {l⊑A : l ⊑ A} -> Eraseᴾ (yes l⊑A) p p' -> Redexᴾ p -> Redexᴾ p'
redexᴱ {l⊑A = l⊑A} e (Step step) with εᴾ-sim (yes l⊑A) step | unlift-εᴾ (yes l⊑A) e
... | step' | eq rewrite eq = Step step'

-- If the original valid low program  does not step, then its erasure does not step.
¬redexᴱ : ∀ {l τ} {p p' : Program (Mac l τ)} {l⊑A : l ⊑ A} {{pⱽ : validᴾ p}} -> Eraseᴾ (yes l⊑A) p p' -> ¬ (Redexᴾ p) -> ¬ (Redexᴾ p')
¬redexᴱ {{pⱽ}} e = contrapositive (redex⁻ᴱ e)

-- If an erased low program does not step, then the original program does not step.
¬redex⁻ᴱ : ∀ {l τ} {p p' : Program (Mac l τ)} {l⊑A : l ⊑ A} -> Eraseᴾ (yes l⊑A) p p' -> ¬ (Redexᴾ p') -> ¬ (Redexᴾ p)
¬redex⁻ᴱ e = contrapositive (redexᴱ e)

-- If a valid low program is stuck, then its erasure is also stuck.
stuckᴱ : ∀ {l τ} {p p' : Program (Mac l τ)} {l⊑A : l ⊑ A} {{pⱽ : validᴾ p}} -> Eraseᴾ (yes l⊑A) p p' -> Stuckᴾ p -> Stuckᴾ p'
stuckᴱ (lowᴾ l⊑A eˢ e) (¬done , ¬redex) = (¬valᴱ e ¬done) , ¬redexᴱ (lowᴾ l⊑A eˢ e) ¬redex

stuck-ε : ∀ {L τ} {p : Program (Mac L τ)} {{pⱽ : validᴾ p}} -> (L⊑A : L ⊑ A) → Stuckᴾ p -> Stuckᴾ (εᴾ (yes L⊑A) p)
stuck-ε L⊑A = stuckᴱ (lift-εᴾ (yes L⊑A) _)
