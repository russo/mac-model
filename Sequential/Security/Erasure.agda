-- This module defines the erasure function for the sequential
-- calculus and proves a few basic lemmas about it.  The module
-- parameter A denotes the security level of the attacker.

import Lattice as L

module Sequential.Security.Erasure (𝓛 : L.Lattice) (A : L.Label 𝓛) where

open import Types 𝓛

open import Sequential.Calculus 𝓛
open import Sequential.Semantics 𝓛

open import Data.Sum
open import Relation.Binary.PropositionalEquality hiding (subst ; [_])
open import Relation.Nullary
open import Data.Empty

--------------------------------------------------------------------------------
-- Erasure function for different components of the calculus

-- Erasure function for terms
ε : ∀ {τ Γ} -> Term Γ τ -> Term Γ τ
ε (var τ∈Γ) = var τ∈Γ
ε (Λ t) = Λ (ε t)
ε (t ∘ t₁) = (ε t) ∘ (ε t₁)
ε （） = （）
ε True = True
ε False = False
ε (if t then t₁ else t₂) = if (ε t) then (ε t₁) else (ε t₂)
ε (return l t) = return l (ε t)
ε (t >>= t₁) = ε t >>= ε t₁
ε ξ = ξ
ε (throw l t) = throw l (ε t)
ε (catch t t₁) = catch (ε t) (ε t₁)
ε (Labeled l t) with l ⊑? A
... | yes _ = Labeled l (ε t)
... | no _ = Labeled l ∙
ε (Labeledχ l t) with l ⊑? A
... | yes _ = Labeledχ l (ε t)
... | no _ = Labeled l ∙
ε (label {h = h} l⊑h t) with h ⊑? A
... | yes _ = label l⊑h (ε t)
... | no _ = label l⊑h ∙
ε (unlabel l⊑h t) = unlabel l⊑h (ε t)
ε (join {h = h} l⊑h t) with h ⊑? A
... | yes _ = join l⊑h (ε t)
... | no _ = join∙ l⊑h (ε t)
ε (join∙ l⊑h t) = join∙ l⊑h (ε t)
ε (fork {h = h} l⊑h t) with h ⊑? A
... | yes _ = fork l⊑h (ε t)
... | no _ = fork∙ l⊑h (ε t)
ε (fork∙ l⊑h t) = fork∙ l⊑h (ε t)
ε #[ n ] = #[ n ]
ε (Ref l t) with l ⊑? A
... | yes _ = Ref l (ε t)
... | no _ = Ref l ∙
ε (read l⊑h t) = read l⊑h (ε t)
ε (write {h = h} l⊑h t t₁) with h ⊑? A
... | yes _ = write l⊑h (ε t) (ε t₁)
... | no _ = write∙ l⊑h (ε t) (ε t₁)
ε (write∙ l⊑h t t₁) = write∙ l⊑h (ε t) (ε t₁)
ε (new {h = h} l⊑h t) with h ⊑? A
... | yes _ = new l⊑h (ε t)
... | no _ = new∙ l⊑h (ε t)
ε (new∙ l⊑h t) = new∙ l⊑h (ε t)
ε (MVar l t) with l ⊑? A
... | yes _ = MVar l (ε t)
... | no _ = MVar l ∙
ε (newMVar {h = h} l⊑h)  with h ⊑? A
... | yes _ = newMVar l⊑h
... | no _ = newMVar∙ l⊑h
ε (newMVar∙ l⊑h) = newMVar∙ l⊑h
ε (take t) = take (ε t)
ε (put t t₁) = put (ε t) (ε t₁)
ε {τ = Labeled l _} (t ⟨$⟩ t₁) with l ⊑? A
... | yes _ = (ε t) ⟨$⟩ (ε t₁)
... | no _ =  (ε t) ⟨$⟩∙ (ε t₁)
ε (t ⟨$⟩∙ t₁) = (ε t) ⟨$⟩∙ ε t₁
ε {τ = Labeled l τ} (t ⟨✴⟩ t₁) with l ⊑? A
... | yes _ = (ε t) ⟨✴⟩ (ε t₁)
... | no _ = (ε t) ⟨✴⟩∙ (ε t₁)
ε (t ⟨✴⟩∙ t₁) = (ε t) ⟨✴⟩∙ (ε t₁)
ε (relabel {h = h} l⊑h t) with h ⊑? A
... | yes _ = relabel l⊑h (ε t)
... | no _ =  relabel∙ l⊑h (ε t)
ε (relabel∙ l⊑h t) = relabel∙ l⊑h (ε t)
ε ∙ = ∙

-- Homomorphic Cell Erasure
εᶜ : ∀ {τ} -> Cell τ -> Cell τ
εᶜ ⊗ = ⊗
εᶜ ⟦ t ⟧ = ⟦ ε t ⟧

-- Homomorphic Memory Erasure
εᴹ : ∀ {l} -> Memory l -> Memory l
εᴹ ∙ = ∙
εᴹ [] = []
εᴹ (c ∷ M) = (εᶜ c) ∷ (εᴹ M)

-- Sensitive memories are collapsed to ∙, otherwise they are erased homomorphically.
εˢ : Store -> Store
εˢ Σ l with l ⊑? A
... | yes _ = εᴹ (Σ l)
... | no _  = ∙

-- Erasure for configurations, i.e., Mac sequential programs with memory.
εᴾ : ∀ {l τ} -> Dec (l ⊑ A) -> Program (Mac l τ) -> Program (Mac l τ)
εᴾ (yes _) ⟨ Σ , t ⟩ = ⟨ εˢ Σ , ε t ⟩
εᴾ (no _) ⟨ Σ , t ⟩ = ⟨ εˢ Σ , ∙ ⟩

--------------------------------------------------------------------------------
-- Several auxiliary lemmas. Many show that erasure is homomorphic over
-- many term manipulation (e.g., substituting, weakingin etc.) i.e.,
-- the order with which we perform the erasure and the operations is
-- irrelevant.

-- Erasure is homomorphic over weakening, i.e.,
-- weakening and then erasing is the same as first weakening and then erasing
ε-wken : ∀ {τ π₁ π₂} -> (t : Term π₁ τ) (p : π₁ ⊆ π₂) -> ε (wken t p) ≡ wken (ε t) p
ε-wken （） p = refl
ε-wken True p = refl
ε-wken False p = refl
ε-wken (Λ t) p with ε-wken t (cons p)
... | x rewrite x = refl
ε-wken (t ∘ t₁) p
  rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (if t then t₁ else t₂) p
  rewrite ε-wken t p | ε-wken t₁ p | ε-wken t₂ p = refl
ε-wken (return l t) p rewrite ε-wken t p = refl
ε-wken (t >>= t₁) p
  rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (Labeled l t) p with l ⊑? A
... | no _ = refl
... | yes _ rewrite ε-wken t p = refl
ε-wken (label {h = H} l⊑h t) p with H ⊑? A
... | no ¬p rewrite ε-wken t p = refl
... | yes _ rewrite ε-wken t p = refl
ε-wken (unlabel l⊑h t) p rewrite ε-wken t p = refl
ε-wken (read x t) p rewrite ε-wken t p = refl
ε-wken (write {h = H} x t t₁) p with H ⊑? A
... | yes _ rewrite ε-wken t p | ε-wken t₁ p = refl
... | no _ rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (write∙ x t t₁) p rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (new {h = H} x t) p with H ⊑? A
... | yes _  rewrite ε-wken t p = refl
... | no _ rewrite ε-wken t p = refl
ε-wken (new∙ x t) p rewrite ε-wken t p = refl
ε-wken #[ n ] p = refl
ε-wken (fork {h = h} l⊑h t) p with h ⊑? A
... | yes _ rewrite ε-wken t p = refl
... | no _ rewrite ε-wken t p = refl
ε-wken (fork∙ l⊑h t) p rewrite ε-wken t p = refl
ε-wken (var τ∈Γ₁) p = refl
ε-wken ξ p = refl
ε-wken (throw l t) p rewrite ε-wken t p = refl
ε-wken (catch t t₁) p
    rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (Labeledχ l t) p with l ⊑? A
... | yes _ rewrite ε-wken t p = refl
... | no _ rewrite ε-wken t p = refl
ε-wken (join {h = h} l⊑h t) p with h ⊑? A
... | yes _ rewrite ε-wken t p = refl
... | no _ rewrite ε-wken t p = refl
ε-wken (join∙ l⊑h t) p rewrite ε-wken t p = refl
ε-wken (Ref l t) p with l ⊑? A
... | yes _ rewrite ε-wken t p = refl
... | no _ rewrite ε-wken t p = refl
ε-wken (MVar l t) p with l ⊑? A
... | yes _ rewrite ε-wken t p = refl
... | no _ rewrite ε-wken t p = refl
ε-wken (newMVar {h = h} l⊑h) p with h ⊑? A
... | yes _ = refl
... | no _ = refl
ε-wken (newMVar∙ l⊑h) p = refl
ε-wken (take t) p rewrite ε-wken t p = refl
ε-wken (put t t₁) p
  rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken {Labeled l τ} (t ⟨$⟩ t₁) p with l ⊑? A
... | yes _ rewrite ε-wken t p | ε-wken t₁ p = refl
... | no _ rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (t ⟨$⟩∙ t₁) p
  rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken {Labeled l _} (t ⟨✴⟩ t₁) p with l ⊑? A
... | yes _ rewrite ε-wken t p | ε-wken t₁ p = refl
... | no _ rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (t ⟨✴⟩∙ t₁) p
  rewrite ε-wken t p | ε-wken t₁ p = refl
ε-wken (relabel {h = h} l⊑h t) p with h ⊑? A
... | yes _ rewrite ε-wken t p = refl
... | no _ rewrite ε-wken t p = refl
ε-wken (relabel∙ l⊑h t) p rewrite ε-wken t p = refl
ε-wken ∙ p = refl

{-# REWRITE ε-wken #-}

--------------------------------------------------------------------------------

-- Erasure is homomorphic over substitution, i.e.,
-- substituting and then erasing is the same as first erasing and then substituting
ε-subst : ∀ {τ τ' Γ} (t₁ : Term Γ τ') (t₂ : Term (τ' ∷ Γ) τ) -> ε (subst t₁ t₂) ≡ subst (ε t₁) (ε t₂)
ε-subst = ε-tm-subst [] _
  where ε-var-subst  :  ∀ {α β} (Γ₁ : Context) (Γ₂ : Context) (t₁ : Term Γ₂ α) (β∈Γ : β ∈ (Γ₁ ++ [ α ] ++ Γ₂))
                      ->  ε (var-subst Γ₁ Γ₂ t₁ β∈Γ) ≡ var-subst Γ₁ Γ₂ (ε t₁) β∈Γ
        ε-var-subst [] Γ₂ t₁ here = refl
        ε-var-subst [] Γ₁ t₁ (there β∈Γ) = refl
        ε-var-subst (β ∷ Γ₁) Γ₂ t₁ here = refl
        ε-var-subst (τ ∷ Γ₁) Γ₂ t₁ (there β∈Γ)
          rewrite ε-wken (var-subst Γ₁ Γ₂ t₁ β∈Γ) (drop {_} {τ} refl-⊆) | ε-var-subst Γ₁ Γ₂ t₁ β∈Γ = refl

        ε-tm-subst : ∀ {τ τ'} (Γ₁ : Context) (Γ₂ : Context) (t₁ : Term Γ₂ τ') (t₂ : Term (Γ₁ ++ [ τ' ] ++ Γ₂) τ)
                   ->  ε (tm-subst Γ₁ Γ₂ t₁ t₂) ≡ tm-subst Γ₁ Γ₂ (ε t₁) (ε t₂)
        ε-tm-subst Γ₁ Γ₂ t₁ （） = refl
        ε-tm-subst Γ₁ Γ₂ t₁ True = refl
        ε-tm-subst Γ₁ Γ₂ t₁ False = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (var τ∈Γ) rewrite ε-var-subst Γ₁ Γ₂ t₁ (∈ᴿ-∈  τ∈Γ) = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (Λ t₂)  rewrite ε-tm-subst (_ ∷ Γ₁) Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (t₂ ∘ t₃)
          rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (if t₂ then t₃ else t₄)
          rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ | ε-tm-subst Γ₁ Γ₂ t₁ t₄ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (return l t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (t₂ >>= t₃)
          rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (Labeled l t₂) with l ⊑? A
        ε-tm-subst Γ₁ Γ₂ t₁ (Labeled l t₂) | yes p rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (Labeled l t₂) | no ¬p = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (label {h = H} l⊑h t₂) with H ⊑? A
        ε-tm-subst Γ₁ Γ₂ t₁ (label l⊑h t₂) | yes p rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (label l⊑h t₂) | no ¬p rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (unlabel l⊑h t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (read x t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (write {h = H} x t₂ t₃) with H ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (write∙ x t₂ t₃)
          rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (new {h = H} x t₂) with H ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (new∙ x t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ #[ n ] = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (fork {h = h} l⊑h t₂) with h ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (fork∙ l⊑h t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ ξ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (throw l t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (catch t₂ t₃) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (Labeledχ l t₂) with l ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (join {h = h} l⊑h t₂) with h ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (join∙ l⊑h t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (Ref l t₂) with l ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (MVar l t₂) with l ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (newMVar {h = h} l⊑h) with h ⊑? A
        ... | yes _ = refl
        ... | no _ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (newMVar∙ l⊑h) = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (take t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (put t₂ t₃)
          rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst {Labeled l τ} Γ₁ Γ₂ t₁ (t₂ ⟨$⟩ t₃) with l ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (t₂ ⟨$⟩∙ t₃)
          rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst {Labeled l τ} Γ₁ Γ₂ t₁ (t₂ ⟨✴⟩ t₃) with l ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (t₂ ⟨✴⟩∙ t₃)
          rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ | ε-tm-subst Γ₁ Γ₂ t₁ t₃ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (relabel {h = h} l⊑h t₂) with h ⊑? A
        ... | yes _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ... | no _ rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ (relabel∙ l⊑h t₂) rewrite ε-tm-subst Γ₁ Γ₂ t₁ t₂ = refl
        ε-tm-subst Γ₁ Γ₂ t₁ ∙ = refl

{-# REWRITE ε-subst #-}

-- Accessing and erasing a low memory is the same as first accessing and then erasing.
εˢ-εᴹ-≡ : ∀ {l} -> (Σ : Store) -> l ⊑ A -> εˢ Σ l ≡ εᴹ (Σ l)
εˢ-εᴹ-≡ {l} Σ l⊑A with l ⊑? A
εˢ-εᴹ-≡ {l} Σ l⊑A | yes p = refl
εˢ-εᴹ-≡ {l} Σ l⊑A | no l⋤A = ⊥-elim (l⋤A l⊑A)


-- Memory erasure does not change the length of a memory
lengthᴹ-≡ : ∀  (Σ : Store) -> (l : Label) -> lengthᴹ (Σ l) ≡ lengthᴹ (εᴹ (Σ l))
lengthᴹ-≡ Σ l = proof (Σ l)
  where proof : ∀ (M : Memory l) -> lengthᴹ M ≡ lengthᴹ (εᴹ M)
        proof ∙ = refl
        proof [] = refl
        proof (t ∷ M) rewrite proof M = refl

-- Extending and erasing a memory is the same as first erasing and then extending
newᴹ-≡ : ∀ {l τ} (M : Memory l) (c : Cell τ) -> εᴹ (newᴹ c M) ≡ newᴹ (εᶜ c) (εᴹ M)
newᴹ-≡ ∙ c = refl
newᴹ-≡ [] c = refl
newᴹ-≡ (c' ∷ M) c rewrite newᴹ-≡ M c = refl

--------------------------------------------------------------------------------

-- The attacker cannot observe allocations on high memories.
newᴴ-≡ : ∀ {h τ} -> (Σ : Store) (c : Cell τ) -> ¬ (h ⊑ A) ->
       let M = Σ h in
         εˢ (Σ [ h ↦ (newᴹ c M) ]ˢ) ≡ (εˢ Σ)
newᴴ-≡ {h} Σ c ¬p = fun-ext proof
  where proof : (l : Label) →  εˢ (Σ [ h ↦ newᴹ c (Σ h) ]ˢ) l ≡ εˢ Σ l
        proof l with l ⊑? A
        proof l | yes p with h ≟ l
        proof l | yes p | yes refl = ⊥-elim (¬p p)
        proof l | yes p | no ¬p = refl
        proof l | no ¬p = refl

-- Allocating and erasing is the same as erasing and allocating for low memories.
newᴸ-≡ : ∀ {L τ} -> (Σ : Store) (c : Cell τ) -> L ⊑ A ->
       let M = Σ L in
         εˢ (Σ [ L ↦ (newᴹ c M) ]ˢ) ≡ (εˢ Σ) [ L ↦ (newᴹ (εᶜ c) (εᴹ M)) ]ˢ
newᴸ-≡ {L} Σ c L⊑A = fun-ext proof
  where proof : (l : Label) →  εˢ (Σ [ L ↦ newᴹ c (Σ L) ]ˢ) l ≡ (εˢ Σ [ L ↦ newᴹ (εᶜ c) (εᴹ (Σ L)) ]ˢ) l
        proof l with l ⊑? A
        proof l | yes p with L ≟ l
        proof l | yes p | yes refl rewrite newᴹ-≡ (Σ l) c = refl
        proof l | yes p | no ¬p with l ⊑? A
        proof l | yes p | no ¬p | yes p₁ = refl
        proof l | yes p | no ¬p | no ¬p₁ = ⊥-elim (¬p₁ p)
        proof l | no ¬p with L ≟ l
        proof l | no ¬p | yes refl = ⊥-elim (¬p L⊑A)
        proof l | no ¬p | no ¬p₁ with l ⊑? A
        proof l | no ¬p | no ¬p₁ | yes p = ⊥-elim (¬p p)
        proof l | no ¬p | no ¬p₁ | no ¬p₂ = refl

--------------------------------------------------------------------------------

-- The attacker cannot boserve write operations on high memories.
writeᴴ-≡ : ∀ {h τ n} {c : Cell τ} {M₂ : Memory h} -> (Σ : Store) ->
           let M₁ = Σ h in
             h ⋤ A -> M₂ ≔ M₁ [ n ↦ c ]ᴹ -> (εˢ Σ) ≡ εˢ (Σ [ h ↦ M₂ ]ˢ)
writeᴴ-≡ {h} {M₂ = M₂} Σ h⋤A x = fun-ext proof
  where proof : ∀ (l : Label) -> εˢ Σ l ≡ εˢ (Σ [ h ↦ M₂ ]ˢ) l
        proof l with l ⊑? A
        proof l | yes p with h ≟ l
        proof l | yes h⊑A | yes refl = ⊥-elim (h⋤A h⊑A)
        proof l | yes p | no ¬p = refl
        proof l | no ¬p = refl

-- Writing on low memories and erasing is the same as first writing
-- and then erasing.
writeᴸ-≡ : ∀ {L n τ} {M₂ : Memory L} {c : Cell τ} -> (Σ : Store)  -> L ⊑ A ->
       let M₁ = Σ L in M₂ ≔ M₁ [ n ↦ c ]ᴹ ->
         εˢ (Σ [ L ↦ M₂ ]ˢ) ≡ (εˢ Σ) [ L ↦ εᴹ M₂ ]ˢ
writeᴸ-≡ {L} {M₂ = M₂} Σ L⊑A x = fun-ext proof
  where proof : (l : Label) -> εˢ (Σ [ L ↦ M₂ ]ˢ) l ≡ (εˢ Σ [ L ↦ εᴹ M₂ ]ˢ) l
        proof l with l ⊑? A
        proof l | yes p with L ≟ l
        proof l | yes p | yes refl = refl
        proof l | yes p | no ¬p with l ⊑? A
        proof l | yes p | no ¬p | yes p₁ = refl
        proof l | yes p | no ¬p | no ¬p₁ = ⊥-elim (¬p₁ p)
        proof l | no ¬p with l ≟ L
        proof l | no l⋤A | yes refl = ⊥-elim (l⋤A L⊑A)
        proof l | no ¬p | no ¬p₁ with L ≟ l
        proof l | no l⋤A | no ¬p₁ | yes refl = ⊥-elim (l⋤A L⊑A)
        proof l | no ¬p | no ¬p₁ | no ¬p₂ with l ⊑? A
        proof l | no ¬p | no ¬p₁ | no ¬p₂ | yes p = ⊥-elim (¬p p)
        proof l | no ¬p | no ¬p₁ | no ¬p₂ | no ¬p₃ = refl

--------------------------------------------------------------------------------
-- Homomorphic properties of read and write operations on low
-- memories.  They lift read and write proof objects to erased low
-- memories.

εᴹ-write : ∀ {L n τ} {M₂ : Memory L} {c : Cell τ} -> (Σ : Store) -> L ⊑ A ->
           let M₁ = Σ L in
             M₂ ≔ M₁ [ n ↦ c ]ᴹ -> (εᴹ M₂) ≔ (εᴹ M₁) [ n ↦ εᶜ c ]ᴹ
εᴹ-write {L} {n} {c = c} Σ L⊑A x = aux x
  where aux : ∀ {n} -> {M₁ M₂ : Memory L} -> M₂ ≔ M₁ [ n ↦ c ]ᴹ -> (εᴹ M₂) ≔ (εᴹ M₁) [ n ↦ εᶜ c ]ᴹ
        aux here = here
        aux (there w) = there (aux w)

εᴹ-read : ∀ {L n τ} {c : Cell τ} -> (Σ : Store) -> L ⊑ A ->
           let M = Σ L in
              n ↦ c ∈ᴹ M -> n ↦ (εᶜ c) ∈ᴹ (εᴹ M)
εᴹ-read {L} {c = c} Σ L⊑A x = aux x
  where aux : ∀ {n} -> {M : Memory L} ->  n ↦ c ∈ᴹ M -> n ↦ εᶜ c ∈ᴹ (εᴹ M)
        aux here = here
        aux (there x) = there (aux x)

--------------------------------------------------------------------------------
