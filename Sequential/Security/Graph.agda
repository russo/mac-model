-- This module defines the graph of the erasure function for the
-- sequential calculus. The graph of the function represents an
-- accessibility predicate in the style of Bove-Capretta (see
-- "Modelling general recursion in type theory"), which we use to
-- reason about non-erased terms starting from their erasure.
--
-- Example For example, we would like to show that t reduces, from the
-- proof that its erasure reduces, i.e., ε(t) ⇝ ε(t').  Plain
-- induction on such a proof term does not work in general, hence we
-- introduce fresh terms tᴱ tᴱ', together with the proof that they are
-- the erasure of t and t', i.e., Erasure t tᴱ and Erasure t' tᴱ'.  It
-- is now possible to do induction over tᴱ ⇝ tᴱ' and make the proof go
-- through.
--
-- The rest of the module defines for each element of the calculus its
-- corresponding accessibility predicate as an inductive data-type
-- together with the proofs that it captures correctly the erasure
-- function, i.e., Erasure t t' iff t ≡ ε(t')

import Lattice as L

module Sequential.Security.Graph (𝓛 : L.Lattice) (A : L.Label 𝓛) where

open import Types 𝓛
open import Sequential.Calculus  𝓛
open import Sequential.Security.Erasure 𝓛 A

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality hiding (subst)
open import Data.Empty
open import Data.Nat
open import Data.Product
open import Data.Sum

-- The graph of the erasure function for terms
data Erase {Γ} : ∀ {τ} -> Term Γ τ -> Term Γ τ -> Set where
  （） : Erase （） （）
  True : Erase True True
  False : Erase False False
  ξ : Erase ξ ξ
  var : ∀ {τ} ->  (τ∈Γ : τ ∈ᴿ Γ) -> Erase (var τ∈Γ) (var τ∈Γ)
  Λ : ∀ {τ₁ τ₂} {t t' : Term (τ₁ ∷ Γ) τ₂} -> Erase t t' -> Erase (Λ t) (Λ t')
  _∘_ : ∀ {τ₁ τ₂} {t₁ t₁' : Term Γ (τ₁ ➔ τ₂)} {t₂ t₂' : Term Γ τ₁} ->
          Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ ∘ t₂) (t₁' ∘ t₂')

  if_then_else_ : ∀ {τ} {t₁ t₁'} {t₂ t₂' t₃ t₃' : Term _ τ} ->
                  Erase t₁ t₁' -> Erase t₂ t₂' -> Erase t₃ t₃' ->
                  Erase (if t₁ then t₂ else t₃) (if t₁' then t₂' else t₃')

  return : ∀ {τ l} {t t' : Term _ τ} -> Erase t t' -> Erase (return l t) (return l t')
  _>>=_ : ∀ {l} {τ₁ τ₂} {t₁ t₁' : Term Γ (Mac l τ₁)} {t₂ t₂' :  Term Γ (τ₁ ➔ Mac l τ₂)} ->
            Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ >>= t₂) (t₁' >>= t₂')

  throw : ∀ {τ l} {t t' : Term _ χ} -> Erase t t' -> Erase (throw {τ = τ} l t) (throw l t')
  catch : ∀ {l τ} {t₁ t₁' : Term Γ (Mac l τ)} {t₂ t₂' :  Term Γ (χ ➔ Mac l τ)} ->
            Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (catch t₁ t₂) (catch t₁' t₂')

  join : ∀ {τ l h} {t t' : Term _ (Mac h τ)} -> (l⊑h : l ⊑ h) (h⊑A : h ⊑ A) -> Erase t t' -> Erase (join l⊑h t) (join l⊑h t')
  join' : ∀ {τ l h} {t t' : Term _ (Mac h τ)} -> (l⊑h : l ⊑ h) (h⋤A : h ⋤ A) -> Erase t t' -> Erase (join l⊑h t) (join∙ l⊑h t')

  join∙ : ∀ {τ l h} {t t' : Term _ (Mac h τ)} -> (l⊑h : l ⊑ h) -> Erase t t' -> Erase (join∙ l⊑h t) (join∙ l⊑h t')

  --------------------------------------------------------------------------------
  -- Flexible Labeled Values

  Labeled : ∀ {τ l} {t t' : Term Γ τ} -> (l⊑A : l ⊑ A) -> Erase t t' -> Erase (Labeled l t) (Labeled l t')
  Labeled' : ∀ {τ l} {t : Term Γ τ} -> (l⋤A : l ⋤ A) ->  Erase (Labeled l t) (Labeled l ∙)

  Labeledχ : ∀ {l τ} {t t' : Term Γ χ} -> (l⊑A : l ⊑ A) -> Erase t t' -> Erase (Labeledχ {τ = τ} l t) (Labeledχ l t')
  Labeledχ' : ∀ {l τ} {t : Term Γ χ} -> (l⋤A : l ⋤ A) -> Erase (Labeledχ {τ = τ} l t) (Labeled l ∙)


  label : ∀ {l h τ} {l⊑h : l ⊑ h} {t t' : Term _ τ} -> (h⊑A : h ⊑ A) -> Erase t t' -> Erase (label l⊑h t) (label l⊑h t')
  label' : ∀ {l h τ} {l⊑h : l ⊑ h} {t : Term _ τ} -> (h⋤A : h ⋤ A) -> Erase (label l⊑h t) (label l⊑h ∙)

  unlabel : ∀ {l h τ} {t t' : Term _ (Labeled l τ)} -> (l⊑h : l ⊑ h) -> Erase t t' -> Erase (unlabel l⊑h t) (unlabel l⊑h t')

  fmap : ∀ {l τ₁ τ₂} {t₁ t₁' : Term _ (τ₁ ➔ τ₂)} {t₂ t₂' : Term _ (Labeled l τ₁)} ->
           (l⊑A : l ⊑ A) -> Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ ⟨$⟩ t₂) (t₁' ⟨$⟩ t₂')

  fmap' : ∀ {l τ₁ τ₂} {t₁ t₁' : Term _ (τ₁ ➔ τ₂)} {t₂ t₂' : Term _ (Labeled l τ₁)} ->
            (l⋤A : l ⋤ A) -> Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ ⟨$⟩ t₂) (t₁' ⟨$⟩∙ t₂')

  fmap∙ : ∀ {l τ₁ τ₂} {t₁ t₁' : Term _ (τ₁ ➔ τ₂)} {t₂ t₂' : Term _ (Labeled l τ₁)} ->
            Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ ⟨$⟩∙ t₂) (t₁' ⟨$⟩∙ t₂')

  app : ∀ {l τ₁ τ₂} {t₁ t₁' : Term _ (Labeled l (τ₁ ➔ τ₂))} {t₂ t₂' : Term _ (Labeled l τ₁)} ->
            (l⊑A : l ⊑ A) -> Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ ⟨✴⟩ t₂) (t₁' ⟨✴⟩ t₂' )

  app' : ∀ {l τ₁ τ₂} {t₁ t₁' : Term _ (Labeled l (τ₁ ➔ τ₂))} {t₂ t₂' : Term _ (Labeled l τ₁)} ->
            (l⋤A : l ⋤ A) -> Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ ⟨✴⟩ t₂) (t₁' ⟨✴⟩∙ t₂' )

  app∙ : ∀ {l τ₁ τ₂} {t₁ t₁' : Term _ (Labeled l (τ₁ ➔ τ₂))} {t₂ t₂' : Term _ (Labeled l τ₁)} ->
           Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (t₁ ⟨✴⟩∙ t₂) (t₁' ⟨✴⟩∙ t₂' )

  relabel : ∀ {l h τ} {t t' : Term _ (Labeled l τ)} -> (l⊑h : l ⊑ h) ->
            (h⊑A : h ⊑ A) -> Erase t t' -> Erase (relabel l⊑h t) (relabel l⊑h t')

  relabel' : ∀ {l h τ} {t t' : Term _ (Labeled l τ)} -> (l⊑h : l ⊑ h) ->
            (h⋤A : h ⋤ A) -> Erase t t' -> Erase (relabel l⊑h t) (relabel∙ l⊑h t')

  relabel∙ : ∀ {l h τ} {t t' : Term _ (Labeled l τ)} -> (l⊑h : l ⊑ h) ->
              Erase t t' -> Erase (relabel∙ l⊑h t) (relabel∙ l⊑h t')

  --------------------------------------------------------------------------------
  -- Memory

  Ref : ∀ {l τ} {t t' : Term _ Addr} -> (l⊑A : l ⊑ A) -> Erase t t' -> Erase (Ref {τ = τ} l t) (Ref l t')
  Ref' : ∀ {l τ} {t : Term _ Addr} -> (l⋤A : l ⋤ A) -> Erase (Ref {τ = τ} l t) (Ref l ∙)

  read : ∀ {l h τ} {t t' : Term _ (Ref l τ)} -> (l⊑h : l ⊑ h) -> Erase t t' -> Erase (read {τ = τ} l⊑h t) (read l⊑h t')

  write : ∀ {l h τ} {t₁ t₁' : Term _ τ} {t₂ t₂' : Term _ (Ref h τ)} -> (l⊑h : l ⊑ h) (h⊑A : h ⊑ A) ->
               Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (write l⊑h t₁ t₂) (write l⊑h t₁' t₂')

  write' : ∀ {l h τ} {t₁ t₁' : Term _ τ} {t₂ t₂' : Term _ (Ref h τ)} -> (l⊑h : l ⊑ h) (h⋤A : h ⋤ A) ->
               Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (write l⊑h t₁ t₂) (write∙ l⊑h t₁' t₂')


  write∙ : ∀ {l h τ}  {t₁ t₁' : Term _ τ} {t₂ t₂' : Term _ (Ref h τ)} -> (l⊑h : l ⊑ h) ->
             Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (write∙ l⊑h t₁ t₂) (write∙ l⊑h t₁' t₂')

  new : ∀ {l h τ} {t t' : Term _ τ} (l⊑h : l ⊑ h) (h⊑A : h ⊑ A) -> Erase t t' -> Erase (new l⊑h t) (new l⊑h t')
  new' : ∀ {l h τ} {t t' : Term _ τ} (l⊑h : l ⊑ h) (h⋤A : h ⋤ A) -> Erase t t' -> Erase (new l⊑h t) (new∙ l⊑h t')
  new∙ : ∀ {l h τ} {t t' : Term _ τ} (l⊑h : l ⊑ h) -> Erase t t' -> Erase (new∙ l⊑h t) (new∙ l⊑h t')

  MVar : ∀ {l τ} {t t' : Term _ _} -> (l⊑A : l ⊑ A) -> Erase t t' -> Erase (MVar {τ = τ} l t) (MVar l t')
  MVar' : ∀ {l τ} {t : Term _ _} -> (l⋤A : l ⋤ A) -> Erase (MVar {τ = τ} l t) (MVar l ∙)

  newMVar : ∀ {l h τ} (l⊑h : l ⊑ h) (h⊑A : h ⊑ A) -> Erase (newMVar {τ = τ} l⊑h) (newMVar l⊑h)
  newMVar' : ∀ {l h τ} (l⊑h : l ⊑ h) (h⋤A : h ⋤ A) -> Erase (newMVar {τ = τ} l⊑h) (newMVar∙ l⊑h)
  newMVar∙ : ∀ {l h τ} (l⊑h : l ⊑ h) -> Erase (newMVar∙ {τ = τ} l⊑h) (newMVar∙ l⊑h)

  take : ∀ {l τ} {t t' : Term _ (MVar l τ)}  -> Erase t t' -> Erase (take t) (take t')
  put : ∀ {l τ} {t₁ t₁' : Term _ τ} {t₂ t₂' : Term _ (MVar l τ)}
        -> Erase t₁ t₁' -> Erase t₂ t₂' -> Erase (put t₁ t₂) (put t₁' t₂')

  #[_] : (n : ℕ) -> Erase #[ n ] #[ n ]

  fork : ∀ {l h} {t t' : Term _ _} -> (l⊑h : l ⊑ h) (h⊑A : h ⊑ A) -> Erase t t' -> Erase (fork l⊑h t) (fork l⊑h t')
  fork' : ∀ {l h} {t t' : Term _ _} -> (l⊑h : l ⊑ h) (h⋤A : h ⋤ A) -> Erase t t' -> Erase (fork l⊑h t) (fork∙ l⊑h t')
  fork∙ : ∀ {l h} {t t' : Term _ _} -> (l⊑h : l ⊑ h) -> Erase t t' -> Erase (fork∙ l⊑h t) (fork∙ l⊑h t')

  ∙ : ∀ {τ} -> Erase {τ = τ} ∙ ∙


-- Sufficient condition for erasure
lift-ε : ∀ {τ Γ} -> (t : Term Γ τ) -> Erase t (ε t)
lift-ε （） = （）
lift-ε True = True
lift-ε False = False
lift-ε (var τ∈Γ) = var τ∈Γ
lift-ε (Λ t) = Λ (lift-ε t)
lift-ε (t ∘ t₁) = (lift-ε t) ∘ (lift-ε t₁)
lift-ε (if t then t₁ else t₂) = if (lift-ε t) then (lift-ε t₁) else (lift-ε t₂)
lift-ε (return l t) = return (lift-ε t)
lift-ε (t >>= t₁) = (lift-ε t) >>= (lift-ε t₁)
lift-ε (Labeled l t) with l ⊑? A
lift-ε (Labeled l t) | yes p = Labeled p (lift-ε t)
lift-ε (Labeled l t) | no ¬p = Labeled' ¬p
lift-ε (Labeledχ l t) with l ⊑? A
lift-ε (Labeledχ l t) | yes p = Labeledχ p (lift-ε t)
lift-ε (Labeledχ l t) | no ¬p = Labeledχ' ¬p
lift-ε (label {h = h} l⊑h t) with h ⊑? A
lift-ε (label l⊑h t) | yes p = label p (lift-ε t)
lift-ε (label l⊑h t) | no ¬p = label' ¬p
lift-ε (unlabel l⊑h t) = unlabel l⊑h (lift-ε t)
lift-ε ξ = ξ
lift-ε (throw l t) = throw (lift-ε t)
lift-ε (catch t t₁) = catch (lift-ε t) (lift-ε t₁)
lift-ε (join {h = h} l⊑h t) with h ⊑? A
lift-ε (join {h = h} l⊑h t) | yes p = join l⊑h p (lift-ε t)
lift-ε (join {h = h} l⊑h t) | no ¬p = join' l⊑h ¬p (lift-ε t)
lift-ε (join∙ l⊑h t) = join∙ l⊑h (lift-ε t)
lift-ε (Ref l t) with l ⊑? A
... | yes p = Ref p (lift-ε t)
... | no ¬p = Ref' ¬p
lift-ε {Labeled l _} (t ⟨$⟩ t₁) with l ⊑? A
... | yes p  = fmap p (lift-ε t) (lift-ε t₁)
... | no ¬p = fmap' ¬p (lift-ε t) (lift-ε t₁)
lift-ε {Labeled l _} (t ⟨$⟩∙ t₁) with l ⊑? A
... | yes p = fmap∙ (lift-ε t) (lift-ε t₁)
... | no ¬p = fmap∙ (lift-ε t) (lift-ε t₁)
lift-ε {Labeled l _} (t ⟨✴⟩ t₁) with l ⊑? A
... | yes p = app p (lift-ε t) (lift-ε t₁)
... | no –p = app' –p (lift-ε t) (lift-ε t₁)
lift-ε {Labeled l _} (t ⟨✴⟩∙ t₁) with l ⊑? A
... | yes p = app∙ (lift-ε t) (lift-ε t₁)
... | no ¬p = app∙ (lift-ε t) (lift-ε t₁)
lift-ε {Labeled h _} (relabel l⊑h t) with h ⊑? A
... | yes p = relabel l⊑h p (lift-ε t)
... | no ¬p = relabel' l⊑h ¬p (lift-ε t)
lift-ε {Labeled h _} (relabel∙ l⊑h t) with h ⊑? A
... | yes p = relabel∙ l⊑h (lift-ε t)
... | no ¬p = relabel∙ l⊑h (lift-ε t)
lift-ε (read x t) = read x (lift-ε t)
lift-ε (write {h = h} x t t₁) with h ⊑? A
lift-ε (write x t t₁) | yes p = write x p (lift-ε t) (lift-ε t₁)
lift-ε (write x t t₁) | no ¬p = write' x ¬p (lift-ε t) (lift-ε t₁)
lift-ε (write∙ x t t₁) = write∙ x (lift-ε t) (lift-ε t₁)
lift-ε (new {h = h} x t) with h ⊑? A
lift-ε (new x t) | yes p = new x p (lift-ε t)
lift-ε (new x t) | no ¬p = new' x ¬p (lift-ε t)
lift-ε (new∙ x t) = new∙ x (lift-ε t)
lift-ε (MVar l t) with l ⊑? A
... | yes p = MVar p (lift-ε t)
... | no ¬p = MVar' ¬p
lift-ε (newMVar {h = h} l⊑h) with h ⊑? A
... | yes p = newMVar l⊑h p
... | no ¬p = newMVar' l⊑h ¬p
lift-ε (newMVar∙ l⊑h) = newMVar∙ l⊑h
lift-ε (take t) = take (lift-ε t)
lift-ε (put t t₁) = put (lift-ε t) (lift-ε t₁)
lift-ε #[ x ] = #[ x ]
lift-ε (fork {h = h} l⊑h t) with h ⊑? A
lift-ε (fork l⊑h t) | yes p = fork l⊑h p (lift-ε t)
lift-ε (fork l⊑h t) | no ¬p = fork' l⊑h ¬p (lift-ε t)
lift-ε (fork∙ l⊑h t) = fork∙ l⊑h (lift-ε t)
lift-ε ∙ = ∙

-- Necessary condition for erasure
unlift-ε : ∀ {Γ τ} {t t' : Term Γ τ} -> Erase t t' -> ε t ≡ t'
unlift-ε （） = refl
unlift-ε True = refl
unlift-ε False = refl
unlift-ε (var τ∈Γ) = refl
unlift-ε ξ = refl
unlift-ε (Λ x) rewrite unlift-ε x = refl
unlift-ε (x ∘ x₁)
  rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (if x then x₁ else x₂)
    rewrite unlift-ε x | unlift-ε x₁ | unlift-ε x₂ = refl
unlift-ε (return x) rewrite unlift-ε x = refl
unlift-ε (x >>= x₁)
  rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (throw x) rewrite unlift-ε x = refl
unlift-ε (catch x x₁)
  rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (join {h = h} l⊑h h⊑A x) with h ⊑? A
... | yes _ rewrite unlift-ε x = refl
... | no h⋤A = ⊥-elim (h⋤A h⊑A)
unlift-ε (join' {h = h} l⊑h h⋤A x) with h ⊑? A
... | yes h⊑A = ⊥-elim (h⋤A h⊑A)
... | no _ rewrite unlift-ε x = refl
unlift-ε (join∙ {h = h} l⊑h x) with h ⊑? A
... | yes _ rewrite unlift-ε x = refl
... | no _ rewrite unlift-ε x = refl
unlift-ε (Labeled {l = l} p x) with l ⊑? A
unlift-ε (Labeled p x) | yes p' rewrite unlift-ε x = refl
unlift-ε (Labeled p x) | no ¬p = ⊥-elim (¬p p)
unlift-ε (Labeledχ {l = l} l⊑A x) with l ⊑? A
... | yes _ rewrite unlift-ε x = refl
... | no l⋤A = ⊥-elim (l⋤A l⊑A)
unlift-ε (Labeledχ' {l = l} l⋤A) with l ⊑? A
... | yes l⊑A = ⊥-elim (l⋤A l⊑A)
... | no _ = refl
unlift-ε (Labeled' {l = l} x) with l ⊑? A
unlift-ε (Labeled' x) | yes p = ⊥-elim (x p)
unlift-ε (Labeled' x) | no ¬p = refl
unlift-ε (label {h = h} p x) with h ⊑? A
unlift-ε (label p₁ x) | yes p rewrite unlift-ε x = refl
unlift-ε (label p x) | no ¬p = ⊥-elim (¬p p)
unlift-ε (label' {h = h} h⋤A) with h ⊑? A
unlift-ε (label' h⋤A) | yes p = ⊥-elim (h⋤A p)
unlift-ε (label' h⋤A) | no ¬p = refl
unlift-ε (unlabel l⊑h x) rewrite unlift-ε x = refl
unlift-ε (fmap {l = l} l⊑A x x₁) with l ⊑? A
... | yes _   rewrite unlift-ε x | unlift-ε x₁ = refl
... | no l⋤A = ⊥-elim (l⋤A l⊑A)
unlift-ε (fmap' {l = l} l⋤A x x₁) with l ⊑? A
... | yes l⊑A = ⊥-elim (l⋤A l⊑A)
... | no _ rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (fmap∙ {l = l} x x₁) with l ⊑? A
... | yes _ rewrite unlift-ε x | unlift-ε x₁ = refl
... | no _ rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (app {l = l} l⊑A x x₁) with l ⊑? A
... | yes _ rewrite unlift-ε x | unlift-ε x₁ = refl
... | no l⋤A = ⊥-elim (l⋤A l⊑A)
unlift-ε (app' {l = l} l⋤A x x₁) with l ⊑? A
... | yes l⊑A  = ⊥-elim (l⋤A l⊑A)
... | no _ rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (app∙ {l = l} x x₁) with l ⊑? A
... | yes _ rewrite unlift-ε x | unlift-ε x₁ = refl
... | no _ rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (relabel {h = h} l⊑h h⊑A x) with h ⊑? A
... | yes _ rewrite unlift-ε x = refl
... | no h⋤A = ⊥-elim (h⋤A h⊑A)
unlift-ε (relabel' {h = h} l⊑h h⋤A x) with h ⊑? A
... | yes h⊑A = ⊥-elim (h⋤A h⊑A)
... | no _ rewrite unlift-ε x = refl
unlift-ε (relabel∙ {h = h} l⊑h x) with h ⊑? A
... | yes _ rewrite unlift-ε x = refl
... | no _ rewrite unlift-ε x = refl
unlift-ε (read l⊑h x) rewrite unlift-ε x = refl
unlift-ε (write {h = h} l⊑h p x x₁) with h ⊑? A
unlift-ε (write l⊑h p₁ x x₁) | yes p
  rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (write l⊑h p x x₁) | no ¬p = ⊥-elim (¬p p)
unlift-ε (write' {h = h} l⊑h x x₁ x₂) with h ⊑? A
unlift-ε (write' l⊑h x x₁ x₂) | yes p = ⊥-elim (x p)
unlift-ε (write' l⊑h x x₁ x₂) | no ¬p
  rewrite unlift-ε x₁ | unlift-ε x₂ = refl
unlift-ε (write∙ {h = h} l⊑h x x₁) with h ⊑? A
unlift-ε (write∙ l⊑h x x₁) | yes p
  rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (write∙ l⊑h x x₁) | no ¬p
  rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε (new {h = h} l⊑h p x) with h ⊑? A
unlift-ε (new l⊑h p₁ x) | yes p rewrite unlift-ε x = refl
unlift-ε (new l⊑h p x) | no ¬p = ⊥-elim (¬p p)
unlift-ε (new' {h = h} l⊑h h⋤A x) with h ⊑? A
unlift-ε (new' l⊑h h⋤A x) | yes p = ⊥-elim (h⋤A p)
unlift-ε (new' l⊑h h⋤A x) | no ¬p rewrite unlift-ε x = refl
unlift-ε (new∙ {h = h} l⊑h x) with h ⊑? A
unlift-ε (new∙ l⊑h x) | yes p rewrite unlift-ε x = refl
unlift-ε (new∙ l⊑h x) | no ¬p rewrite unlift-ε x = refl
unlift-ε (Ref {l = l} l⊑A x) with l ⊑? A
... | yes _  rewrite unlift-ε x = refl
... | no l⋤A = ⊥-elim (l⋤A l⊑A)
unlift-ε (Ref' {l = l} l⋤A) with l ⊑? A
... | yes l⊑A = ⊥-elim (l⋤A l⊑A)
... | no _ = refl
unlift-ε (MVar {l = l} l⊑A x) with l ⊑? A
... | yes _  rewrite unlift-ε x = refl
... | no l⋤A = ⊥-elim (l⋤A l⊑A)
unlift-ε (MVar' {l = l} l⋤A) with l ⊑? A
... | yes l⊑A = ⊥-elim (l⋤A l⊑A)
... | no _ = refl
unlift-ε (newMVar {h = h} l⊑h p) with h ⊑? A
unlift-ε (newMVar l⊑h p₁) | yes p = refl
unlift-ε (newMVar l⊑h p) | no ¬p = ⊥-elim (¬p p)
unlift-ε (newMVar' {h = h} l⊑h h⋤A) with h ⊑? A
unlift-ε (newMVar' l⊑h h⋤A) | yes p = ⊥-elim (h⋤A p)
unlift-ε (newMVar' l⊑h h⋤A) | no ¬p = refl
unlift-ε (newMVar∙ {h = h} l⊑h) with h ⊑? A
unlift-ε (newMVar∙ l⊑h) | yes p = refl
unlift-ε (newMVar∙ l⊑h) | no ¬p = refl
unlift-ε (take x) rewrite unlift-ε x = refl
unlift-ε (put x x₁)
  rewrite unlift-ε x | unlift-ε x₁ = refl
unlift-ε #[ n ] = refl
unlift-ε (fork {h = h} l⊑h p x) with h ⊑? A
unlift-ε (fork l⊑h p₁ x) | yes p rewrite unlift-ε x = refl
unlift-ε (fork l⊑h p x) | no ¬p = ⊥-elim (¬p p)
unlift-ε (fork' {h = h} l⊑h h⋤A x) with h ⊑? A
unlift-ε (fork' l⊑h h⋤A x) | yes p = ⊥-elim (h⋤A p)
unlift-ε (fork' l⊑h h⋤A x) | no ¬p rewrite unlift-ε x = refl
unlift-ε (fork∙ l⊑h x) rewrite unlift-ε x = refl
unlift-ε ∙ = refl

--------------------------------------------------------------------------------

-- The graph of the erasure function for cells
data Eraseᶜ {τ} : Cell τ -> Cell τ -> Set where
  ⊗ : Eraseᶜ ⊗ ⊗
  ⟦_⟧ : ∀ {t₁ t₂ : CTerm τ} -> Erase t₁ t₂ -> Eraseᶜ ⟦ t₁ ⟧ ⟦ t₂ ⟧

-- Sufficient condition
lift-εᶜ : ∀ {τ} (c : Cell τ) -> Eraseᶜ c (εᶜ c)
lift-εᶜ ⊗ = ⊗
lift-εᶜ ⟦ x ⟧ = ⟦ lift-ε x ⟧

-- Necessary condition
unlift-εᶜ : ∀ {τ} {c₁ c₂ : Cell τ} -> Eraseᶜ c₁ c₂ -> εᶜ c₁ ≡ c₂
unlift-εᶜ ⊗ = refl
unlift-εᶜ ⟦ x ⟧ rewrite unlift-ε x = refl

--------------------------------------------------------------------------------

-- The graph of the erasure function for memories
data Eraseᴹ {l : Label} : Memory l -> Memory l -> Set where
  [] : Eraseᴹ [] []
  _∷_ : ∀ {τ} {c₁ c₂ : Cell τ} {M₁ M₂ : Memory l} -> Eraseᶜ c₁ c₂ -> Eraseᴹ M₁ M₂ -> Eraseᴹ (c₁ ∷ M₁) (c₂ ∷ M₂)
  ∙ : Eraseᴹ ∙ ∙

-- Sufficient condition
lift-εᴹ : ∀ {l} (M : Memory l) -> Eraseᴹ M (εᴹ M)
lift-εᴹ ∙ = ∙
lift-εᴹ [] = []
lift-εᴹ (x ∷ M) = (lift-εᶜ x) ∷ (lift-εᴹ M)

-- Necessary condition
unlift-εᴹ : ∀ {l} {M₁ M₂ : Memory l} -> Eraseᴹ M₁ M₂ -> εᴹ M₁ ≡ M₂
unlift-εᴹ [] = refl
unlift-εᴹ (x ∷ e)
  rewrite unlift-εᶜ x | unlift-εᴹ e = refl
unlift-εᴹ ∙ = refl

--------------------------------------------------------------------------------

-- The graph of the erasure function for stores
Eraseˢ : Store -> Store -> Set
Eraseˢ Σ Σ' = (l : Label) -> (l ⊑ A × Eraseᴹ (Σ l) (Σ' l)) ⊎ ( l ⋤ A × Σ' l ≡ ∙)

-- Sufficient condition
lift-εˢ : (Σ : Store) -> Eraseˢ Σ (εˢ Σ)
lift-εˢ Σ l with l ⊑? A
... | yes l⊑A = inj₁ (l⊑A , (lift-εᴹ (Σ l)))
... | no l⋤A = inj₂ (l⋤A , refl)

-- Necessary condition
unlift-εˢ : ∀ {Σ Σ' : Store} -> Eraseˢ Σ Σ' -> εˢ Σ ≡ Σ'
unlift-εˢ f = fun-ext (proof f)
  where proof : ∀ {Σ Σ'} -> Eraseˢ Σ Σ' -> (l : Label) -> εˢ Σ l ≡ Σ' l
        proof f l with l ⊑? A
        proof f l | yes l⊑A with f l
        proof f l | yes l⊑A | inj₁ (l⊑A' , e) rewrite unlift-εᴹ e = refl
        proof f l | yes l⊑A | inj₂ (l⋤A , _) = ⊥-elim (l⋤A l⊑A)
        proof f l | no l⋤A with f l
        proof f l | no l⋤A | inj₁ (l⊑A , e) = ⊥-elim (l⋤A l⊑A)
        proof f l | no l⋤A | inj₂ (_ , eq) = sym eq

-- Helper function to access the erasure of low memories
getᴸ : ∀ {Σ Σ' L} -> Eraseˢ Σ Σ' -> L ⊑ A -> Eraseᴹ (Σ L) (Σ' L)
getᴸ {L = L} eˢ L⊑A with eˢ L
getᴸ {L = L} eˢ L⊑A | inj₁ x = proj₂ x
getᴸ {L = L} eˢ L⊑A | inj₂ (L⋤A , proj₄) = ⊥-elim (L⋤A L⊑A)

--------------------------------------------------------------------------------

-- The graph of the erasure function for programs
data Eraseᴾ {τ : Ty} {l : Label} : Dec (l ⊑ A) -> (p₁ p₂ : Program (Mac l τ)) -> Set where
  lowᴾ : ∀ {Σ Σ'} {t t' : CTerm _} -> (l⊑A : l ⊑ A) (eˢ : Eraseˢ Σ Σ') (e : Erase t t') -> Eraseᴾ (yes l⊑A) ⟨ Σ , t ⟩ ⟨ Σ' , t' ⟩
  highᴾ : ∀ {Σ Σ'} {t : CTerm _} -> (l⋤A : l ⋤ A) (eˢ : Eraseˢ Σ Σ') -> Eraseᴾ (no l⋤A) ⟨ Σ , t ⟩ ⟨ Σ' , ∙ ⟩

-- Sufficient condition
lift-εᴾ : ∀ {l τ} -> (x : Dec (l ⊑ A)) -> (p : Program (Mac l τ)) -> Eraseᴾ x p (εᴾ x p)
lift-εᴾ (yes p) ⟨ Σ₁ , t₁ ⟩ = lowᴾ p (lift-εˢ Σ₁) (lift-ε t₁)
lift-εᴾ (no ¬p) ⟨ Σ₁ , t₁ ⟩ = highᴾ ¬p (lift-εˢ Σ₁)

-- Necessary condition
unlift-εᴾ : ∀ {l τ} {p p' : Program (Mac l τ)} -> (x : Dec (l ⊑ A)) -> Eraseᴾ x p p' -> εᴾ x p ≡ p'
unlift-εᴾ (yes p) (lowᴾ .p x x₁) rewrite unlift-εˢ x | unlift-ε x₁ = refl
unlift-εᴾ (no ¬p) (highᴾ .¬p x) rewrite unlift-εˢ x = refl

--------------------------------------------------------------------------------
-- Lemmas

wkenᴱ : ∀ {Γ₁ Γ₂ τ} {t t' : Term Γ₁ τ} -> Erase t t' -> (p : Γ₁ ⊆ Γ₂) ->  Erase (wken t p) (wken t' p)
wkenᴱ {Γ₁} {Γ₂} {τ} {t} e p with lift-ε (wken t p)
... | x rewrite unlift-ε e = x

substᴱ :  ∀ {Γ τ₁ τ₂} {x x' : Term Γ τ₁} {t t' : Term (τ₁ ∷ Γ) τ₂} -> Erase x x' -> Erase t t' -> Erase (subst x t) (subst x' t')
substᴱ {x = x} {t = t} e₁ e₂ with lift-ε (subst x t)
... | e rewrite unlift-ε e₁ | unlift-ε e₂ = e
