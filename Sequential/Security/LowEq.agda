-- This module defines low-equivalence for the sequential part of the
-- calculus.
--
-- We define low-equivalence as the kernel of the erasure function,
-- i.e., t₁ ≅ t₂ iff ε t₁ ≡ ε t₂.  Since this definition is
-- inconvenient for many proofs, we define also an alternative
-- "structural" version of low-equivalence (≈), which exploits the
-- graph of the function defined in Sequential.Security.Graph.  In
-- particular, the data-type t₁ ≈ t₂, contains two graphs Erase t₁ tᵉ
-- and Erase t₂ tᵉ (note the same erasure tᵉ as second index).  For
-- each definition of structural low-equivalence (≈) we prove that it
-- is equivalent to the definitional notion (≅) (see functions ⌞_⌟ and
-- ⌜_⌝).

import Lattice as L

module Sequential.Security.LowEq (𝓛 : L.Lattice) (A : L.Label 𝓛) where

open import Types 𝓛
open import Sequential.Security.Erasure 𝓛 A
open import Sequential.Security.Graph 𝓛 A
open import Sequential.Calculus 𝓛

open import Relation.Binary.PropositionalEquality
open import Data.Empty
open import Relation.Nullary
open import Data.Maybe as M

--------------------------------------------------------------------------------

_≅_ : ∀ {π τ} (t₁ t₂ : Term π τ) -> Set
t₁ ≅ t₂ = ε t₁ ≡ ε t₂

data _≈_ {π τ} (t₁ t₂ : Term π τ) : Set where
  ⟨_,_⟩ : ∀ {t' : Term π τ} -> (e₁ : Erase t₁ t') (e₂ : Erase t₂ t') -> t₁ ≈ t₂

⌞_⌟ : ∀ {π τ} {t₁ t₂ : Term π τ} -> t₁ ≈ t₂ -> t₁ ≅ t₂
⌞ ⟨ e₁ , e₂ ⟩ ⌟ rewrite unlift-ε e₁ | unlift-ε e₂ = refl

⌜_⌝ : ∀ {π τ} {t₁ t₂ : Term π τ} -> t₁ ≅ t₂ -> t₁ ≈ t₂
⌜_⌝ {t₁ = t₁} {t₂} eq with lift-ε t₁ | lift-ε t₂
... | x | y rewrite eq = ⟨ x , y ⟩

refl-≈ : ∀ {π τ} {t : Term π τ} -> t ≈ t
refl-≈ = ⌜ refl ⌝

sym-≈ : ∀ {π τ} {t₁ t₂ : Term π τ} -> t₁ ≈ t₂ -> t₂ ≈ t₁
sym-≈ t₁≈t₂ = ⌜ sym ⌞ t₁≈t₂ ⌟ ⌝

trans-≈ : ∀ {π τ} {t₁ t₂ t₃ : Term π τ} -> t₁ ≈ t₂ -> t₂ ≈ t₃ -> t₁ ≈ t₃
trans-≈ t₁≈t₂ t₂≈t₃ = ⌜ trans ⌞ t₁≈t₂ ⌟ ⌞ t₂≈t₃ ⌟ ⌝

--------------------------------------------------------------------------------

_≅ᴹ_ : ∀ {l} -> Memory l -> Memory l -> Set
M₁ ≅ᴹ M₂ = εᴹ M₁ ≡ εᴹ M₂

data _≈ᴹ_ {l} (M₁ : Memory l) (M₂ : Memory l) : Set where
  Kᴹ : ∀ {Mᴱ : Memory l} -> Eraseᴹ M₁ Mᴱ -> Eraseᴹ M₂ Mᴱ -> M₁ ≈ᴹ M₂

⌞_⌟ᴹ : ∀ {l} {M₁ M₂ : Memory l} -> M₁ ≈ᴹ M₂ -> M₁ ≅ᴹ M₂
⌞ Kᴹ e₁ e₂ ⌟ᴹ rewrite unlift-εᴹ e₁ | unlift-εᴹ e₂ = refl

⌜_⌝ᴹ : ∀ {l} {M₁ M₂ : Memory l} -> M₁ ≅ᴹ M₂ -> M₁ ≈ᴹ M₂
⌜_⌝ᴹ {M₁ = M₁} {M₂} eq with lift-εᴹ M₁ | lift-εᴹ M₂
... | e₁ | e₂ rewrite eq = Kᴹ e₁ e₂

--------------------------------------------------------------------------------

_≅ˢ_ : Store -> Store -> Set
Σ₁ ≅ˢ Σ₂ = εˢ Σ₁ ≡ εˢ Σ₂

data _≈ˢ_ (Σ₁ Σ₂ : Store) : Set where
  Kˢ : ∀ {Σᴱ} -> Eraseˢ Σ₁ Σᴱ -> Eraseˢ Σ₂ Σᴱ -> Σ₁ ≈ˢ Σ₂

⌞_⌟ˢ : ∀ {Σ₁ Σ₂} -> Σ₁ ≈ˢ Σ₂ -> Σ₁ ≅ˢ Σ₂
⌞ Kˢ e₁ e₂ ⌟ˢ rewrite unlift-εˢ e₁ | unlift-εˢ e₂ = refl

⌜_⌝ˢ : ∀ {Σ₁ Σ₂} -> Σ₁ ≅ˢ Σ₂ -> Σ₁ ≈ˢ Σ₂
⌜_⌝ˢ {Σ₁} {Σ₂} eq = Kˢ (lift-εˢ Σ₁) (flip (sym eq) (lift-εˢ Σ₂))
  where flip : ∀ {Σ₂' Σ₂''} -> Σ₂' ≡ Σ₂'' -> Eraseˢ Σ₂ Σ₂' -> Eraseˢ Σ₂ Σ₂''
        flip refl e = e

trans-≈ˢ : ∀ {Σ₁ Σ₂ Σ₃ : Store} → Σ₁ ≈ˢ Σ₂ → Σ₂ ≈ˢ Σ₃ → Σ₁ ≈ˢ Σ₃
trans-≈ˢ Σ₁≈Σ₂ Σ₂≈Σ₃ = ⌜ trans ⌞ Σ₁≈Σ₂ ⌟ˢ ⌞ Σ₂≈Σ₃ ⌟ˢ ⌝ˢ


--------------------------------------------------------------------------------

_≅ᴾ⟨_⟩_ : ∀ {l τ} -> Program (Mac l τ) -> Dec (l ⊑ A) -> Program (Mac l τ) -> Set
p₁ ≅ᴾ⟨ x ⟩ p₂ = εᴾ x p₁ ≡ εᴾ x p₂

open Program

-- Program low-equivalence
data _≈ᴾ⟨_⟩_ {l τ} (p₁ : Program (Mac l τ)) (x : Dec (l ⊑ A)) (p₂ : Program (Mac l τ)) : Set where
  Kᴾ : ∀ {pᴱ} -> (e₁ : Eraseᴾ x p₁ pᴱ) (e₂ : Eraseᴾ x p₂ pᴱ) -> p₁ ≈ᴾ⟨ x ⟩ p₂


⌞_⌟ᴾ : ∀ {l τ} {p₁ p₂ : Program (Mac l τ)} {x : Dec _} -> p₁ ≈ᴾ⟨ x ⟩ p₂ -> p₁ ≅ᴾ⟨ x ⟩ p₂
⌞_⌟ᴾ {x = x}  (Kᴾ e₁ e₂) rewrite unlift-εᴾ x e₁ | unlift-εᴾ x e₂ =  refl


⌜_⌝ᴾ : ∀ {l τ} {p₁ p₂ : Program (Mac l τ)} {x : Dec _} -> p₁ ≅ᴾ⟨ x ⟩ p₂ -> p₁ ≈ᴾ⟨ x ⟩ p₂
⌜_⌝ᴾ {p₁ = p₁} {p₂} {x} eq with lift-εᴾ x p₁ | lift-εᴾ x p₂
... | e₁ | e₂ rewrite eq = Kᴾ e₁ e₂

_≅ᴾ_ : ∀ {l τ} -> (p₁ p₂ : Program (Mac l τ)) -> Set
p₁ ≅ᴾ p₂ = p₁ ≅ᴾ⟨ (_ ⊑? A) ⟩ p₂

refl-≈ᴾ : ∀ {l τ} {p : Program (Mac l τ)} -> p ≈ᴾ⟨ l ⊑? A ⟩ p
refl-≈ᴾ {l} = ⌜ refl ⌝ᴾ

sym-≈ᴾ : ∀ {l τ} {p₁ p₂ : Program (Mac l τ)} {x : Dec (l ⊑ A)} -> p₁ ≈ᴾ⟨ x ⟩ p₂ -> p₂ ≈ᴾ⟨ x ⟩ p₁
sym-≈ᴾ eq = ⌜ sym ⌞ eq ⌟ᴾ ⌝ᴾ

trans-≈ᴾ : ∀ {l τ} {p₁ p₂ p₃ : Program (Mac l τ)} {x : Dec (l ⊑ A)} -> p₁ ≈ᴾ⟨ x ⟩ p₂ -> p₂ ≈ᴾ⟨ x ⟩ p₃ -> p₁ ≈ᴾ⟨ x ⟩ p₃
trans-≈ᴾ eq₁ eq₂ = ⌜ trans ⌞ eq₁ ⌟ᴾ ⌞ eq₂ ⌟ᴾ ⌝ᴾ

ext-≈ᴾ : ∀ {l τ} {x y : Dec (l ⊑ A)} {p₁ p₂ : Program (Mac l τ)} -> p₁ ≈ᴾ⟨ x ⟩ p₂ -> p₁ ≈ᴾ⟨ y ⟩ p₂
ext-≈ᴾ {x = yes p} {yes p₁} (Kᴾ (lowᴾ .p eˢ e) (lowᴾ .p eˢ₁ e₁)) = Kᴾ (lowᴾ p₁ eˢ e) (lowᴾ p₁ eˢ₁ e₁)
ext-≈ᴾ {x = yes p} {no ¬p} eq = ⊥-elim (¬p p)
ext-≈ᴾ {x = no ¬p} {yes p} eq = ⊥-elim (¬p p)
ext-≈ᴾ {x = no ¬p} {no ¬p₁} (Kᴾ (highᴾ .¬p eˢ) (highᴾ .¬p eˢ₁)) = Kᴾ (highᴾ ¬p₁ eˢ) (highᴾ ¬p₁ eˢ₁)

ext-≅ᴾ : ∀ {l τ} {x y : Dec (l ⊑ A)} {p₁ p₂ : Program (Mac l τ)} -> p₁ ≅ᴾ⟨ x ⟩ p₂ -> p₁ ≅ᴾ⟨ y ⟩ p₂
ext-≅ᴾ {x = x} {y = y} eq = ⌞ ext-≈ᴾ {x = x} {y = y} ⌜ eq ⌝ᴾ ⌟ᴾ

get-≈ˢ : ∀ {l τ} {x : Dec (l ⊑ A)} {p₁ p₂ : Program (Mac l τ)} -> p₁ ≈ᴾ⟨ x ⟩ p₂ -> store p₁ ≈ˢ store p₂
get-≈ˢ (Kᴾ (lowᴾ l⊑A eˢ e) (lowᴾ .l⊑A eˢ₁ e₁)) = Kˢ eˢ eˢ₁
get-≈ˢ (Kᴾ (highᴾ l⋤A eˢ) (highᴾ .l⋤A eˢ₁)) = Kˢ eˢ eˢ₁

get-≈ : ∀ {L τ} {L⊑A : L ⊑ A} {p₁ p₂ : Program (Mac L τ)} -> p₁ ≈ᴾ⟨ yes L⊑A ⟩ p₂ -> term p₁ ≈ term p₂
get-≈ (Kᴾ (lowᴾ l⊑A eˢ e) (lowᴾ .l⊑A eˢ₁ e₁)) = ⟨ e , e₁ ⟩

--------------------------------------------------------------------------------
